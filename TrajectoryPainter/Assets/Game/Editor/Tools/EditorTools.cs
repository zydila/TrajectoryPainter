using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using TP.Game.Data;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace TP.Editor.Tools
{
    public static class EditorTools
    {
        [MenuItem("Tools/Remove Persistent Data", false, 153)]
        private static void RemovePersistentData()
        {
            PlayerPrefs.DeleteAll();
        }
    
        [MenuItem("Tools/Config/Levels Config", false, 154)]
        private static void SelectLevelsConfig()
        {
            FocusAssetInEditorWindow("Assets/Game/Resources/Data/LevelPacks/BasicLevelPack.asset");
        }
    
        [MenuItem("Tools/Config/Music Config", false, 155)]
        private static void MusicConfig()
        {
            FocusAssetInEditorWindow("Assets/Game/Resources/Data/MusicData.asset");
        }
    
    
        [MenuItem("Tools/Config/InApp Config", false, 156)]
        private static void InAppConfig()
        {
            FocusAssetInEditorWindow("Assets/Game/Resources/Data/InAppData.asset");
        }
    
        [MenuItem("Tools/Localizations/Localization Folder", false, 150)]
        private static void SelectLocalizationsConfig()
        {
            FocusAssetInEditorWindow("Assets/Game/Resources/Data/Localizations");
        }
    
        [MenuItem("Tools/Localizations/Download", false, 152)]
        private static void DownloadLocalizations()
        {
            LocalizationDownloader.Download();
        }
        
        [MenuItem("Tools/Localizations/Download Tutorial", false, 151)]
        private static void OpenDownloadTutorial()
        {
            Process.Start("notepad.exe", "../Documentation/download_localization_tutorial.txt");
        }
        
        [MenuItem("Tools/Localizations/Edit Localization", false, 153)]
        private static void OpenLocalizationSheet()
        {
            Application.OpenURL("https://docs.google.com/spreadsheets/d/1NsBGrw6zfLKr63ksodU_bQ3AdoTbP5OGEFIIOdDf15M/edit#gid=0");
        }
        
        public static void FocusAssetInEditorWindow(string path)
        {
            EditorUtility.FocusProjectWindow();
            var obj = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
        
        public static List<T> GetAllAssetsInFolder<T>(string path) where T : Object
        {
            var result = new List<T>();
            var allFiles = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            for (var i = 0; i < allFiles.Length; i++)
            {
                var file = allFiles[i];
                var asset = AssetDatabase.LoadAssetAtPath<T>(file);
                if (asset != null)
                {
                    result.Add(asset);
                }
            }

            return result;
        }
    }
}