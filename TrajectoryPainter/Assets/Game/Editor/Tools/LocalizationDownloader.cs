using System;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using Debug = UnityEngine.Debug;

namespace TP.Editor.Tools
{
    public static class LocalizationDownloader
    {
        private const string PY_SCRIPTS_LOCATION = "../pyscripts/";
        private const string DOWNLOAD_SCRIPT = PY_SCRIPTS_LOCATION + "download_localization.py";
        private const string LOCALIZATION_FILE_NAME = "TP Localization.csv";
        private const string LOCALIZATION_FILE_PATH_IN_PROJ = "Assets/Game/Editor/Data/Localization/";
        
        public static void Download()
        {
                var startInfo = new ProcessStartInfo
                {
                        WindowStyle = ProcessWindowStyle.Normal,
                        FileName = "cmd.exe",
                        Arguments = $"/C python {DOWNLOAD_SCRIPT}",
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false
                };
                string error = null;
                try
                {
                    using var process = Process.Start(startInfo);
                    process.WaitForExit(50000);
                    using var errorReader = process.StandardError;
                    using var messageReader = process.StandardOutput;
                    error = errorReader.ReadToEnd();
                    var result = messageReader.ReadToEnd();
                    if (!string.IsNullOrEmpty(error))
                    {
                        Debug.LogWarning(error);
                    }
                    if (!string.IsNullOrEmpty(result))
                    {
                        Debug.Log(result);
                    }
                }
                catch (Exception e)
                {
                    EditorUtility.DisplayDialog("Error!", e.Message, "ОК");
                    return;
                }
                
                if (!string.IsNullOrEmpty(error))
                {
                    EditorUtility.DisplayDialog("Error!", $"Error during python execution. {error}", "ОК");
                    return; 
                }
                
                if (File.Exists(PY_SCRIPTS_LOCATION + LOCALIZATION_FILE_NAME))
                {
                    File.Delete(LOCALIZATION_FILE_PATH_IN_PROJ + LOCALIZATION_FILE_NAME);
                }
                
                File.Move(PY_SCRIPTS_LOCATION + LOCALIZATION_FILE_NAME, LOCALIZATION_FILE_PATH_IN_PROJ + LOCALIZATION_FILE_NAME);
                AssetDatabase.ImportAsset(LOCALIZATION_FILE_PATH_IN_PROJ + LOCALIZATION_FILE_NAME);
                AssetDatabase.Refresh();
                EditorTools.FocusAssetInEditorWindow(LOCALIZATION_FILE_PATH_IN_PROJ + LOCALIZATION_FILE_NAME);
        }
    }
}