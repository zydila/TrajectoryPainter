using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TP.Game.Services;

namespace TP.Editor.Tools
{
    public class LocalizationParser
    {
        private string[] _localizationFileLines;
        private Localization.Language _language;
        Dictionary<Localization.Language, int> _languageInLineIndexes = new Dictionary<Localization.Language, int>();
        
        public LocalizationParser SetLocalizationFile(string filePath)
        {
            _localizationFileLines = File.ReadAllLines(filePath, Encoding.UTF8);
            var firstLineSplit = _localizationFileLines[0].Split(',').ToList();
            foreach (var language in Enum.GetValues(typeof(Localization.Language)).Cast<Localization.Language>())
            {
                var index = firstLineSplit.FindIndex(word => word == language.ToString());
                _languageInLineIndexes.Add(language, index);
            }
            return this;
        }
        
        public LocalizationParser SetLocalizationLanguage(Localization.Language language)
        {
            _language = language;
            return this;
        }

        public Dictionary<string, string> Parse()
        {
            var result = new Dictionary<string, string>();
            var languageIndex = _languageInLineIndexes[_language];
            for (var i = 1; i < _localizationFileLines.Length; i++)
            {
                var lineSplit = _localizationFileLines[i].Split(',');
                if (lineSplit.Length < Enum.GetValues(typeof(Localization.Language)).Length + 1)
                {
                    continue;
                }
                result.Add(lineSplit[0], lineSplit[languageIndex]);
            }
            return result;
        }
        
    }
}