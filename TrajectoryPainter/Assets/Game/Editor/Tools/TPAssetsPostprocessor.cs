using System;
using System.IO;
using System.Linq;
using TP.Game.Data;
using TP.Game.Services;
using UnityEditor;
using UnityEngine;

namespace TP.Editor.Tools
{
    public class TPAssetsPostprocessor : AssetPostprocessor
    {
        private const string MUSIC_PATH = "Assets/Game/Resources/Audio/Music/";
        private const string MUSIC_DATA_PATH = "Assets/Game/Resources/Data/MusicData.asset";
        private const string EDITOR_RESOURCES_PATH = "Assets/Game/Editor/Data/";
        private const string LOCALIZATION_FILE_PATH = EDITOR_RESOURCES_PATH + "Localization/TP Localization.csv";
        private const string LOCALIZATION_DATA_PATH = "Assets/Game/Resources/Data/Localizations/";
        private const string UI_EFFECTS_PATH = "Assets/Game/Resources/Audio/Effects/";

        readonly AudioImporterSampleSettings _musicImporterSettings = new AudioImporterSampleSettings
        {
            sampleRateSetting = AudioSampleRateSetting.OverrideSampleRate,
            sampleRateOverride = 44100,
            loadType = AudioClipLoadType.Streaming,
            compressionFormat = AudioCompressionFormat.Vorbis,
            quality = 0.9f
        };

        readonly AudioImporterSampleSettings _uiEffectsImporterSettings = new AudioImporterSampleSettings
        {
            sampleRateSetting = AudioSampleRateSetting.OverrideSampleRate,
            sampleRateOverride = 44100,
            loadType = AudioClipLoadType.DecompressOnLoad,
            compressionFormat = AudioCompressionFormat.ADPCM,
            quality = 0.9f
        };
        
        private void OnPreprocessAudio()
        {
            if (assetPath.Contains(MUSIC_PATH))
            {
                ProcessMusic();
                Debug.Log("Music Processed");
            }
            if (assetPath.Contains(UI_EFFECTS_PATH))
            {
                ProcessEffects();
                Debug.Log("UI Effects Processed");
            }
        }

        private void OnPreprocessAsset()
        {
            if (assetPath.Contains(LOCALIZATION_FILE_PATH))
            {
                ProcessLocalization();
                Debug.Log("Localization Processed");
            }
        }

        private void ProcessMusic()
        {
            SetMusicImportSettings();
            UpdateMusicData();
        }

        private void ProcessEffects()
        {
            SetEffectsImportSettings();
        }

        private void SetMusicImportSettings()
        {
            var audioImporter = (AudioImporter) assetImporter;
            audioImporter.defaultSampleSettings = _musicImporterSettings;
        }
        
        private void SetEffectsImportSettings()
        {
            var audioImporter = (AudioImporter) assetImporter;
            audioImporter.defaultSampleSettings = _uiEffectsImporterSettings;
        }

        private void ProcessLocalization()
        {
            var parser = new LocalizationParser().SetLocalizationFile(LOCALIZATION_FILE_PATH);
            
            foreach (var language in Enum.GetValues(typeof(Localization.Language)).Cast<Localization.Language>())
            {
                var assetPath = LOCALIZATION_DATA_PATH + language + ".asset";
                var data = AssetDatabase.LoadAssetAtPath<LocalizationData>(assetPath);
                if (data == null)
                {
                    data = CreateData(assetPath);
                }
                data.SetLocalization(parser.SetLocalizationLanguage(language).Parse());
                EditorUtility.SetDirty(data);
            }
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        private LocalizationData CreateData(string assetPath)
        {
            var data = ScriptableObject.CreateInstance<LocalizationData>();
            AssetDatabase.CreateAsset(data, assetPath);
            AssetDatabase.SaveAssets();
            return data;
        }

        private void UpdateMusicData()
        {
            var musicData = AssetDatabase.LoadAssetAtPath<MusicData>(MUSIC_DATA_PATH);
            musicData.Clear();
            var clips = Directory.GetFiles(MUSIC_PATH, "*.*", SearchOption.AllDirectories);
            foreach (var clip in clips)
            {
                if (clip.EndsWith(".wav"))
                {
                    var split = clip.Split('/', '.');
                    musicData.AddClipName(split[split.Length - 2]);
                }
            }
            EditorUtility.SetDirty(musicData);
        }
    }
}