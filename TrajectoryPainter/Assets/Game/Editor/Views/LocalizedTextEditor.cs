using System.Collections.Generic;
using System.Linq;
using TP.Game.Data;
using TP.Game.Views;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Editor.Views
{
    [CustomEditor(typeof(LocalizedText))]
    public class LocalizedTextEditor : UnityEditor.Editor
    {
        private const string ENGLISH_LOCALIZATION_DATA_PATH = "Assets/Game/Resources/Data/Localizations/English.asset";
        private const int MAX_VARIANTS = 20;
        
        private LocalizedText _localizedText;
        private Dictionary<string, string> _englishLocalization;
        private List<string> _keys;
        private string _currentKey;
        private List<string> _possibleVariants = new List<string>();
        
        private void Awake()
        {
            _localizedText = target as LocalizedText;
            _englishLocalization = AssetDatabase.LoadAssetAtPath<LocalizationData>(ENGLISH_LOCALIZATION_DATA_PATH).GetLocalization();
            _keys = _englishLocalization.Keys.ToList();
            UpdateBtnText();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (!string.IsNullOrEmpty(_localizedText.Key))
            {
                if (_currentKey != _localizedText.Key)
                {
                    _possibleVariants = _keys.FindAll(key => key.ToLower().Contains(_localizedText.Key.ToLower()))
                                             .Take(MAX_VARIANTS)
                                             .ToList();
                    _currentKey = _localizedText.Key;
                }
                _possibleVariants.ForEach(DrawKeyBtn);
                if (_possibleVariants.Count >= MAX_VARIANTS)
                {
                    GUILayout.Label("...");
                }
            }
        }

        private void DrawKeyBtn(string key)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(key, GUILayout.Width(150)))
            {
                _localizedText.SetTextKey(key);
                GUI.FocusControl(null);
                UpdateBtnText();
                EditorUtility.SetDirty(target);
            }
            GUILayout.Label(_englishLocalization[key]);
            EditorGUILayout.EndHorizontal();
        }

        private void UpdateBtnText()
        {
            _localizedText.GetComponent<Text>().text = _englishLocalization.ContainsKey(_localizedText.Key)
                ? _englishLocalization[_localizedText.Key]
                : $"#{_localizedText.Key}";
        }
    }
}