using TP.Game.Controllers;
using TP.Game.Services;
using UnityEditor;

namespace TP.Editor.Views
{
    [CustomEditor(typeof(TutorialController))]
    public class TutorialControllerEditor : UnityEditor.Editor
    {
        private TutorialController _tutorialController;
        
        private void Awake()
        {
            _tutorialController = target as TutorialController;
            _tutorialController.UpdateViewsAndTrackers();
        }
    }
    
    [CustomEditor(typeof(TutorialStageView))]
    public class TutorialStageViewEditor : UnityEditor.Editor
    {
        private void Awake()
        {
            FindObjectOfType<TutorialController>()?.UpdateViewsAndTrackers();
        }
    }
    
    [CustomEditor(typeof(TutorialStageCompletionTracker))]
    public class TutorialStageCompletionTrackerViewEditor : UnityEditor.Editor
    {
        private void Awake()
        {
            FindObjectOfType<TutorialController>()?.UpdateViewsAndTrackers();
        }
    }
}