using TP.Game.Controllers;
using TP.Game.Services;
using TP.Game.Views;
using UnityEditor;

namespace TP.Editor.Views
{
    [CustomEditor(typeof(UnitView), true)]
    public class UnitViewEditor : UnityEditor.Editor
    {
        private UnitView unitView;
        
        private void Awake()
        {
            unitView = target as UnitView;
            unitView.OnEditorAwake();
        }
    }
}