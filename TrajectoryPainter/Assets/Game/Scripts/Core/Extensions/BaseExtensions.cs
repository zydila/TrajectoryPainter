﻿using System;
using TP.Game.Services;

namespace TP.Core.Extensions
{
    public static class BaseExtensions
    {
        public static string Localize(this string locKey)
        {
            return Localization.Get(locKey);
        }
        
        /// <summary>
        /// Localize text with parameters 
        /// </summary>
        /// <param name="locKey">Key</param>
        /// <param name="args">Parameters in format [i * 2] = {KEY}, [i * 2 + 1] = value</param>
        /// <returns>Localized text</returns>
        /// <exception cref="ArgumentException"></exception>
        public static string Localize(this string locKey, params string[] args)
        {
            if (args.Length % 2 != 0)
            {
                throw new ArgumentException("args count should be an even number");
            }
            var locText = Localization.Get(locKey);
            
            for (var i = 0; i < args.Length / 2; i++)
            {
                locText = locText.Replace("{" + args[i * 2] + "}", args[i * 2 + 1]);
            }

            return locText;
        }
    }
}