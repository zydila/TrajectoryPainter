using System.Collections;
using System.Collections.Generic;
using TP.Game.Data;
using UnityEngine;

namespace TP.Core.Extensions
{
    public static class MonoBehaviourExtensions 
    {
        public static void PlayClip(this AudioSource source, AudioClip clip, float volume, float pitch, bool force)
        {
            if (source.isPlaying && !force)
            {
                return;
            }
            source.clip = clip;
            source.volume = volume * Options.MasterVolume.Value;
            source.pitch = pitch;
            source.Play();
        }
    }
}