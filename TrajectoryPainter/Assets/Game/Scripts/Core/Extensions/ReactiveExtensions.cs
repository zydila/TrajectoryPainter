using System;
using System.Collections;
using System.Collections.Generic;
using TP.Game;
using UniRx;
using UnityEngine;

namespace TP.Core.Extensions
{
    public static class ReactiveExtensions
    {
        public static CompositeDisposable StartTimer(this IReactiveProperty<long> reactiveCooldown, long endTimestamp, float tickInterval, Action doOnComplete)
        {
            var cooldown = endTimestamp - GameContainer.TimeModel.CurrentTimestamp;
            var cd = new CompositeDisposable();
            if (cooldown < 0)
            {
                return cd;
            }

            reactiveCooldown.Value = cooldown;
            Observable
                .Interval(TimeSpan.FromSeconds(tickInterval))
                .StartWith(cooldown)
                .TakeWhile((e) => endTimestamp > (int) GameContainer.TimeModel.CurrentTimestamp - tickInterval)
                .Select(l => reactiveCooldown.Value = (long) Mathf.Max(0, endTimestamp - GameContainer.TimeModel.CurrentTimestamp))
                .DoOnCompleted(() =>
                {
                    if (endTimestamp <= (int) GameContainer.TimeModel.CurrentTimestamp)
                    {
                        doOnComplete?.Invoke();
                    }
                })
                .Subscribe(i => { }, () => reactiveCooldown.Value = 0)
                .AddTo(cd);
            return cd;
        }
    }
}
