using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TP.Core.Extensions
{
    public static class ReflectionExtensions
    {
        public static List<Type> FindAllDerivedTypes<T>()
        {
            return FindAllDerivedTypes<T>(Assembly.GetAssembly(typeof(T)));
        }

        private static List<Type> FindAllDerivedTypes<T>(Assembly assembly)
        {
            var derivedType = typeof(T);
            return assembly
                  .GetTypes()
                  .Where(t => t != derivedType && derivedType.IsAssignableFrom(t))
                  .ToList();
        }
    }
}