﻿using System.Collections.Generic;
using UnityEngine;

namespace TP.Core.Tools
{
    [System.Serializable]
    public class BezierPath
    {
        [SerializeField, HideInInspector] List<Vector3> points;
        [SerializeField, HideInInspector] List<float> segmentWeights; // each segment length to total path length 
        Vector3[] segmentIter = new Vector3[4];

        public int SegmentsCount
        {
            get { return points.Count / 3; }
        }

        public int PointsCount
        {
            get { return points.Count; }
        }

        public Vector3 this[int i]
        {
            get { return points[i]; }
            set
            {
                Vector3 delta = value - points[i];
                points[i] = value;

                // moving an anchor point - move tangents along by the same amount 
                if (i % 3 == 0)
                {
                    if (i + 1 < points.Count)
                        points[i + 1] += delta;
                    if (i - 1 >= 0)
                        points[i - 1] += delta;
                }
                // moving a tangent
                else
                {
                    // find pairing tangent on other side of anchor 
                    bool nextIsAnchor = (i + 1) % 3 == 0;
                    int anchor = nextIsAnchor ? i + 1 : i - 1;
                    int pairTangent = nextIsAnchor ? i + 2 : i - 2;

                    if (pairTangent >= 0 && pairTangent < points.Count)
                    {
                        // maintain the same distance from anchor to pairing tangent
                        float distance = (points[anchor] - points[pairTangent]).magnitude;
                        // keep the direction from this tangent to anchor
                        Vector3 direction = (points[anchor] - value).normalized;
                        points[pairTangent] = points[anchor] + direction * distance;
                    }
                }

                RecalculateSegmentWeights();
            }
        }

        public BezierPath(Vector3 center, float offset)
        {
            // form a cubic curve from 4 points
            points = new List<Vector3>
            {
                center + Vector3.left * offset, // anchor 
                center + (Vector3.left + Vector3.up) * 0.5f * offset, // supporting 
                center + (Vector3.right + Vector3.down) * 0.5f * offset, // supporting
                center + Vector3.right * offset // anchor 
            };
            RecalculateSegmentWeights();
        }

        public void AddSegment(Vector3 anchor)
        {
            // line from previous support and anchor extending with same length  
            points.Add(points[points.Count - 1] * 2 - points[points.Count - 2]);
            // half way between new support and final anchor
            points.Add((points[points.Count - 1] + anchor) * .5f);
            // final anchor
            points.Add(anchor);

            RecalculateSegmentWeights();
        }

        public void GetSegment(Vector3[] array, int segmentIndex)
        {
            if (array.Length != 4 || segmentIndex >= SegmentsCount || segmentIndex < 0)
                return;

            array[0] = points[segmentIndex * 3];
            array[1] = points[segmentIndex * 3 + 1];
            array[2] = points[segmentIndex * 3 + 2];
            array[3] = points[segmentIndex * 3 + 3];
        }

        public void RemoveAnchor(int i)
        {
            // not an anchor
            if (i % 3 != 0)
                return;

            if (points.Count <= 4)
                return;

            if (i == 0)
                points.RemoveRange(0, 3);
            else if (i == points.Count - 1)
                points.RemoveRange(i - 2, 3);
            else
            {
                int min = Mathf.Clamp(i - 1, 0, PointsCount - 1);
                int max = Mathf.Clamp(i + 1, 0, PointsCount - 1);
                points.RemoveRange(min, max - min + 1);
            }

            RecalculateSegmentWeights();
        }

        public Vector3 Lerp01(float lerp01)
        {
            lerp01 = Mathf.Clamp01(lerp01);
            if (lerp01 >= 1f)
                return points[points.Count - 1];

            int segmentIndex = 0;
            while (lerp01 > segmentWeights[segmentIndex])
            {
                lerp01 -= segmentWeights[segmentIndex];
                segmentIndex++;
            }

            segmentIndex = Mathf.Clamp(segmentIndex, 0, SegmentsCount - 1);
            if (segmentIter == null)
                segmentIter = new Vector3[4];

            GetSegment(segmentIter, segmentIndex);
            var segmentLerp = lerp01 / segmentWeights[segmentIndex];

            return CubicBezier(segmentIter[0], segmentIter[1], segmentIter[2], segmentIter[3], segmentLerp);
        }

        void RecalculateSegmentWeights()
        {
            if (segmentWeights == null || segmentWeights.Count != SegmentsCount)
                segmentWeights = new List<float>(SegmentsCount);

            float totalLength = 0;
            // approximation of curve length can be found here
            // https://stackoverflow.com/questions/29438398/cheap-way-of-calculating-cubic-bezier-length
            for (int i = 0; i < SegmentsCount; ++i)
            {
                GetSegment(segmentIter, i);
                float chord = Vector3.Distance(segmentIter[3], segmentIter[0]);
                float controlNet = Vector3.Distance(segmentIter[0], segmentIter[1]) +
                                   Vector3.Distance(segmentIter[2], segmentIter[1]) +
                                   Vector3.Distance(segmentIter[3], segmentIter[2]);
                float arcLength = chord + controlNet / 2;
                segmentWeights.Add(arcLength);
                totalLength += arcLength;
            }

            for (var i = 0; i < segmentWeights.Count; i++)
                segmentWeights[i] /= totalLength;
        }

        Vector3 CubicBezier(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float weight)
        {
            Vector3 abc = QuadraticBezier(a, b, c, weight);
            Vector3 bcd = QuadraticBezier(b, c, d, weight);
            return Vector3.Lerp(abc, bcd, weight);
        }

        Vector3 QuadraticBezier(Vector3 a, Vector3 b, Vector3 c, float weight)
        {
            Vector3 ab = Vector3.Lerp(a, b, weight);
            Vector3 bc = Vector3.Lerp(b, c, weight);
            return Vector3.Lerp(ab, bc, weight);
        }
    }
}