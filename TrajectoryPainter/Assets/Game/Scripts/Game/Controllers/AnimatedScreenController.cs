using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace TP.Game.Controllers
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class AnimatedScreenController : ScreenController
    {
        private SimpleAnimation _animation;
        private CancellationTokenSource _cancellation;
        
        private void Awake()
        {
            _animation = GetComponent<SimpleAnimation>();
            OnAwake();
        }
        
        protected virtual void OnAwake() { }

        protected override Task OpenInternal(bool instant)
        {
            _cancellation?.Cancel();
            if (instant)
            {
                return Task.CompletedTask;
            }
            _cancellation = new CancellationTokenSource();
            return _animation.Play("Show", _cancellation);
        }

        protected override Task CloseInternal(bool instant)
        {
            _cancellation?.Cancel();
            if (instant)
            {
                return Task.CompletedTask;
            }
            _cancellation = new CancellationTokenSource();
            return _animation.Play("Hide", _cancellation);
        }

        protected Task PlayAnimation(string animationName)
        {
            return _animation.Play(animationName, _cancellation);
        }
        
        protected override void OnDestroyInternal()
        {
            _cancellation?.Cancel();
        }
    }
}