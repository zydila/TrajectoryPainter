using TP.Core.Extensions;
using UnityEngine;

namespace TP.Game.Controllers
{
    public class AudioClipToSourcePlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip clip;
        [SerializeField] private AudioSource source;

        [SerializeField, Range(0, 1f)] private float volume = 1f;
        [SerializeField, Range(0, 2f)] private float pitch = 1f;

        public void Play(bool force)
        {
            source.PlayClip(clip, volume, pitch, force);
        }

        public void Stop()
        {
            if (source.clip == clip)
            {
                source.Stop();
            }
        }
    }
}