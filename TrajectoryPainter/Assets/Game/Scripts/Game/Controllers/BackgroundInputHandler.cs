using TP.Game.Models;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TP.Game.Controllers
{
    public class BackgroundInputHandler : MonoBehaviour, IDragHandler
    {
        [SerializeField] private float dragAmount = 45;
        
        private Camera _mainCamera;
        private Transform _cameraTransform;
        
        private void Awake()
        {
            _mainCamera = Camera.main;
            _cameraTransform = _mainCamera.transform;
        }

        public void OnDrag(PointerEventData eventData)
        {
//            if (MapCamera.AllowMoveCamera())
//            {
//                Vector3 delta = -eventData.delta / dragAmount;
//                var position = _cameraTransform.position;
//                var newPosX = Mathf.Clamp(position.x + delta.x, -MapCamera.MaxCameraMove, MapCamera.MaxCameraMove);
//                var newPosY = Mathf.Clamp(position.y + delta.y, -MapCamera.MaxCameraMove, MapCamera.MaxCameraMove);
//                var newPos = new Vector3(newPosX, newPosY, position.z);
//                _cameraTransform.position = newPos;
//            }
        }
    }

}
