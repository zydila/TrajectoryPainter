using TP.Game.Data;
using UniRx;
using UnityEngine;

namespace TP.Game.Controllers
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicController : MonoBehaviour
    {
        [SerializeField] private MusicData musicData;
        
        private AudioSource _source;
        private bool _checkMusicFinished;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
            _source.loop = true;
            StartNewMusic();
        }

        private void StartNewMusic()
        {
            _source.clip = musicData.GetRandomMusicClip();
            _source.Play();
            _checkMusicFinished = true;
        }

        void Update()
        {
            if (_checkMusicFinished && !_source.isPlaying && Application.isFocused)
            {
                _checkMusicFinished = false;
                StartNewMusic();
            }
        }
        
    }
}