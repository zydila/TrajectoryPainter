using System;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Controllers
{
    public class ScreenController : MonoBehaviour
    {
        [SerializeField] private ScreenEvents events;
        
        public IReadOnlyReactiveProperty<bool> IsOpened => _isOpened;
        public readonly ReactiveProperty<bool> _isOpened = new ReactiveProperty<bool>(false);
        private CompositeDisposable _disposable;

        public Task Open(bool instant = false)
        {
            gameObject.SetActive(true);
            _isOpened.Value = true;
            var openTask = OpenInternal(instant);
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            openTask.ToObservable()
                    .ObserveOnMainThread()
                    .Subscribe(unit => events.OnOpen?.Invoke())
                    .AddTo(_disposable);
            return openTask;
        }

        public Task Close(bool instant = false)
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            var closeTask = CloseInternal(instant);
            closeTask.ToObservable()
                     .ObserveOnMainThread()
                     .Subscribe(unit =>
                      {
                          gameObject.SetActive(false);
                          _isOpened.Value = false;
                          _disposable?.Dispose();
                      }).AddTo(_disposable);
            return closeTask;
        }

        protected virtual Task OpenInternal(bool instant)
        {
            return Task.CompletedTask;
        }

        protected virtual Task CloseInternal(bool instant)
        {
            return Task.CompletedTask;
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
            OnDestroyInternal();
        }
        
        protected virtual void OnDestroyInternal() { }
        
        [Serializable]
        public class ScreenEvents
        {
            public UnityEvent OnOpen;
        }
    }
}