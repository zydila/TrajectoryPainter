using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace TP.Game.Controllers
{
    public class ScreensController : MonoBehaviour
    {
        [SerializeField] private ScreenController defaultOpenedScreen;
        [SerializeField] private CanvasGroup canvasGroup;
        
        private static readonly ReactiveCollection<InputLock> _locks = new ReactiveCollection<InputLock>();

        public static IReadOnlyReactiveProperty<ScreenController> CurrentlyOpenScreen => _currentlyOpenScreen;
        private static readonly ReactiveProperty<ScreenController> _currentlyOpenScreen = new ReactiveProperty<ScreenController>();
        
        private static ScreensController _instance;
        
        private List<ScreenController> _screen;

        private void Awake()
        {
            _instance = this;
            _screen = GetComponentsInChildren<ScreenController>(true).ToList();
            _screen.ForEach(screen => screen.gameObject.SetActive(false));
            _locks.ObserveCountChanged().Subscribe(OnLockCountChanged).AddTo(this);
            OpenScreen(defaultOpenedScreen);
        }

        private void OnLockCountChanged(int count)
        {
            canvasGroup.blocksRaycasts = count == 0;
        }

        public static T OpenScreen<T>(bool instant = false) where T : ScreenController
        {
            return _instance?.OpenScreenInternal<T>(instant);
        }

        public void OpenScreen(ScreenController screen, bool instant = false)
        {
            _instance?.OpenScreenInternal(screen, instant);
        }

        public void OpenScreen(ScreenController screen)
        {
            _instance?.OpenScreenInternal(screen, false);
        }
        
        private T OpenScreenInternal<T>(bool instant) where T : ScreenController
        {
            var screen = _screen.Find(scr => scr.GetType() == typeof(T));
            if (screen != null)
            {
                OpenScreenInternal(screen, instant);
                return (T) screen;
            }
            return null;
        }
        
        private async void OpenScreenInternal(ScreenController screen, bool instant)
        {
            var inputLock = LockInput();
            var current = CurrentlyOpenScreen.Value;
            if (current != null)
            {
                await current.Close(instant);
            }
            _currentlyOpenScreen.Value = screen;
            await screen.Open(instant);
            inputLock.Unlock();
        }

        public static InputLock LockInput()
        {
            var inputLock = new InputLock();
            _locks.Add(inputLock);
            return inputLock;
        }

        public class InputLock
        {
            public void Unlock()
            {
                _locks.Remove(this);
            }
        }
    }
}