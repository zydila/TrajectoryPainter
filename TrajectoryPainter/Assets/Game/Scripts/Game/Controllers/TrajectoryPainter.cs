//using TP.Game.Models;
//using UnityEngine;
//using UniRx;
//
//namespace TP.Game.Controllers
//{
//    public class TrajectoryPainter : MonoBehaviour
//    {
//        public const float MAX_DISTANCE = 1f;
//        
//        private Camera _mainCamera;
//        private UnitTrajectory _trajectory;
//        private bool _trajectoryCaptured;
//        
//        private Vector3 _lastDrawnPoint = new Vector3(0, 0, -2);
//
//        private CompositeDisposable _lastKnownPointDisposable;
//
//        private LevelModel _model;
//        
//        public void Init(LevelModel model)
//        {
//            _model = model;
//            _model.SelectedUnit.Subscribe(unit =>
//            {
//                if (unit != null)
//                {
//                    CaptureTrajectory(unit.Trajectory);
//                }
//                else
//                {
//                    ReleaseTrajectory();
//                }
//            }).AddTo(this);
//        }
//        
//        private void CaptureTrajectory(UnitTrajectory trajectory)
//        {
//            _trajectory = trajectory;
//            _lastDrawnPoint = trajectory.LastDrawnPoint.Value;
//            _lastKnownPointDisposable?.Dispose();
//            _lastKnownPointDisposable = new CompositeDisposable();
//            trajectory.LastDrawnPoint
//                .Subscribe(point => _lastDrawnPoint = point)
//                .AddTo(_lastKnownPointDisposable);
//            _mainCamera = Camera.main;
//            _trajectoryCaptured = true;
//        }
//
//        private void ReleaseTrajectory()
//        {
//            _lastKnownPointDisposable?.Dispose();
//            _trajectoryCaptured = false;
//        }
//
//        void Update()
//        {
//            if (!_trajectoryCaptured)
//            {
//                return;
//            }
//
//            Vector3 inputPosition;
//#if UNITY_EDITOR
//            if (Input.mousePresent && Input.GetButton("Fire1"))
//            {
//                inputPosition = Input.mousePosition;
//            }
//#else
//            if (Input.touchCount > 0)
//            {
//                inputPosition = Input.touches[0].position;
//            }
//#endif
//            else
//            {
//                return;
//            }
//
//            var currentPoint = _mainCamera.ScreenToWorldPoint(inputPosition);
//            currentPoint.Set(currentPoint.x, currentPoint.y, UnitTrajectory.DEFAULT_Z_POSITION);
//            var distance = (currentPoint - _lastDrawnPoint).magnitude;
//            if (distance > MAX_DISTANCE)
//            {
//                currentPoint = (currentPoint - _lastDrawnPoint).normalized * MAX_DISTANCE + _lastDrawnPoint;
//                _trajectory.AddPoint.Execute(currentPoint);
//            }
//        }
//    }
//}
