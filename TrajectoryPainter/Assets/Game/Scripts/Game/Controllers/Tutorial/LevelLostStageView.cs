using TP.Game.Models;
using UniRx;

namespace TP.Game.Controllers
{
    public class LevelLostStageView : TutorialStageView
    {
        private CompositeDisposable _lostDisposable = new CompositeDisposable();
        
        protected override void OnTutorialStageChanged(int currentStage)
        {
            if (stage != currentStage)
            {
                Close();
                return;
            }

            var model = GameContainer.LevelSelectionModel.CurrentLevelModel.Value;
            if (model.State.Value == LevelModel.LevelState.Lost)
            {
                Show();
                foreach (var unit in GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Units)
                {
                    unit.ClearTrajectory.Execute();
                }
                return;
            }
            _lostDisposable?.Dispose();
            _lostDisposable = new CompositeDisposable();
            model.State
                 .Where(state => state == LevelModel.LevelState.Lost)
                 .Subscribe(state =>
                  {
                      if (stage == currentStage)
                      {
                          Show();
                          foreach (var unit in model.Units)
                          {
                              unit.ClearTrajectory.Execute();
                          }
                      }
                      else
                      {
                          Close();
                      }

                      _lostDisposable?.Dispose();
                  }).AddTo(_lostDisposable);
        }

        protected override void OnDestroyInternal()
        {
            _lostDisposable?.Dispose();
        }
    }
}