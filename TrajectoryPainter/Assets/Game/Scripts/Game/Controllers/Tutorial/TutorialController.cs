using System.Collections.Generic;
using System.Linq;
using TP.Game.Data;
using TP.Game.Services;
using UniRx;
using UnityEngine;

namespace TP.Game.Controllers
{
    public class TutorialController : MonoBehaviour
    {
        [SerializeField] private string tutorialId;
        [SerializeField] private List<TutorialStageView> views;
        [SerializeField] private List<TutorialStageCompletionTracker> trackers;

        public IReadOnlyReactiveProperty<bool> IsActive => _isActive;
        private ReactiveProperty<bool> _isActive = new ReactiveProperty<bool>(true);
        
        public IReadOnlyReactiveProperty<int> CurrentStage => _currentStage;
        private ReactiveProperty<int> _currentStage = new ReactiveProperty<int>();

        public ReactiveCommand<int> NextStage;
        public ReactiveCommand Finish;
        public ReactiveCommand Restart;

        private TutorialSaveData _save;

        private void Awake()
        {
            _save = GameContainer.PlayerModel.CrossSessionData.PlayerTutorialStage;
            _isActive.Value = _save == null || !_save.CompletedTutors.Contains(tutorialId);
            NextStage = new ReactiveCommand<int>(_isActive);
            Finish = new ReactiveCommand(_isActive);
            Restart = new ReactiveCommand(_isActive);
            NextStage.Subscribe(stage =>
            {
                if (stage == _currentStage.Value)
                {
                    _currentStage.Value += 1;
                }
            }).AddTo(this);
            Finish.Subscribe(unit =>
            {
                _isActive.Value = false;
                if (_save == null)
                {
                    _save = new TutorialSaveData();
                }

                if (!_save.CompletedTutors.Contains(tutorialId))
                {
                    _save.CompletedTutors.Add(tutorialId);
                }
                GameContainer.PlayerModel.CrossSessionData.PlayerTutorialStage = _save;
            }).AddTo(this);
            Restart.Subscribe(unit => _currentStage.Value = 0).AddTo(this);
            GameContainer.SceneContext
                         .Inited
                         .Where(inited => inited)
                         .Subscribe(inited => Init())
                         .AddTo(this);
        }

        private void Init()
        {
            views.ForEach(view => view?.Init(this));
            trackers.ForEach(tracker => tracker?.Init(this));
        }

        public void UpdateViewsAndTrackers()
        {
            views = FindObjectsOfType<TutorialStageView>(true).ToList();
            trackers = FindObjectsOfType<TutorialStageCompletionTracker>(true).ToList();
        }
    }
}