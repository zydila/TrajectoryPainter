using System;
using TP.Game.Models;
using UniRx;
using UnityEngine;

namespace TP.Game.Controllers
{
    public class TutorialStageView : MonoBehaviour
    {
        [SerializeField] protected int stage;

        private readonly ReactiveProperty<bool> _isShowing = new ReactiveProperty<bool>();
        
        private TutorialController _controller;
        protected readonly CompositeDisposable _disposable = new CompositeDisposable();

        public void Init(TutorialController controller)
        {
            _controller = controller;
            controller.CurrentStage
                      .Subscribe(OnTutorialStageChanged)
                      .AddTo(_disposable);
            controller.IsActive
                      .Subscribe(isActive => OnTutorialStageChanged(controller.CurrentStage.Value))
                      .AddTo(this);
            _isShowing.Subscribe(gameObject.SetActive)
                      .AddTo(this);
            _isShowing.Subscribe(showing =>
            {
                if (stage == 2)
                {
                    Debug.Log("showing = " + showing);
                }
            });
        }

        private void OnEnable()
        {
            if (!_isShowing.Value)
            {
                gameObject.SetActive(false);
            }
        }

        protected  virtual void OnTutorialStageChanged(int newStage)
        {
            if (!_controller.IsActive.Value)
            {
                gameObject.SetActive(false);
                _disposable?.Dispose();
                return;
            }
            
            if (stage == newStage)
            {
                Show();
            }
            else
            {
                Close();
            }
        }
        
        protected virtual void Show()
        {
            _isShowing.Value = true;
        }

        protected virtual void Close()
        {
            _isShowing.Value = false;
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
            OnDestroyInternal();
        }
        
        protected virtual void OnDestroyInternal() { }
    }
}