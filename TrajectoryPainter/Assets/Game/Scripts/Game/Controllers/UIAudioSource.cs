using TP.Game.Data;
using UniRx;
using UnityEngine;

namespace TP.Game.Controllers
{
    [RequireComponent(typeof(AudioSource))]
    public class UIAudioSource : MonoBehaviour
    {
        private static UIAudioSource _instance;

        private AudioSource _audioSource;

        private void Awake()
        {
            _instance = this;
            _audioSource = GetComponent<AudioSource>();
        }

        public static void PlaySound(AudioClip clip, float pitch, float volume, bool force = true)
        {
            if (_instance == null)
            {
                Debug.LogWarning("No UI Audio Source");
                return;
            }

            _instance.PlaySoundInternal(clip, pitch, volume, force);
        }

        private void PlaySoundInternal(AudioClip clip, float pitch, float volume, bool force = true)
        {
            _audioSource.clip = clip;
            _audioSource.pitch = pitch;
            _audioSource.volume = volume * Options.MasterVolume.Value;
            if (_audioSource.isPlaying && !force)
            {
                return;
            }
            _audioSource.Play();
        }

    }
}