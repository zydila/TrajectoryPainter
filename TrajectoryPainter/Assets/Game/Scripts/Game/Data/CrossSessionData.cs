using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using TP.Game.Models;
using UnityEngine;

namespace TP.Game.Data
{
    public class CrossSessionData
    {
        private const string PLAYER_LEVEL = "player_level";
        private const string TUTORIAL_SAVE = "tutorial_save";
        private const string LEVELS_SAVE = "levels_save";
        private const string LEVEL_PACKS_SAVE = "level_packs_save";
        private const string PLAYER_LIVES = "player_lives";
        private const string SPENT_COINS = "spent_coins";
        private const string PLAYER_LIVES_LAST_RESTORED_TIMESTAMP = "player_life_last_time_restore";

        private int playerLevel;
        private bool playerLevelLoaded;
        public int PlayerLevel
        {
            get
            {
                if (!playerLevelLoaded)
                {
                    playerLevel = PlayerPrefs.GetInt(PLAYER_LEVEL);
                    playerLevelLoaded = true;
                }

                return playerLevel;
            }
            set
            {
                if (playerLevel == value)
                {
                    return;
                }
                playerLevel = value;
                PlayerPrefs.SetInt(PLAYER_LEVEL, value);
                playerLevelLoaded = true;
                PlayerPrefs.Save();
            }
        }
        
        private int playerLives;
        private bool playerLivesLoaded;
        public int PlayerLives
        {
            get
            {
                if (!playerLivesLoaded)
                {
                    playerLives = PlayerPrefs.GetInt(PLAYER_LIVES, 3);
                    playerLivesLoaded = true;
                }

                return playerLives;
            }
            set
            {
                if (playerLives == value)
                {
                    return;
                }
                playerLives = value;
                PlayerPrefs.SetInt(PLAYER_LIVES, value);
                playerLivesLoaded = true;
                PlayerPrefs.Save();
            }
        }
        
        private TutorialSaveData playerTutorialStage;
        private bool playerTutorialStageLoaded;
        public TutorialSaveData PlayerTutorialStage
        {
            get
            {
                if (!playerTutorialStageLoaded)
                {
                    PlayerPrefsWrapper.GetObject(TUTORIAL_SAVE, out playerTutorialStage);
                    playerTutorialStageLoaded = true;
                }

                return playerTutorialStage;
            }
            set
            {
                playerTutorialStage = value;
                PlayerPrefsWrapper.SetObject(TUTORIAL_SAVE, value);
                playerTutorialStageLoaded = true;
                PlayerPrefs.Save();
            }
        }
        
        private int spentCoinAmount;
        private bool spentCoinAmountLoaded;
        public int SpentCoinAmount
        {
            get
            {
                if (!spentCoinAmountLoaded)
                {
                    spentCoinAmount = PlayerPrefs.GetInt(SPENT_COINS, 0);
                    spentCoinAmountLoaded = true;
                }

                return spentCoinAmount;
            }
            set
            {
                if (spentCoinAmount == value)
                {
                    return;
                }
                spentCoinAmount = value;
                PlayerPrefs.SetInt(SPENT_COINS, value);
                spentCoinAmountLoaded = true;
                PlayerPrefs.Save();
            }
        }
        
        private int playerLifeRestoreTimestamp;
        private bool playerLifeRestoreTimestampLoaded;
        public int PlayerLifeRestoreTimestamp
        {
            get
            {
                if (!playerLifeRestoreTimestampLoaded)
                {
                    playerLifeRestoreTimestamp = PlayerPrefs.GetInt(PLAYER_LIVES_LAST_RESTORED_TIMESTAMP);
                    playerLifeRestoreTimestampLoaded = true;
                }

                return playerLifeRestoreTimestamp;
            }
            set
            {
                if (playerLifeRestoreTimestamp == value)
                {
                    return;
                }
                playerLifeRestoreTimestamp = value;
                PlayerPrefs.SetInt(PLAYER_LIVES_LAST_RESTORED_TIMESTAMP, value);
                playerLifeRestoreTimestampLoaded = true;
                PlayerPrefs.Save();
            }
        }

        private LevelsSaveData levelsSave;
        private bool levelSaveLoaded;
        public LevelsSaveData LevelsSave
        {
            get
            {
                if (!levelSaveLoaded)
                {
                    PlayerPrefsWrapper.GetObject(LEVELS_SAVE, out levelsSave);
                    levelSaveLoaded = true;
                }

                return levelsSave;

            }
            set
            {
                levelsSave = value;
                PlayerPrefsWrapper.SetObject(LEVELS_SAVE, value);
                levelSaveLoaded = true;
                PlayerPrefs.Save();
            }
        }
        
        private LevelPacksSave levelPacksSave;
        private bool levelPacksLoaded;
        public LevelPacksSave LevelPacksSave
        {
            get
            {
                if (!levelPacksLoaded)
                {
                    PlayerPrefsWrapper.GetObject(LEVEL_PACKS_SAVE, out levelPacksSave);
                    levelPacksLoaded = true;
                }

                return levelPacksSave;

            }
            set
            {
                levelPacksSave = value;
                PlayerPrefsWrapper.SetObject(LEVEL_PACKS_SAVE, value);
                levelPacksLoaded = true;
                PlayerPrefs.Save();
            }
        }

        public void ClearData()
        {
            PlayerPrefs.DeleteAll();
        }
    }

    public static class PlayerPrefsWrapper
    {
        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }
        
        public static int GetInt(string key)
        {
            return PlayerPrefs.GetInt(key);
        }
        
        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }
        
        public static float GetFloat(string key)
        {
            return PlayerPrefs.GetFloat(key);
        }
        
        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }
        
        public static string GetString(string key)
        {
            return PlayerPrefs.GetString(key);
        }
        
        public static void SetObject(string key, object obj)
        {
            var serialized = JsonConvert.SerializeObject(obj);
            PlayerPrefs.SetString(key, serialized);
        }
        
        public static bool GetObject<T>(string key, [CanBeNull] out T result)
        {
            var objStr = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(objStr))
            {
                result = default(T);
                return false;
            }

            try
            {
                result = JsonConvert.DeserializeObject<T>(objStr);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                result = default(T);
            }

            return true;
        }
        
        public static void Save()
        {
            PlayerPrefs.Save();
        }
    }
}
