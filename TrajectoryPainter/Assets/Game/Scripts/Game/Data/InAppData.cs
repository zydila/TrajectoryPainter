using System;
using System.Collections.Generic;
using UnityEngine;

namespace TP.Game.Data
{
    [CreateAssetMenu(fileName = "InAppData", menuName = "ScriptableObjects/InAppData", order = 1)]
    public class InAppData : ScriptableObject
    {
        private const string IN_APP_DATA_PATH = "Data/InAppData";
        
        [SerializeField] private List<InAppProductData> products;
        public IReadOnlyCollection<InAppProductData> ProductsData => products;

        public static InAppData Load()
        {
            return Resources.Load<InAppData>(IN_APP_DATA_PATH);
        }
    }

    [Serializable]
    public class InAppProductData
    {
        public string Id;
        public string Sku;
        public string LocKey;
    }
}
