using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Services;
using UnityEngine;

namespace TP.Game.Data
{
    [CreateAssetMenu(fileName = "LevelsPack", menuName = "ScriptableObjects/LevelsPackData", order = 1)]
    public class LevelsPackData : ScriptableObject
    {
        [SerializeField] public string ProductId;
        [SerializeField] public List<LevelDataElement> Levels = new List<LevelDataElement>();
        public bool IsFree => string.IsNullOrEmpty(ProductId) || Product == null;

        private InAppShop.InAppProduct _product;
        public InAppShop.InAppProduct Product
        {
            get
            {
                if (_product == null)
                {
                    _product = GameContainer.InAppShop.GetProduct(ProductId);
                }
                return _product;
            }
        }

        public LevelDataElement GetLevelData(string id)
        {
            return Levels.First(level => level.Id == id);
        }
        
        public int GetLevelIndex(string id)
        {
            return Levels.FindIndex(data => data.Id == id);
        }
    }
    
    [Serializable]
    public class LevelDataElement
    {
        public string Id;
        public string NameKey;
        public int MaxScore;
    }
}
