using System.Collections.Generic;
using TP.Game.Models;
using UnityEngine;

namespace TP.Game.Data
{
    public class LevelsSaveData
    {
        public Dictionary<string, LevelResult> Scores { get; } = new Dictionary<string, LevelResult>();

        public void AddScore(LevelModel model)
        {
            AddScore(model.LevelId, model.TotalScore.Value, model.TotalTimeLeft01.Value);
        }
        
        public void AddScore(string levelName, int score, float timeLeft01)
        {
            if (Scores.ContainsKey(levelName))
            {
                Scores[levelName].Score = Mathf.Max(score, Scores[levelName].Score);
                Scores[levelName].TotalTimeLeft01 = Mathf.Max(timeLeft01, Scores[levelName].TotalTimeLeft01); //TODO: timeLeft is separate from score
            }
            else
            {
                var levelResult = new LevelResult(score, timeLeft01);
                Scores.Add(levelName, levelResult);
            }
        }
    }

    public class LevelResult
    {
        public int Score;
        public float TotalTimeLeft01;

        public LevelResult(int score, float totalTimeLeft01)
        {
            Score = score;
            TotalTimeLeft01 = totalTimeLeft01;
        }
    }
}