using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TP.Core;
using UnityEngine;

namespace TP.Game.Data
{
    public class LocalizationData : ScriptableObject
    {
        [SerializeField] private LocalizationDictionary localizationSave;

        public Dictionary<string, string> GetLocalization()
        {
            return localizationSave.AsDictionary;
        }

        public void SetLocalization(Dictionary<string, string> localization)
        {
            localizationSave = new LocalizationDictionary(localization);
        }
    }

    [Serializable]
    public class LocalizationDictionary : SerializableDictionary<string, string>
    {
        public LocalizationDictionary(IDictionary<string, string> dictionary) : base(dictionary) { }
    }
}
