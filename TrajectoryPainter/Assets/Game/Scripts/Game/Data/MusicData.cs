using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TP.Game.Data
{
    [CreateAssetMenu(fileName = "MusicData", menuName = "ScriptableObjects/MusicData", order = 1)]
    public class MusicData : ScriptableObject
    {
        private const string MUSIC_PATH = "Audio/Music/";

        [SerializeField] public List<string> musicNames = new List<string>();
            
        private int _currentMusicIndex = -1;

        public AudioClip GetRandomMusicClip()
        {
            if (musicNames.Count == 0)
            {
                throw new ArgumentNullException("No Music In Config!");
            }
            if (musicNames.Count == 1)
            {
                return Resources.Load<AudioClip>(MUSIC_PATH + musicNames[0]);
            }
            
            var randomIndex = -1;
            while (randomIndex == -1 || randomIndex == _currentMusicIndex)
            {
                randomIndex = Random.Range(0, musicNames.Count);
            }
            _currentMusicIndex = randomIndex;
            return Resources.Load<AudioClip>(MUSIC_PATH + musicNames[_currentMusicIndex]);
        }

        public void Clear()
        {
            musicNames.Clear();
        }
        
        public void AddClip(AudioClip clip)
        {
            if (!musicNames.Contains(clip.name))
            {
                musicNames.Add(clip.name);
            }
        }
        
        public void AddClipName(string clipName)
        {
            if (!musicNames.Contains(clipName))
            {
                musicNames.Add(clipName);
            }
        }
    }
}
