using System;
using TP.Game.Services;
using UniRx;
using UnityEngine;

namespace TP.Game.Data
{
    public static class Options
    {
        private const string MUSIC_VOLUME = "music_volume";
        private const string MASTER_VOLUME = "master_volume";
        private const string MUSIC_MUTED = "sound_muted";
        private const string SELECTED_LANGUAGE = "selected_language";
        
        public static IReadOnlyReactiveProperty<float> MusicVolume => _musicVolume;
        private static readonly ReactiveProperty<float> _musicVolume = new ReactiveProperty<float>(PlayerPrefs.GetFloat(MUSIC_VOLUME, 1));
        
        public static IReadOnlyReactiveProperty<float> MasterVolume => _masterVolume;
        private static readonly ReactiveProperty<float> _masterVolume = new ReactiveProperty<float>(PlayerPrefs.GetFloat(MASTER_VOLUME, 1));
        
        public static IReadOnlyReactiveProperty<bool> SoundMuted => _soundMuted;
        private static readonly ReactiveProperty<bool> _soundMuted = new ReactiveProperty<bool>(PlayerPrefs.GetInt(MUSIC_MUTED, 0) == 1);
        
        public static IReadOnlyReactiveProperty<Localization.Language> Language => _language;
        private static readonly ReactiveProperty<Localization.Language> _language = new ReactiveProperty<Localization.Language>((Localization.Language) PlayerPrefs.GetInt(SELECTED_LANGUAGE, 0));
        
        public static void SetMusicVolume(float newVolume)
        {
            if (Math.Abs(_musicVolume.Value - newVolume) > 0.005f)
            {
                _musicVolume.Value = newVolume;
                PlayerPrefs.SetFloat(MUSIC_VOLUME, newVolume);
                PlayerPrefs.Save();
            }
        }
        
        public static void SetMasterVolume(float newVolume)
        {
            if (Math.Abs(_masterVolume.Value - newVolume) > 0.005f)
            {
                _masterVolume.Value = newVolume;
                PlayerPrefs.SetFloat(MASTER_VOLUME, newVolume);
                PlayerPrefs.Save();
            }
        }
        
        public static void SetSoundMuted(bool newMuted)
        {
            if (_soundMuted.Value != newMuted)
            {
                _soundMuted.Value = newMuted;
                PlayerPrefs.SetInt(MUSIC_MUTED, newMuted ? 1 : 0);
                PlayerPrefs.Save();
            }
        }

        public static void SetLanguage(Localization.Language language)
        {
            if (_language.Value != language)
            {
                _language.Value = language;
                PlayerPrefs.SetInt(SELECTED_LANGUAGE, (int) language);
                PlayerPrefs.Save();
            }
        }
        
        private const string INITIAL_RESOLUTION_X = "resolution_x";
        private static int initialResolutionX;
        private static bool initialResolutionXLoaded;
        public static int InitialResolutionX
        {
            get
            {
                if (!initialResolutionXLoaded)
                {
                    initialResolutionX = PlayerPrefs.GetInt(INITIAL_RESOLUTION_X, 0);
                    initialResolutionXLoaded = true;
                }

                return initialResolutionX;
            }
            set
            {
                if (initialResolutionX == value)
                {
                    return;
                }
                initialResolutionX = value;
                PlayerPrefs.SetInt(INITIAL_RESOLUTION_X, value);
                initialResolutionXLoaded = true;
                PlayerPrefs.Save();
            }
        }       
        
        private const string INITIAL_RESOLUTION_Y = "resolution_y";
        private static int initialResolutionY;
        private static bool initialResolutionYLoaded;
        public static int InitialResolutionY
        {
            get
            {
                if (!initialResolutionYLoaded)
                {
                    initialResolutionY = PlayerPrefs.GetInt(INITIAL_RESOLUTION_Y, 0);
                    initialResolutionYLoaded = true;
                }

                return initialResolutionY;
            }
            set
            {
                if (initialResolutionY == value)
                {
                    return;
                }
                initialResolutionY = value;
                PlayerPrefs.SetInt(INITIAL_RESOLUTION_Y, value);
                initialResolutionYLoaded = true;
                PlayerPrefs.Save();
            }
        }
    }
}