using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TP.Game.Data
{
    public class TutorialSaveData
    {
        public List<string> CompletedTutors { get; } = new List<string>();
    }
}