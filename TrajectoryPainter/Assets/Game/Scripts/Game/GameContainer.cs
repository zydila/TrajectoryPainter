using System.Collections;
using System.Collections.Generic;
using TP.Game.Models;
using TP.Game.Services;
using UnityEngine;

namespace TP.Game
{
    public class GameContainer
    {
        public static TimeModel TimeModel { get; } = new TimeModel();
        public static InAppShop InAppShop { get; } = new InAppShop();
        
        private static PlayerModel _playerModel;
        public static PlayerModel PlayerModel
        {
            get
            {
                if (_playerModel == null)
                {
                    _playerModel = new PlayerModel();
                    _playerModel.Init();
                }

                return _playerModel;
            }
        }

        private static LevelSelectionModel _levelSelectionModel;
        public static LevelSelectionModel LevelSelectionModel
        {
            get
            {
                if (_levelSelectionModel == null)
                {
                    _levelSelectionModel = new LevelSelectionModel();
                    _levelSelectionModel.Init();
                }

                return _levelSelectionModel;
            }
        }

        public static SceneInitializer SceneContext { get; private set; }

        private static bool _inited = false;

        public static void Init(SceneInitializer sceneContext)
        {
            SceneContext = sceneContext;
            if (!_inited)
            {
                Localization.InitLocalization();
                _inited = true;
            }
        }
    }
}