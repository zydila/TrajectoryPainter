using System;
using UniRx;

namespace TP.Game.Interfaces
{
    public interface ICurrencyStorage
    {
        IReadOnlyReactiveProperty<int> CurrentAmount { get; }
        IObservable<int> Spend(int amount);
    }
}