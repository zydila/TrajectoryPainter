using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace TP.Game.Interfaces
{
    public interface ISceneClickable
    {
        void OnClick();
    }
}
