using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TP.Game.Interfaces
{
    public interface ISceneCollider
    {
        ISceneModel SceneModel { get; }
    }
}