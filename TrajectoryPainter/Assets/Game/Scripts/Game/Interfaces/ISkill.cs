using System;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.Interfaces
{
    public interface ISkill : IDisposable
    {
        IReadOnlyReactiveProperty<bool> CanBeUsed { get; }
        IReadOnlyReactiveProperty<bool> Finished { get; }
        IReadOnlyReactiveProperty<bool> Used { get; }
        IReadOnlyReactiveProperty<bool> Aborted { get; }
        ReactiveCommand Use { get; }
        ReactiveCommand Abort { get; }
        ReactiveCommand Reset { get; }
        SkillViewConfig ViewConfig { get; }
    }

    public struct SkillViewConfig
    {
        public Color Color;
        public string NameKey;
        public string DescriptionKey;
        public string LocKeyParameter;

        public SkillViewConfig(Color color, string nameKey, string descriptionKey)
        {
            Color = color;
            NameKey = nameKey;
            DescriptionKey = descriptionKey;
            LocKeyParameter = "";
        }
        
        public SkillViewConfig(SkillComponent skillComponent)
        {
            Color = skillComponent.Color;
            DescriptionKey = skillComponent.DescriptionKey;
            NameKey = skillComponent.NameKey;
            LocKeyParameter = (skillComponent is ActiveSkillComponent activeSkillComponent)
                ? activeSkillComponent.ActiveTime.ToString("0:00")
                : "";
        }
    }

    public interface ICollisionSkill : ISkill
    {
        bool Collide(ISceneModel sceneModel);
    }
    
    public class SkillAttribute : Attribute
    {
        public readonly Type SkillType;

        public SkillAttribute(Type type)
        {
            SkillType = type;
        }
    }
}
