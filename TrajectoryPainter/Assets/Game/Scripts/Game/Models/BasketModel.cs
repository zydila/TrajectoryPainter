using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Interfaces;
using TP.Game.Services;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.Models
{
    public class BasketModel : IDisposable, ISceneModel
    {
        public readonly IReadOnlyCollection<UnitModel.UnitType> Types;
        public readonly IReadOnlyReactiveProperty<bool> Filled;
        public readonly ReactiveCommand<UnitModel> ConsumeUnit = new ReactiveCommand<UnitModel>();

        private readonly ReactiveProperty<int> _consumedAmount = new ReactiveProperty<int>(0);
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        public BasketModel(BasketView view)
        {
            Types = view.UnitTypes;
            if (Types.Count == 0)
            {
                Types = Enum.GetValues(typeof(UnitModel.UnitType))
                            .Cast<UnitModel.UnitType>()
                            .ToList();
            }

            Filled = _consumedAmount.Select(amount => amount > 0)
                                    .ToReadOnlyReactiveProperty(false);
            ConsumeUnit.Subscribe(unit => _consumedAmount.Value++)
                       .AddTo(_disposable);
        }
        
        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}