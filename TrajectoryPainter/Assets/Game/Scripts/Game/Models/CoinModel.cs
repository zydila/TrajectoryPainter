using System;
using TP.Game.Interfaces;
using UniRx;

namespace TP.Game.Models
{
    public class CoinModel : ISceneModel, IDisposable
    {
        public Type CoinType { get; private set; }
        
        public IReadOnlyReactiveProperty<int> Collected => _collected;
        private readonly ReactiveProperty<int> _collected = new ReactiveProperty<int>();
        public readonly IReadOnlyReactiveProperty<bool> IsCollected;

        public ReactiveCommand Collect;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private int _coinValue;

        public CoinModel(int value, Type type)
        {
            CoinType = type;
            _coinValue = value;
            var collected = _collected.Select(amount => amount == 0);
            Collect = new ReactiveCommand(collected);
            Collect.Subscribe(unit => _collected.Value = _coinValue)
                .AddTo(_disposable);
            IsCollected = _collected.Select(count => count > 0)
                                    .ToReadOnlyReactiveProperty();
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State
                         .Where(state => state == LevelModel.LevelState.Prepare)
                         .Subscribe(prepare => _collected.Value = 0)
                         .AddTo(_disposable);
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
        
        public enum Type
        {
            Basic
        }
    }
}
