using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Controllers;
using TP.Game.Services;
using TP.Game.UI.Screens;
using TP.Game.Views;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TP.Game.Models
{
    public class LevelModel : IDisposable
    {
        public string LevelId { get; private set; }
        public float TimeLimit { get; private set; } = 5f;
        
        public ReactiveCommand StartLevel { get; } = new ReactiveCommand(GameContainer.PlayerModel.HasLives);
        public ReactiveCommand Restart { get; } = new ReactiveCommand();
        public ReactiveCommand<float> ChangeCurrentTimeLerp01 { get; } = new ReactiveCommand<float>();
        public ReactiveCommand<bool> FinishLevel { get; } = new ReactiveCommand<bool>();
        public ReactiveCommand ExitLevel { get; }

        public IReadOnlyReactiveProperty<UnitModel> SelectedUnit => _selectedUnit;
        private readonly ReactiveProperty<UnitModel> _selectedUnit = new ReactiveProperty<UnitModel>();

        public IReadOnlyReactiveProperty<float> CurrentTime { get; private set; }
        
        public IReadOnlyReactiveProperty<float> CurrentTimeLerp => _currentTimeLerp;
        private readonly ReactiveProperty<float> _currentTimeLerp = new ReactiveProperty<float>();
        
        public IReadOnlyReactiveProperty<float> TotalTimeLeft01;
        
        public IReadOnlyReactiveProperty<int> TotalScore => _totalScore;
        private readonly ReactiveProperty<int> _totalScore = new ReactiveProperty<int>();
        
        public IReadOnlyReactiveProperty<bool> LevelFinished;
        
        public IReadOnlyReactiveProperty<LevelState> State => _state;
        private readonly ReactiveProperty<LevelState> _state = new ReactiveProperty<LevelState>(LevelState.Prepare);

        private LevelCompleteService _levelCompleteService;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private CompositeDisposable _levelCompletionDisposable = new CompositeDisposable();
        private CompositeDisposable _selectedUnitDisposable = new CompositeDisposable();
        
        public IReadOnlyList<UnitModel> Units => _units;
        public IReadOnlyList<CoinModel> Coins => _coins;
        private List<UnitModel> _units;
        private List<CoinModel> _coins;
        private List<BasketModel> _baskets;

        public LevelModel(string levelId, float timeLimit)
        {
            LevelId = levelId;
            TimeLimit = timeLimit;
            CurrentTime = CurrentTimeLerp.Select(lerp => lerp * TimeLimit).ToReadOnlyReactiveProperty();
            LevelFinished = _state.Select(state => state == LevelState.Finished || state == LevelState.Lost)
                                  .ToReadOnlyReactiveProperty();
            ExitLevel = new ReactiveCommand();
            SelectedUnit.Subscribe(unit =>
            {
                _selectedUnitDisposable?.Dispose();
                _selectedUnitDisposable = new CompositeDisposable();
                unit?.IsSelected.Subscribe(selected =>
                {
                    if (!selected)
                    {
                        _selectedUnit.Value = null;
                        _selectedUnitDisposable?.Dispose();
                    }
                }).AddTo(_selectedUnitDisposable);
            });
            ChangeCurrentTimeLerp01.Subscribe(lerp => _currentTimeLerp.Value = lerp).AddTo(_disposable);
            Restart
                .Subscribe(unit =>
                {
//                    Dispose();
//                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                    _state.Value = LevelState.Prepare;
                    _totalScore.Value = 0;
                    ChangeCurrentTimeLerp01.Execute(0);
                })
                .AddTo(_disposable);
            StartLevel.Subscribe(unit => _state.Value = LevelState.InProgress).AddTo(_disposable);
            FinishLevel.Subscribe(result => _state.Value = result ? LevelState.Finished : LevelState.Lost).AddTo(_disposable);
            FinishLevel.Subscribe(result => _totalScore.Value = result ? _totalScore.Value : 0).AddTo(_disposable);
            ExitLevel.Subscribe(unit => SceneLoader.LoadScene<LevelSelectScreen>("GameLevels"));

            GameContainer.LevelSelectionModel.LevelLoaded.Execute(this);
        }
        
        public void Init(List<UnitModel> units, List<CoinModel> coins, List<BasketModel> baskets)
        {
            _units = units;
            _coins = coins;
            _baskets = baskets;
            foreach (var unitModel in units)
            {
                unitModel.IsSelected.Subscribe(selected =>
                {
                    if (selected)
                    {
                        _selectedUnit.Value = unitModel;
                    }
                }).AddTo(_disposable);
                unitModel.TrajectoryModel
                         .TotalTrajectoryTime
                         .Subscribe(time => ChangeCurrentTimeLerp01.Execute(time / TimeLimit))
                         .AddTo(_disposable);
                unitModel.TrajectoryModel
                         .Capture
                         .Subscribe(unit => ChangeCurrentTimeLerp01.Execute(unitModel.TrajectoryModel.TotalTrajectoryTime.Value / TimeLimit))
                         .AddTo(_disposable);
            }

            TotalTimeLeft01 = units.Select(unit => unit.TrajectoryModel.TrajectoryTimeLeft01)
                                   .CombineLatest()
                                   .Select(times => times.Sum() / 2)
                                   .ToReadOnlyReactiveProperty(0);
            
            foreach (var coinModel in coins)
            {
                coinModel.Collected
                    .Subscribe(coinValue => _totalScore.Value += coinValue)
                    .AddTo(_disposable);
            }

            _state.Subscribe(state =>
            {
                if (state == LevelState.InProgress)
                {
                    _levelCompletionDisposable?.Dispose();
                    _levelCompletionDisposable = new CompositeDisposable();
                    _levelCompleteService = new LevelCompleteService();
                    ChangeCurrentTimeLerp01.Execute(0);
                    _levelCompleteService.Completed(this, units, baskets)
                        .Subscribe(result => FinishLevel.Execute(result))
                        .AddTo(_levelCompletionDisposable);
                }
            }).AddTo(_disposable);
        }
        
        public enum LevelState
        {
            Prepare,
            InProgress,
            Lost,
            Finished
        }

        public void Dispose()
        {
            _disposable?.Dispose();
            _selectedUnitDisposable?.Dispose();
            _coins?.ForEach(coin => coin.Dispose());
            _units?.ForEach(unit => unit.Dispose());
        }
    }
}
