using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Controllers;
using TP.Game.Data;
using TP.Game.Services;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TP.Game.Models
{
    public class LevelSelectionModel : IDisposable
    {
        private const string LEVEL_DATA = "Data/LevelPacks/";

        public LevelPackType SelectedPackType { get; private set; }
        
        public IReactiveCommand<string> StartLevel { get; private set; }
        public IReactiveCommand<LevelPackType> SelectPack { get; } = new ReactiveCommand<LevelPackType>();
        public IReactiveCommand<LevelPackType> BuyPack { get; } = new ReactiveCommand<LevelPackType>();
        public IReactiveCommand<LevelModel> LevelLoaded { get;  private set;}
        public IReactiveCommand<string> BuyLevel { get; } = new ReactiveCommand<string>();
        public ReactiveCommand StartNextLevel { get; private set; }

        public IReadOnlyReactiveProperty<LevelModel> CurrentLevelModel => _currentLevelModel;
        private readonly ReactiveProperty<LevelModel> _currentLevelModel = new ReactiveProperty<LevelModel>();
        
        public IReadOnlyReactiveProperty<int> CurrentLevel => _currentLevel;
        private readonly ReactiveProperty<int> _currentLevel = new ReactiveProperty<int>();
        
        public LevelsPackData LevelsPackDataValue => _levelsData.Value;
        public IReadOnlyReactiveProperty<LevelsPackData> LevelsData => _levelsData;
        private readonly ReactiveProperty<LevelsPackData> _levelsData = new ReactiveProperty<LevelsPackData>();
        
        public IReadOnlyReactiveProperty<LevelsSaveData> LevelsSaveData => _levelsSaveData;
        private readonly ReactiveProperty<LevelsSaveData> _levelsSaveData = new ReactiveProperty<LevelsSaveData>();
        
        public IReadOnlyReactiveProperty<LevelPacksSave> PacksSaveData => _packsSaveData;
        private readonly ReactiveProperty<LevelPacksSave> _packsSaveData = new ReactiveProperty<LevelPacksSave>();

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private CompositeDisposable _levelFinishDisposable = new CompositeDisposable();

        public Dictionary<LevelPackType, LevelsPackData> PacksData { get; } = new Dictionary<LevelPackType, LevelsPackData>();

        public void Init()
        {
            Load();
            StartLevel = new ReactiveCommand<string>(GameContainer.PlayerModel.HasLives);
            LevelLoaded = new ReactiveCommand<LevelModel>();
            var nextLevelCanBeExecuted = _currentLevelModel.Where(level => level != null)
                                                           .Select(level => LevelsPackDataValue.GetLevelIndex(level.LevelId) < LevelsPackDataValue.Levels.Count - 1)
                                                           .CombineLatest(GameContainer.PlayerModel.HasLives, (currentLevelNotLast, hasLives) => currentLevelNotLast && hasLives)
                                                           .ToReadOnlyReactiveProperty();
            StartNextLevel = new ReactiveCommand(nextLevelCanBeExecuted);
            StartLevel.Subscribe(id =>
                       {
                           if (LevelsData.Value.GetLevelIndex(id) < _currentLevel.Value + 1)
                           {
                               SceneLoader.LoadScene(id);
                           }
                           else
                           {
                               throw new LevelLockedException(LevelsData.Value.GetLevelIndex(id), CurrentLevel.Value);
                           }
                       })
                      .AddTo(_disposable);
            SelectPack.Subscribe(OnSelectPack).AddTo(_disposable);
            SelectPack.Execute(LevelPackType.BasicLevelPack);
            
            StartNextLevel.Subscribe(unit => StartLevel.Execute(LevelsPackDataValue.Levels[LevelsPackDataValue.GetLevelIndex(_currentLevelModel.Value.LevelId) + 1].Id))
                          .AddTo(_disposable);
            LevelLoaded.Subscribe(OnLevelLoaded).AddTo(_disposable);
            BuyLevel.Subscribe(OnBuyLevel).AddTo(_disposable);
            BuyPack.Subscribe(OnBuyPack).AddTo(_disposable);
            
            _levelsSaveData.Subscribe(data => GameContainer.PlayerModel.CrossSessionData.LevelsSave = data).AddTo(_disposable);
            _packsSaveData.Subscribe(data => GameContainer.PlayerModel.CrossSessionData.LevelPacksSave = data).AddTo(_disposable);
            _currentLevel.Subscribe(level =>
                          {
                              _packsSaveData.Value.SetProgress(SelectedPackType, level);
                              _packsSaveData.SetValueAndForceNotify(_packsSaveData.Value);
                          })
                         .AddTo(_disposable);
        }

        private void Load()
        {
            foreach (var pack in Enum.GetValues(typeof(LevelPackType)).Cast<LevelPackType>())
            {
                var data = Resources.Load<LevelsPackData>(LEVEL_DATA + pack);
                if (data != null)
                {
                    PacksData.Add(pack, data);
                }
            }
            
            _levelsSaveData.Value = GameContainer.PlayerModel.CrossSessionData.LevelsSave ?? new LevelsSaveData();
            _packsSaveData.Value = GameContainer.PlayerModel.CrossSessionData.LevelPacksSave ?? new LevelPacksSave();
        }

        private void OnLevelLoaded(LevelModel model)
        {
            _currentLevelModel.Value = model;
            _levelFinishDisposable?.Dispose();
            _levelFinishDisposable = new CompositeDisposable();
            model.FinishLevel
                 .Where(result => result)
                 .Subscribe(result =>
                  {
                      if (LevelsPackDataValue.GetLevelIndex(model.LevelId) == _currentLevel.Value)
                      {
                          _currentLevel.Value += 1;
                      }
                      _levelsSaveData.Value.AddScore(model);
                      _levelsSaveData.SetValueAndForceNotify(_levelsSaveData.Value);
                  })
                 .AddTo(_levelFinishDisposable);
            model.ExitLevel
                 .Subscribe(value =>
                  {
                      _currentLevelModel.Value = null;
                      _levelFinishDisposable?.Dispose();
                  })
                 .AddTo(_levelFinishDisposable);
        }

        private void OnBuyLevel(string levelId)
        {
            CompositeDisposable spendCoinsDisposable = new CompositeDisposable();
            GameContainer.PlayerModel
                         .CoinStorage
                         .Spend(LevelsPackDataValue.GetLevelData(levelId).MaxScore)
//                           .Catch<string, NotEnoughCoinsException>(ex => {})
                         .DoOnError(ex =>
                          {
                              Debug.LogWarning(ex.Message);
                              spendCoinsDisposable?.Dispose();
                          })
                         .DoOnCompleted(() =>
                          {
                              _currentLevel.Value = GameContainer.LevelSelectionModel.LevelsPackDataValue.GetLevelIndex(levelId);
                              _levelsSaveData.Value.AddScore(levelId, 0, 0);
                              _levelsSaveData.SetValueAndForceNotify(_levelsSaveData.Value);
                              spendCoinsDisposable?.Dispose();
                          })
                         .Subscribe()
                         .AddTo(spendCoinsDisposable); 
        }

        private void OnBuyPack(LevelPackType pack)
        {
            var uiLock = ScreensController.LockInput();
            GameContainer.InAppShop
                         .PurchaseProduct(PacksData[pack].Product)
                         .DoOnError(exception =>
                          {
                              uiLock.Unlock();
                              Debug.LogError($"BuyPackError {exception.Message}");
                          })
                         .DoOnCompleted(() =>
                          {
                              uiLock.Unlock();
                              _packsSaveData.Value.SetProgress(pack, 0);
                              _packsSaveData.SetValueAndForceNotify(_packsSaveData.Value);
                          }).Subscribe();
        }

        private void OnSelectPack(LevelPackType pack)
        {
            if (!PacksData[pack].IsFree && !_packsSaveData.Value.IsPackUnlocked(pack))
            {
                throw new PackLockedException(pack);
            }
            SelectedPackType = pack;
            _levelsData.Value = PacksData[pack];
            _currentLevel.Value = _packsSaveData.Value.GetPackProgress(pack);
        }

        public void Dispose()
        {
            _disposable.Dispose();
        }
    }

    public class LevelLockedException : Exception
    {
        public LevelLockedException(int levelNum, int unlockedLevel) : base($"You are trying to start locked level {levelNum}, unlocked {unlockedLevel}") { }
    }

    public class PackLockedException : Exception
    {
        public PackLockedException(LevelPackType type) : base($"You are trying to access locked level pack {type}") { }
    }


    public class LevelPackSave
    {
        public bool IsUnlocked = false;
        public int currentLevel = 0;

        public LevelPackSave(bool isUnlocked)
        {
            IsUnlocked = isUnlocked;
        }
    }

    public class LevelPacksSave
    {
        public Dictionary<LevelPackType, LevelPackSave> packsProgress = new Dictionary<LevelPackType, LevelPackSave>();

        public void SetProgress(LevelPackType type, int level)
        {
            if (!packsProgress.ContainsKey(type))
            {
                packsProgress.Add(type, new LevelPackSave(true));
            }

            packsProgress[type].currentLevel = level;
        }

        public int GetPackProgress(LevelPackType type)
        {
            if (!packsProgress.ContainsKey(type))
            {
                return 0;
            }

            return packsProgress[type].currentLevel;
        }

        public bool IsPackUnlocked(LevelPackType type)
        {
            if (!packsProgress.ContainsKey(type))
            {
                return false;
            }

            return packsProgress[type].IsUnlocked;
        }
    }

    public enum LevelPackType
    {
        BasicLevelPack,
        AdvancedLevelPack,
        ThirdPack
    }
}