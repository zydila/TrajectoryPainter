using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TP.Game.Models
{
    public static class MapCamera
    {
        public static float MaxCameraMove;
        private static bool _allowMoveCamera;
        private static bool _allowMoveCameraSet;
        private static float _referenceRatio = 9f / 16f;
        private const float MAX_ALLOWED_RATIO = 0.35f;
        private const float MAX_ALLOWED_CAMERA_MOVE = 2.3f;
        
        public static bool AllowMoveCamera()
        {
            if (_allowMoveCameraSet)
            {
                return _allowMoveCamera;
            }
            var screenRatio = (float) Screen.width / Screen.height;
            _allowMoveCamera = _referenceRatio - screenRatio > 0.05;
            _allowMoveCameraSet = true;
            MaxCameraMove = MAX_ALLOWED_CAMERA_MOVE / (screenRatio / MAX_ALLOWED_RATIO);
            return _allowMoveCamera;
        }
    }
}
