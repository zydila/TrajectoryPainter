using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Tools;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TP.Game.Models
{
    [Serializable]
    public struct AnimationKey
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;
        public int Frame;
        public bool Valid;
    }
    
    [ExecuteInEditMode]
    public class ObstacleAnimation : MonoBehaviour
    {
        [SerializeField]
        private List<AnimationKey> animationKeys = new List<AnimationKey>();
        
        public List<AnimationKey> AnimationKeys => animationKeys;

        [SerializeField] private int numOfFrames;
        public int NumOfFrames => numOfFrames;

        private AnimationKey _cachedKey = new AnimationKey();

        public void SetStartKey()
        {
            if (animationKeys.Any(key => key.Frame == 0))
            {
                AddAnimationKey(transform, 0);
            }
        }
        
        private void AddAnimationKey(AnimationKey key)
        {
            animationKeys.Add(key);
            animationKeys = animationKeys.OrderBy(animationKey => animationKey.Frame).ToList();
        }
        
        public void AddAnimationKey(Transform trans, float lerp)
        {
            var frame = Mathf.CeilToInt(numOfFrames * lerp);
            var existingKeyIndex = animationKeys.FindIndex(checkKey => checkKey.Frame == frame);
            var key = new AnimationKey
            {
                Position = trans.position, 
                Rotation = trans.rotation, 
                Scale = trans.localScale,
                Frame = frame,
                Valid = true
            };
            
            if (existingKeyIndex >= 0 && animationKeys[existingKeyIndex].Valid)
            {
                animationKeys[existingKeyIndex] = key;
            }
            else
            {
                AddAnimationKey(key);
            }
        }

        public void SetNumOfFrames(int numOfFrames)
        {
            this.numOfFrames = numOfFrames;
        }

        public AnimationKey LerpAnimation01(float lerp)
        {
            var closestFrame = Mathf.CeilToInt(lerp * numOfFrames);
            var closestKeyIndex = -1;
            _cachedKey.Valid = false;
            for (var i = 0; i < animationKeys.Count; i++)
            {
                if (closestFrame <= animationKeys[i].Frame)
                {
                    closestKeyIndex = i;
                    break;
                }
            }

            if (closestKeyIndex == -1)
            {
                return _cachedKey;
            }
            
            _cachedKey.Valid = true;
            if (closestKeyIndex == 0)
            {
                return animationKeys[0];
            }

            var targetFrameLerp = (float) animationKeys[closestKeyIndex].Frame / numOfFrames;
            var previousFrameLerp = (float) animationKeys[closestKeyIndex - 1].Frame / numOfFrames;
            var localLerp = (lerp - previousFrameLerp) / (targetFrameLerp - previousFrameLerp);
            return LerpBetweenKeys(closestKeyIndex - 1, closestKeyIndex, localLerp);
        }

        private AnimationKey LerpBetweenKeys(int index1, int index2, float lerp)
        {
            _cachedKey.Position = Vector3.Lerp(animationKeys[index1].Position, animationKeys[index2].Position, lerp);
            _cachedKey.Scale = Vector3.Lerp(animationKeys[index1].Scale, animationKeys[index2].Scale, lerp);
            _cachedKey.Rotation = Quaternion.LerpUnclamped(animationKeys[index1].Rotation, animationKeys[index2].Rotation, lerp);
            return _cachedKey;
        }

        public void RemoveKey(int keyFrame)
        {
            animationKeys.RemoveAll(key => key.Frame == keyFrame);
        }

        public void SetCurrentPositionForAllKeyFrames()
        {
            animationKeys?.ForEach(key => key.Position = transform.position);
        }

        public void SetCurrentScaleForAllKeyFrames()
        {
            animationKeys?.ForEach(key => key.Scale = transform.localScale);
        }

        public void SetCurrentRotationForAllKeyFrames()
        {
            animationKeys?.ForEach(key => key.Rotation = transform.rotation);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ObstacleAnimation))]
    public class ObstacleAnimationEditor : Editor
    {
        private ObstacleAnimation _animation;
        private LevelCreator _creator;
        
        private void Awake()
        {
            _animation = target as ObstacleAnimation;
            _creator = FindObjectOfType<LevelCreator>();
            _animation.SetStartKey();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Add Key"))
            {
                _animation.AddAnimationKey(_animation.transform, _creator.cache.TimeLerp);
            }
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Set Position For All Frames"))
            {
                _animation.SetCurrentPositionForAllKeyFrames();
            }
            if (GUILayout.Button("Set Scale For All Frames"))
            {
                _animation.SetCurrentScaleForAllKeyFrames();
            }
            if (GUILayout.Button("Set Rotation For All Frames"))
            {
                _animation.SetCurrentRotationForAllKeyFrames();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label($"Current {Mathf.CeilToInt(_creator.cache.TimeLerp * _animation.NumOfFrames)}", GUILayout.Width(100));
            _creator.cache.TimeLerp = GUILayout.HorizontalSlider(_creator.cache.TimeLerp, 0, 1, GUILayout.Height(25));
            if (Mathf.Abs(_creator.cache.TimeLerpCache - _creator.cache.TimeLerp) > 0.01)
            {
                _creator.LerpTime(_creator.cache.TimeLerp);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            foreach (var key in _animation.AnimationKeys)
            {
                EditorGUILayout.BeginVertical();
                if (GUILayout.Button("-", GUILayout.Width(15)))
                {
                    _animation.RemoveKey(key.Frame);
                    break;
                }
                if (GUILayout.Button(key.Frame.ToString(), GUILayout.Width(50)))
                {
                    _creator.LerpTime((float) key.Frame / _animation.NumOfFrames);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("Level Creator"))
            {
                Selection.activeObject = _creator.gameObject;
            }
        }
    }
#endif
}