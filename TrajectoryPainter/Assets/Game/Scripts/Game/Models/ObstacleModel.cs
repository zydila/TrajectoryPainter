using System;
using TP.Game.Interfaces;
using UniRx;

namespace TP.Game.Models
{
    public class ObstacleModel : ISceneModel, IDisposable
    {
        public Type ObstacleType { get; }
        public IReadOnlyReactiveProperty<bool> Active => _active;
        private readonly ReactiveProperty<bool> _active = new ReactiveProperty<bool>(true);
        public ReactiveCommand Kill { get; } = new ReactiveCommand();

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        
        public ObstacleModel(Type type)
        {
            ObstacleType = type;
            Kill.Subscribe(unit => _active.Value = false)
                .AddTo(_disposable);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State.Subscribe(state =>
            {
                if (state == LevelModel.LevelState.Prepare)
                {
                    _active.Value = true;
                }
            }).AddTo(_disposable);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ExitLevel
                         .Subscribe(unit => Dispose())
                         .AddTo(_disposable);
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
        
        public enum Type
        {
            Basic,
            Border
        }
    }
}
