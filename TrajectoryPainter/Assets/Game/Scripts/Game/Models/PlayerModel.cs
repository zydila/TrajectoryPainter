using System;
using System.Linq;
using TP.Core.Tools;
using TP.Game.Data;
using TP.Game.Interfaces;
using TP.Game.Services;
using UniRx;
using UnityEngine;

namespace TP.Game.Models
{
    public class PlayerModel
    {
        private const int MAX_LIVES = 3;
        private const int LIFE_RESTORE_TIME = 30;
        
        public IReadOnlyReactiveProperty<int> PlayerLives => _playerLives;
        public IReadOnlyReactiveProperty<bool> HasLives;
        public IReactiveProperty<TimerModel> LivesTimer => _livesTimer;

        private readonly ReactiveProperty<int> _playerLives = new ReactiveProperty<int>();
        private readonly ReactiveProperty<int> _restoreLifeTimestamp = new ReactiveProperty<int>();
        private readonly ReactiveProperty<TimerModel> _livesTimer = new ReactiveProperty<TimerModel>();

        private ReactiveCommand LoadPlayerCommand { get; set; }
        private ReactiveCommand AddLife { get;  set;}
        
        public ICurrencyStorage CoinStorage { get; private set; }
        public CrossSessionData CrossSessionData => _crossSessionData;
        private readonly CrossSessionData _crossSessionData = new CrossSessionData();
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private CompositeDisposable _levelFinishDisposable = new CompositeDisposable();
        private CompositeDisposable _restoreLivesDisposable = new CompositeDisposable();
        
        public ReactiveCommand AddLifeDebug { get; private set; }
        public ReactiveCommand AddLevelDebug { get; private set; }
        public ReactiveCommand MinusLevelDebug { get; private set; }
        public ReactiveCommand ClearSaveData { get; private set; }

        public void Init()
        {
            HasLives = _playerLives.Select(lives => lives > 0)
                                   .ToReadOnlyReactiveProperty();
            LoadPlayerCommand = new ReactiveCommand();
            AddLife = new ReactiveCommand(_playerLives.Select(lives => lives < MAX_LIVES));
            LoadPlayerCommand.DoOnError(ex => { })
                             .Subscribe(LoadPlayer)
                             .AddTo(_disposable);
            AddLife.Subscribe(unit => OnAddLife())
                   .AddTo(_disposable);
            LoadPlayerCommand.Execute();
            _restoreLifeTimestamp.Subscribe(restoreLifeTimestamp => _crossSessionData.PlayerLifeRestoreTimestamp = restoreLifeTimestamp)
                                 .AddTo(_disposable);
            _playerLives.Subscribe(lives => _crossSessionData.PlayerLives = lives)
                        .AddTo(_disposable);
            _playerLives.Where(lives => lives < MAX_LIVES)
                        .Subscribe(OnPlayerLivesChange)
                        .AddTo(_disposable);
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Where(level => level != null)
                         .Subscribe(SetCurrentLevel)
                         .AddTo(_disposable);
            CoinStorage = new CoinStorage(this);
            if (Debug.isDebugBuild)
            {
                InitDebug();
            }
        }

        private void SetCurrentLevel(LevelModel levelModel)
        {
            _levelFinishDisposable?.Dispose();
            _levelFinishDisposable = new CompositeDisposable();
//            levelModel?.State
//                       .Where(state => state == LevelModel.LevelState.InProgress)
//                       .Subscribe(inProgress => _playerLives.Value -= 1)
//                       .AddTo(_levelFinishDisposable);
                
            levelModel?.FinishLevel
                       .Where(result => !result)
                       .Subscribe(result => _playerLives.Value -= 1)
                       .AddTo(_levelFinishDisposable);
        }

        private void OnPlayerLivesChange(int lives)
        {
            if (_livesTimer.Value == null || _livesTimer.Value.Completed)
            {
                _restoreLivesDisposable?.Dispose();
                _restoreLivesDisposable = new CompositeDisposable();
                _livesTimer.Value?.Dispose();

                if (GameContainer.TimeModel.CurrentTimestamp >= _restoreLifeTimestamp.Value)
                {
                    _restoreLifeTimestamp.Value =
                        (int) GameContainer.TimeModel.CurrentTimestamp + LIFE_RESTORE_TIME;
                }

                _livesTimer.Value = new TimerModel((long) _restoreLifeTimestamp.Value);
                _livesTimer.Value
                           .CompletedObservable
                           .Where(completed => completed)
                           .Subscribe(completed =>
                            {
                                _restoreLivesDisposable?.Dispose();
                                _livesTimer.Value?.Dispose();
                                _livesTimer.Value = null;
                                AddLife.Execute();
                            }).AddTo(_restoreLivesDisposable);
            }
        }

        private void OnAddLife()
        {
            _playerLives.Value = Mathf.Min(_playerLives.Value + 1, MAX_LIVES);
            if (_playerLives.Value == MAX_LIVES)
            {
                _livesTimer.Value?.Dispose();
                _livesTimer.Value = null;
            }
        }

        private void LoadPlayer(Unit unit)
        {
            _playerLives.Value = _crossSessionData.PlayerLives;
            _restoreLifeTimestamp.Value = _crossSessionData.PlayerLifeRestoreTimestamp;
            var timeLeftSinceGameClosed = GameContainer.TimeModel.CurrentTimestamp - _restoreLifeTimestamp.Value;
            if (_restoreLifeTimestamp.Value > 0  && timeLeftSinceGameClosed > 0)
            {
                for (int i = 0; i < timeLeftSinceGameClosed / LIFE_RESTORE_TIME; i++)
                {
                    AddLife.Execute();
                }
            }
        }
        
        private void InitDebug()
        {
            AddLifeDebug = new ReactiveCommand();
            AddLevelDebug = new ReactiveCommand();
            MinusLevelDebug = new ReactiveCommand();
            ClearSaveData = new ReactiveCommand();

            AddLifeDebug.Subscribe(unit => AddLife.Execute()).AddTo(_disposable);
            ClearSaveData.Subscribe(unit =>
            {
                CrossSessionData.ClearData();
                LoadPlayerCommand.Execute();
            }).AddTo(_disposable);
        }
    }
}