using System;
using System.Threading.Tasks;
using TP.Game.Interfaces;
using TP.Game.UI.Elements;
using TP.Game.Views;
using UniRx;
using SkillView = TP.Game.UI.Elements.SkillView;

namespace TP.Game.Models
{
    public abstract class ActiveBaseSkill : ISkill
    {
        protected double ActiveTime { get; }

        public IReadOnlyReactiveProperty<bool> Active => _active;
        public IReadOnlyReactiveProperty<bool> CanBeUsed { get; }
        public IReadOnlyReactiveProperty<bool> Finished { get; }
        public IReadOnlyReactiveProperty<bool> Used => _used;
        public IReadOnlyReactiveProperty<bool> Aborted => _aborted;
        public ReactiveCommand Use { get; }
        public ReactiveCommand Abort { get; }
        public ReactiveCommand Reset { get; }
        public SkillViewConfig ViewConfig { get; }

        private readonly ReactiveProperty<bool> _used = new ReactiveProperty<bool>();
        private readonly ReactiveProperty<bool> _aborted = new ReactiveProperty<bool>();
        private readonly ReactiveProperty<bool> _active = new ReactiveProperty<bool>();
        
        protected readonly CompositeDisposable _disposable = new CompositeDisposable();
        private CompositeDisposable _activeDisposable = new CompositeDisposable();
        
        protected ActiveBaseSkill(SkillComponent skillComponent)
        {
            ViewConfig = new SkillViewConfig(skillComponent);
            Reset = new ReactiveCommand();
            ActiveTime = (skillComponent as ActiveSkillComponent).ActiveTime;
            CanBeUsed = _used
                       .CombineLatest(GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State
                                                   .Select(state => state == LevelModel.LevelState.InProgress),
                            (used, levelStarted) => !used && levelStarted)
                       .ToReadOnlyReactiveProperty();
            
            Use = new ReactiveCommand(CanBeUsed);
            Abort = new ReactiveCommand(_active);
            Abort.Subscribe(unit =>
            {
                _aborted.Value = true;
                _active.Value = false;
                _used.Value = false;
                _activeDisposable?.Dispose();
            }).AddTo(_disposable);
            Use.Subscribe(unit =>
            {
                _active.Value = true;
                _used.Value = true;
                _activeDisposable?.Dispose();
                _activeDisposable = new CompositeDisposable();
                Task.Delay((int) (ActiveTime * 1000))
                    .ToObservable()
                    .ObserveOnMainThread()
                    .Subscribe(unit => _active.Value = false)
                    .AddTo(_activeDisposable);
            }).AddTo(_disposable);
            _active.Where(active => active)
                   .Subscribe(active => _activeDisposable?.Dispose())
                   .AddTo(_disposable);
            Reset.Subscribe(unit =>
            {
                _aborted.Value = false;
                _active.Value = false;
                _used.Value = false;
                _activeDisposable?.Dispose();
            }).AddTo(_disposable);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State
                         .Where(state => state == LevelModel.LevelState.Prepare).Subscribe(prepare => Reset.Execute())
                         .AddTo(_disposable);
            Finished = Active
                      .CombineLatest(Used, (active, used) => !active && used)
                      .ToReadOnlyReactiveProperty();
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}
