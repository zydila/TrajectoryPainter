using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP.Game.Interfaces;
using TP.Game.UI.Elements;
using TP.Game.Views;
using UniRx;
using UnityEngine;
using SkillView = TP.Game.UI.Elements.SkillView;

namespace TP.Game.Models
{
    public class InvincibleSkill : ActiveBaseSkill, ICollisionSkill
    {
//        private List<ObstacleModel.Type> _obstacleTypes;
//        
        public InvincibleSkill(ActiveSkillComponent skillComponent) : base(skillComponent)
        {
        }
//        {
//            if (skillComponent is InvincibilitySkillView skill && skill.ObstacleTypes.Count > 0)
//            {
//                _obstacleTypes = skill.ObstacleTypes;
//                return;
//            }
//
//            _obstacleTypes = Enum.GetValues(typeof(ObstacleModel.Type)).Cast<ObstacleModel.Type>().ToList();//obstacleTypes;
//        }
        
        public bool Collide(ISceneModel sceneModel)
        {
            if (Active.Value && sceneModel is ObstacleModel)
            {
                return true;
            }

            return false;
        }
    }
}