using TP.Game.Interfaces;
using TP.Game.Views;

namespace TP.Game.Models
{
    public class KillObstaclesOnTouchSkill : ActiveBaseSkill, ICollisionSkill
    {
//        private List<ObstacleModel.Type> _obstacleTypes;
        
        public KillObstaclesOnTouchSkill(KillObstaclesSkillComponent skillComponent) : base(skillComponent)
        {
//            if (skillComponent is KillObstaclesSkillView skill && skill.ObstacleTypes.Count > 0)
//            {
//                _obstacleTypes = skill.ObstacleTypes;
//                return;
//            }
//
//            _obstacleTypes = Enum.GetValues(typeof(ObstacleModel.Type)).Cast<ObstacleModel.Type>().ToList();
        }
        
        public bool Collide(ISceneModel sceneModel)
        {
            if (sceneModel is ObstacleModel obstacleModel)// &&  _obstacleTypes.Contains(obstacleModel.ObstacleType))
            {
                obstacleModel.Kill.Execute();
                return true;
            }

            return false;
        }
    }
}