using TP.Game.Interfaces;
using TP.Game.UI.Elements;
using TP.Game.Views;
using SkillView = TP.Game.UI.Elements.SkillView;

namespace TP.Game.Models
{
    public class ShrinkSkill : ActiveBaseSkill
    {
        public double SkillTime => ActiveTime;
        
        public ShrinkSkill(SkillComponent skillComponent) : base(skillComponent) { }
    }
}