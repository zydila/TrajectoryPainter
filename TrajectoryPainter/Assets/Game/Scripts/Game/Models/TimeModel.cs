using System;

namespace TP.Game.Models
{
    public class TimeModel
    {
        public long CurrentTimestamp => DateTimeOffset.Now.ToUnixTimeSeconds();
    }
}