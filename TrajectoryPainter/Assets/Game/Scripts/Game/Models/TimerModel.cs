using System;
using TP.Core.Extensions;
using UniRx;
using UnityEngine;

namespace TP.Game.Models
{
    public class TimerModel : IDisposable
    {
        public static int Count;
        public int Index;
        
        public readonly ReactiveProperty<long> TimeLeft = new ReactiveProperty<long>(0);
        public IObservable<bool> CompletedObservable { get; }
        public bool Completed { get; private set; }
        
        private readonly long _endTimestamp;
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        public TimerModel(long endTimestamp, float tickInterval = 1f)
        {
            Index = Count;
            Count++;
            _endTimestamp = endTimestamp;
            TimeLeft.StartTimer(_endTimestamp, tickInterval, null)
                    .AddTo(_disposable);
            CompletedObservable = TimeLeft
                                 .Where(timeLeft => timeLeft <= 0)
                                 .Select(timeLeft => true)
                                 .Take(1);
            CompletedObservable.Where(completed => completed)
                               .Subscribe(completed => Completed = completed)
                               .AddTo(_disposable);
            CompletedObservable.Subscribe(value => Debug.Log(value)).AddTo(_disposable);
        }

        public TimerModel(int duration, float tickInterval = 1f)
        {
            _endTimestamp = GameContainer.TimeModel.CurrentTimestamp + duration;
            CompletedObservable = TimeLeft.Select(timeLeft => timeLeft <= 0);
            CompletedObservable.Where(completed => completed).Subscribe(completed => Completed = completed);
            TimeLeft.StartTimer(_endTimestamp, tickInterval, null)
                    .AddTo(_disposable);
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}