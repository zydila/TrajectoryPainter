using System;
using System.Collections.Generic;
using System.Linq;
using TP.Core.Tools;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.Models
{
    public class TrajectoryModel : IDisposable
    {
        public const float DEFAULT_Z_POSITION = 2f;
        public const float SEGMENT_SIZE = 0.15f;
        public const float TIME_FOR_POINT = 0.2f;
        
        public IReadOnlyReactiveCollection<Vector3> TrajectoryPoints => _trajectoryPoints;
        private readonly ReactiveCollection<Vector3> _trajectoryPoints = new ReactiveCollection<Vector3>();

        public IReadOnlyReactiveProperty<List<Vector3>> SmoothedPoints { get; }
        
        private readonly ReactiveProperty<List<List<Vector3>>> _smoothedPointsSegments = new ReactiveProperty<List<List<Vector3>>>();

        public IReadOnlyReactiveProperty<Vector3> LastDrawnPoint => _lastDrawnPoint;
        private readonly ReactiveProperty<Vector3> _lastDrawnPoint = new ReactiveProperty<Vector3>();

        public readonly IReadOnlyReactiveProperty<float> TotalTrajectoryTime;
        public readonly IReadOnlyReactiveProperty<float> TrajectoryTimeLeft01;

        public IReactiveCommand<Vector3> AddPoint => _addPoint;
        public IReactiveCommand<Vector3> CurrentDesirablePoint => _currentDesirablePoint;
        public readonly ReactiveCommand Capture = new ReactiveCommand();
        private readonly ReactiveCommand<Vector3> _addPoint;
        private readonly ReactiveCommand<Vector3> _currentDesirablePoint;
        private readonly IReadOnlyReactiveProperty<int> _trajectoryPointsCount;
        
        public IReactiveCommand<int> RemoveAllPointsAfterIndex => _removeAllPointsAfterIndex;
        private readonly ReactiveCommand<int> _removeAllPointsAfterIndex = new ReactiveCommand<int>();

        public readonly UnitModel UnitModel;
        private float _trajectoryTimeLimit; 
        private float _timeLimitRatio; 
       
        private int _maxNumOfPoints;
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private readonly List<Vector3> _trajectoryPointsCache = new List<Vector3>();

        public TrajectoryModel(UnitModel unitModel, float timeLimit)
        {
            UnitModel = unitModel;
            _trajectoryTimeLimit = timeLimit > 0.1f ? 
                                       Mathf.Min(GameContainer.LevelSelectionModel.CurrentLevelModel.Value.TimeLimit, timeLimit) : 
                                       GameContainer.LevelSelectionModel.CurrentLevelModel.Value.TimeLimit;
            _timeLimitRatio = GameContainer.LevelSelectionModel.CurrentLevelModel.Value.TimeLimit / _trajectoryTimeLimit;
            _maxNumOfPoints = Mathf.CeilToInt(_trajectoryTimeLimit / TIME_FOR_POINT) + 1;
            var canAddPoints =_trajectoryPoints.ObserveCountChanged().Select(count => count < _maxNumOfPoints);
            _addPoint = new ReactiveCommand<Vector3>(canAddPoints);
            _currentDesirablePoint = new ReactiveCommand<Vector3>(canAddPoints);
            AddPoint.Subscribe(AddTrajectoryPoint)
                    .AddTo(_disposable);
            TrajectoryPoints
                .ObserveCountChanged()
                .Subscribe(lastAddedPoint => _lastDrawnPoint.Value = TrajectoryPoints.Last())
                .AddTo(_disposable);
            _trajectoryPointsCount = TrajectoryPoints.ObserveCountChanged()
                                                     .ToReadOnlyReactiveProperty();
            _removeAllPointsAfterIndex
                .Subscribe(RemoveTrajectoryPoints)
                .AddTo(_disposable);
            AddPoint.Execute(UnitModel.Position.Value);
            SmoothedPoints = _smoothedPointsSegments
                            .Where(segments => segments != null && segments.Count > 0)
                            .Select(segments =>
                             {
                                 var smoothedPoints = new List<Vector3>();
                                 segments.ForEach(smoothedPoints.AddRange);
                                 return smoothedPoints;
                             }).ToReadOnlyReactiveProperty();
            TrajectoryPoints.ObserveCountChanged()
                .Subscribe(count =>
                {
                    _trajectoryPointsCache.Clear();
                    _trajectoryPoints.ToList().ForEach(_trajectoryPointsCache.Add);
                    _smoothedPointsSegments.Value = LineSmoother.SmoothLineWithSegments(_trajectoryPoints.ToList(), SEGMENT_SIZE);
                })
                .AddTo(_disposable);
            CurrentDesirablePoint.Subscribe(point =>
            {
                if (_trajectoryPointsCache.Count > _trajectoryPointsCount.Value)
                {
                    _trajectoryPointsCache[_trajectoryPointsCache.Count - 1] = point;
                }
                else
                {
                    _trajectoryPointsCache.Add(point);
                }
                _smoothedPointsSegments.Value = LineSmoother.SmoothLineWithSegments(_trajectoryPointsCache, SEGMENT_SIZE);
            });
            TotalTrajectoryTime = SmoothedPoints
                .Where(points => points != null)
                .Select(points =>
                {
                    var time = Mathf.Max(_trajectoryPoints.Count - 1, 0) * TIME_FOR_POINT;
                    time += (_lastDrawnPoint.Value - _trajectoryPointsCache[_trajectoryPointsCache.Count - 1]).magnitude * TIME_FOR_POINT / TrajectoryView.MAX_DISTANCE;
                    return time;
                })
                .ToReadOnlyReactiveProperty(0);
            TrajectoryTimeLeft01 = TotalTrajectoryTime
                                  .Select(trajectoryTime => 1 - trajectoryTime / _trajectoryTimeLimit)
                                  .ToReadOnlyReactiveProperty(1);
        }
        
        private void AddTrajectoryPoint(Vector3 point)
        {
            _trajectoryPoints.Add(new Vector3(point.x, point.y, DEFAULT_Z_POSITION));
        }

        public Vector3 Lerp01(float lerp)
        {
            lerp = Mathf.Clamp01(_timeLimitRatio * lerp);
            if (SmoothedPoints.Value == null)
            {
                return _trajectoryPoints[0];
            }


            var desiredIndexUnclamped = lerp * (_maxNumOfPoints - 1);
            var desiredIndex = Mathf.FloorToInt(desiredIndexUnclamped);
            if (desiredIndex >= _trajectoryPoints.Count - 1)
            {
                return SmoothedPoints.Value.Last();
            }

            var smoothLerp = desiredIndexUnclamped - desiredIndex;
            var smoothedIndexUnclamped = smoothLerp * (_smoothedPointsSegments.Value[desiredIndex].Count - 1);
            var smoothedIndex = Mathf.FloorToInt(smoothedIndexUnclamped);
            
            if (smoothedIndex + 1 >= _smoothedPointsSegments.Value[desiredIndex].Count)
            {
                return _smoothedPointsSegments.Value[desiredIndex].Last();
            }
            
            return Vector3.Lerp(_smoothedPointsSegments.Value[desiredIndex][smoothedIndex], _smoothedPointsSegments.Value[desiredIndex][smoothedIndex + 1], smoothedIndexUnclamped - smoothedIndex);
        }

        private void RemoveTrajectoryPoints(int indexRemoveFrom)
        {
            for (var i = _trajectoryPoints.Count - 1; i >= indexRemoveFrom; i--)
            {
                _trajectoryPoints.RemoveAt(indexRemoveFrom);
            }
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}