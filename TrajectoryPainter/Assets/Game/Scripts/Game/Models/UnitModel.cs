using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Interfaces;
using TP.Game.Services;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.Models
{
    public class UnitModel : IDisposable, ISceneModel
    {
        public IReadOnlyReactiveProperty<Vector3> Position => _position;
        private readonly ReactiveProperty<Vector3> _position = new ReactiveProperty<Vector3>();
        
        public readonly IReadOnlyReactiveProperty<bool> DetectCollisions;
        
        public IReadOnlyReactiveProperty<UnitState> State => _state;
        private readonly ReactiveProperty<UnitState> _state = new ReactiveProperty<UnitState>(UnitState.Alive);

        public IReadOnlyReactiveProperty<bool> IsSelected => _isSelected;
        private readonly ReactiveProperty<bool> _isSelected = new ReactiveProperty<bool>();
        
        public readonly IReadOnlyReactiveProperty<bool> IsCollidingWithObstacle;
        private readonly ReactiveCollection<ISceneModel> _collidingObstacles = new  ReactiveCollection<ISceneModel>();
        
        public readonly TrajectoryModel TrajectoryModel;
        public readonly int Priority;
        public readonly UnitType Type;
        
        public ReactiveCommand Die { get; }
        public ReactiveCommand Consume { get; }
        public ReactiveCommand<ISceneModel> Collision { get; }
        public ReactiveCommand<ISceneModel> CollisionExit { get; }
        public ReactiveCommand<float> LerpMoveAlongTrajectory { get; }
        private ReactiveCommand Reset { get; }

        private IReadOnlyReactiveProperty<List<ISkill>> _activeSkills;
        private List<ISkill> _skills = new List<ISkill>();
        public List<ISkill> Skills => _skills;

        public ReactiveCommand Select { get; }
        public ReactiveCommand Deselect { get; }
        public ReactiveCommand ClearTrajectory { get; }

        private readonly List<ObstacleModel.Type> _obstacleTypes;
        private readonly List<CoinModel.Type> _coinTypes;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        
        public UnitModel(UnitView unitView, int priority)
        {
            Priority = priority;
            Type = unitView.Type;
            _obstacleTypes = unitView.ObstacleTypes;
            if (!_obstacleTypes.Contains(ObstacleModel.Type.Border))
            {
                _obstacleTypes.Add(ObstacleModel.Type.Border);
            }
            _coinTypes = unitView.CoinTypes;
            var canExecuteSelectCommands = State.Select(value => value == UnitState.Alive);
            LerpMoveAlongTrajectory = new ReactiveCommand<float>(canExecuteSelectCommands);
            Select = new ReactiveCommand(canExecuteSelectCommands);
            Deselect = new ReactiveCommand(canExecuteSelectCommands);
            Die = new ReactiveCommand(canExecuteSelectCommands);
            Collision = new ReactiveCommand<ISceneModel>(canExecuteSelectCommands);
            CollisionExit = new ReactiveCommand<ISceneModel>(canExecuteSelectCommands);
            Consume = new ReactiveCommand(canExecuteSelectCommands);
            Reset = new ReactiveCommand();
            ClearTrajectory = new ReactiveCommand(GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State.Select(state => state != LevelModel.LevelState.InProgress));

            ClearTrajectory.Subscribe(unit => TrajectoryModel.RemoveAllPointsAfterIndex.Execute(1))
                           .AddTo(_disposable);
            Select.Subscribe(unit => _isSelected.Value = true)
                .AddTo(_disposable);
            Deselect.Subscribe(unit => _isSelected.Value = false)
                .AddTo(_disposable);
            LerpMoveAlongTrajectory.Subscribe(lerpValue => _position.Value = TrajectoryModel.Lerp01(lerpValue))
                .AddTo(_disposable);
            Die.Subscribe(unit => _state.Value = UnitState.Dead)
                .AddTo(_disposable);
            _position.Value = unitView.transform.position;
            TrajectoryModel = new TrajectoryModel(this, unitView.TimeLimit);
            var levelModel = GameContainer.LevelSelectionModel.CurrentLevelModel.Value;
            levelModel.CurrentTimeLerp
                      .Subscribe(lerp => _position.Value = TrajectoryModel.Lerp01(lerp))
                      .AddTo(_disposable);
            DetectCollisions = levelModel.State
                                         .Select(levelState => levelState == LevelModel.LevelState.InProgress)
                                         .ToReadOnlyReactiveProperty();
            Consume.Subscribe(unit => _state.Value = UnitState.Consumed).AddTo(_disposable);
            Collision.Subscribe(collider =>
            {
                if (GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State.Value == LevelModel.LevelState.InProgress)
                {
                    LiveCollisionHandler(collider);
                }
                else
                {
                    PlaningCollisionHandler(collider);
                }
            }).AddTo(_disposable);
            CollisionExit.Subscribe(collider =>
            {
                if (GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State.Value != LevelModel.LevelState.InProgress)
                {
                    
                }
                _collidingObstacles.Remove(collider);
            }).AddTo(_disposable);
            IsCollidingWithObstacle = _collidingObstacles.ObserveCountChanged()
                                          .Select(count => count > 0)
                                          .ToReadOnlyReactiveProperty();
            State.Subscribe(state =>
            {
                switch (state)
                {
                    case UnitState.Alive:
                        _collidingObstacles.Clear();
                        break;
                    case UnitState.Consumed:
                    case UnitState.Dead:
                        AbortAllSkills();
                        break;
                }
            }).AddTo(_disposable);
            unitView.Init(this);
            InitSkills(unitView.Skills);
            Reset.Subscribe(unit =>
                  {
                      _state.Value = UnitState.Alive;
                  })
                 .AddTo(_disposable);
            levelModel.State
                      .Where(state => state == LevelModel.LevelState.Prepare)
                      .Subscribe(prepare => Reset.Execute())
                      .AddTo(_disposable);
            levelModel.State
                      .Where(state => state == LevelModel.LevelState.Finished || state == LevelModel.LevelState.Lost)
                      .Subscribe(prepare => AbortAllSkills())
                      .AddTo(_disposable);
        }

        private void InitSkills(List<SkillComponent> skillComponents)
        {
            if (skillComponents == null || skillComponents.Count == 0)
            {
                return;
            }
            var builder = new SkillBuilder();
            foreach (var skillView in skillComponents)
            {
                var skill = builder.BuildSkill(skillView);
                _skills.Add(skill);
                skillView.SetSkill(skill);
            }
            _activeSkills = _skills.Where(skill => skill is ActiveBaseSkill)
                                   .Cast<ActiveBaseSkill>()
                                   .Select(skill => skill.Active)
                                   .CombineLatest()
                                   .Select(skills => _skills.Where(skill => skill is ActiveBaseSkill activeSkill && activeSkill.Active.Value).ToList())
                                   .ToReadOnlyReactiveProperty();
        }

        private void AbortAllSkills()
        {
            _skills.ForEach(skill => skill.Abort.Execute());
        }

        public void Dispose()
        {
            _disposable?.Dispose();
            TrajectoryModel?.Dispose();
            _skills?.ForEach(skill => skill?.Dispose());
            _skills = null;
        }

        private void PlaningCollisionHandler(ISceneModel sceneCollider)
        {
            if (sceneCollider is ObstacleModel obstacleModel)
            {
                if (_obstacleTypes.Contains(obstacleModel.ObstacleType))
                {
                    _collidingObstacles.Add(sceneCollider);
                }
            }
        }

        private void LiveCollisionHandler(ISceneModel sceneCollider)
        {
            if (sceneCollider is CoinModel coinModel)
            {
                if (_coinTypes.Contains(coinModel.CoinType))
                {
                    coinModel.Collect.Execute();
                }
            }
            
            if (sceneCollider is UnitModel unitModel)
            {
                if (unitModel.Priority > Priority)
                {
                    Consume.Execute();
                }
            }
            
            if (sceneCollider is BasketModel basketModel)
            {
                if (basketModel.Types.Contains(Type))
                {
                    basketModel.ConsumeUnit.Execute(this);
                    Consume.Execute();
                }
            }
            
            if (_activeSkills != null && _activeSkills.HasValue && _activeSkills.Value != null)
            {
                foreach (var skill in _activeSkills.Value)
                {
                    if (skill is ICollisionSkill collisionSkill)
                    {
                        if (collisionSkill.Collide(sceneCollider))
                        {
                            return;
                        }
                    }
                }
            }

            if (sceneCollider is ObstacleModel obstacleModel)
            {
                if (_obstacleTypes.Contains(obstacleModel.ObstacleType))
                {
                    _collidingObstacles.Add(sceneCollider);
                    Die.Execute();
                }
            }
        }

        public enum UnitState
        {
            Alive,
            Consumed,
            Dead
        }

        public enum UnitType
        {
            Yellow,
            Red,
            Blue,
        }
    }
    
}