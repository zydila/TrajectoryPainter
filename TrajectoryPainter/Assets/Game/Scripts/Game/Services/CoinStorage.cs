using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public class CoinStorage : ICurrencyStorage
    {
        private PlayerModel _playerModel;
        public IReadOnlyReactiveProperty<int> CurrentAmount => _totalCoins;
        public ReactiveProperty<int> _totalCoins = new ReactiveProperty<int>();
        
        public CoinStorage(PlayerModel playerModel)
        {
            _playerModel = playerModel;
            GameContainer.LevelSelectionModel
                         .LevelsSaveData
                         .Where(save => save != null)
                         .Select(save => save.Scores.Values.Sum(score => score.Score) - _playerModel.CrossSessionData.SpentCoinAmount)
                         .Subscribe(coins => _totalCoins.Value = coins);
        }
        
        public IObservable<int> Spend(int amount)
        {
            return Observable.Create<int>(observer =>
            {
                if (_totalCoins.Value < amount)
                {
                    observer.OnError(new NotEnoughCoinsException());
                }

                _playerModel.CrossSessionData.SpentCoinAmount += amount;
                _totalCoins.Value -= amount;
                observer.OnNext(_totalCoins.Value);
                observer.OnCompleted();
                return Disposable.Empty;
            });
        }
    }

    public class NotEnoughCoinsException : Exception
    {
        public NotEnoughCoinsException() : base("Not Enough coins")
        {
        }
    }
}