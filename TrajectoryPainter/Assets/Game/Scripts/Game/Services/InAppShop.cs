using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP.Game.Data;
using TP.Game.UI.Elements;
using UniRx;

namespace TP.Game.Services
{
    public class InAppShop
    {
        public IReadOnlyCollection<InAppProduct> Products => _products;
        private readonly List<InAppProduct> _products = new List<InAppProduct>();

        private bool _purchaseInProgress;
        
        public InAppShop()
        {
            foreach (var inAppProductData in InAppData.Load().ProductsData)
            {
                _products.Add(new InAppProduct(inAppProductData.Id, inAppProductData.Sku, inAppProductData.LocKey, 1,"USD"));
            }
        }

        public InAppProduct GetProduct(string id)
        {
            return _products.FirstOrDefault(product => product.Id == id);
        }
        
        public IObservable<bool> PurchaseProduct(InAppProduct product)
        {
            if (_purchaseInProgress)
            {
                return Observable.Return(false);
            }
            var purchase = Observable.Create<bool>(observer => new PurchasingSubscription(observer, product));
            UILoadingIcon.Show();
            purchase.DoOnError(ex => _purchaseInProgress = false)
                    .DoOnCompleted(() => _purchaseInProgress = false)
                    .Finally(UILoadingIcon.Hide)
                    .Subscribe();
            return purchase;
        }

        private class PurchasingSubscription : IDisposable
        {
            private IObserver<bool> _observer;
            private InAppProduct _product;
            
            public PurchasingSubscription(IObserver<bool> observer, InAppProduct product)
            {
                _observer = observer;
                _product = product;
                StartPurchase();
            }

            private async void StartPurchase()
            {
                if (_product == null)
                {
                    _observer.OnError(new ArgumentException("Product is null"));
                    _observer = null;
                    Dispose();
                    return;
                }
                await Task.Delay(1000);
                _observer.OnNext(true);
                _observer.OnCompleted();
                Dispose();
            }
            
            public void Dispose()
            {
                _observer = null;
            }
        }

        public class InAppProduct
        {
            public readonly string Id;
            public readonly string Sku;
            public readonly string LocKey;
            public readonly int LocalizedPrice;
            public readonly string ISO;

            public InAppProduct(string id, string sku, string locKey, int localizedPrice, string iso)
            {
                Id = id;
                Sku = sku;
                LocKey = locKey;
                LocalizedPrice = localizedPrice;
                ISO = iso;
            }

            public string LocalizePrice()
            {
                return LocalizedPrice + Localization.Get(ISO);
            }
        }
    }
}