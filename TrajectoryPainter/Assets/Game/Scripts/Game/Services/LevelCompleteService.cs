using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Models;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public class LevelCompleteService
    {
        private IReadOnlyReactiveProperty<bool> _result;
        private IReadOnlyReactiveProperty<bool> _hasResult;
        private IReadOnlyReactiveProperty<bool> _unitDied;
        private IReadOnlyReactiveProperty<bool> _unitsConsumed;
        private IReadOnlyReactiveProperty<bool> _started;
        private IReadOnlyReactiveProperty<bool> _levelFinished;
        private IReadOnlyReactiveProperty<bool> _allBasketsFilled;
//        private static int _inter;
        
        public IObservable<bool> Completed(LevelModel levelModel, List<UnitModel> units, List<BasketModel> baskets)
        {
            _started = levelModel.State
                                 .Select(state => state == LevelModel.LevelState.InProgress)
                                 .ToReadOnlyReactiveProperty(false);

            _levelFinished = levelModel.CurrentTime
                                       .Select(time => Mathf.Abs(levelModel.TimeLimit - Mathf.Min(time, levelModel.TimeLimit)) < 0.0049f)
                                       .CombineLatest(_started, (timePassed, started) => started && timePassed)
                                       .ToReadOnlyReactiveProperty(false);
            _allBasketsFilled = baskets.Select(basket => basket.Filled)
                                       .CombineLatest()
                                       .Select(allFilled => allFilled.All(filled => filled))
                                       .ToReadOnlyReactiveProperty(false);
            
//            _unitDied = units.Select(unit => unit.State)
//                .ToObservable()
//                .Where(state => state.Value == UnitModel.UnitState.Dead)
//                .Take(1)
//                .Select(state => true)
//                .ToReadOnlyReactiveProperty();

            _unitDied = units.Select(unit => unit.State)
                             .CombineLatest()
                             .Select(states => states.Count(state => state == UnitModel.UnitState.Dead) > 0)
                             .ToReadOnlyReactiveProperty(false);

            _unitsConsumed = units.Select(unit => unit.State)
                                  .CombineLatest()
                                  .Select(states => states.Count(state => state == UnitModel.UnitState.Consumed))
                                  .Select(numOfConsumed => numOfConsumed == units.Count)
                                  .ToReadOnlyReactiveProperty(false);

            _result = _levelFinished.CombineLatest(_unitDied, _unitsConsumed, _allBasketsFilled, 
                                         (levelFinished, unitDied, consumed, basketsFilled) => !levelFinished && !unitDied && consumed && basketsFilled)
                                    .ToReadOnlyReactiveProperty(false);

            _hasResult = _levelFinished.CombineLatest(_unitDied, _unitsConsumed, (levelFinished, unitDied, consumed) => levelFinished || unitDied || consumed)
                                       .ToReadOnlyReactiveProperty(false);

//            _started.Subscribe(res => Debug.Log($"_started {res} {_inter++}"));
//            _result.Subscribe(res => Debug.Log($"_result {res} {_inter++}"));
//            _unitsConsumed.Subscribe(res => Debug.Log($"_unitsConsumed {res} {_inter++}"));
//            _unitDied.Subscribe(res => Debug.Log($"_unitDied {res} {_inter++}"));
//            _levelFinished.Subscribe(res => Debug.Log($"_levelFinished {res} {_inter++}"));
//            _hasResult.Subscribe(res => Debug.Log($"_hasResult {res} {_inter++}"));

            return Observable.Create<bool>(observer =>
            {
                return _started.CombineLatest(_hasResult, (started, hasResult) => started && hasResult)
                               .Where(hasResult => hasResult)
                               .Take(1)
                               .Subscribe(hasResult => observer.OnNext(_result.Value));
            });
        }
    }
}