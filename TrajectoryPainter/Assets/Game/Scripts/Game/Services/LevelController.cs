using TP.Game.Models;
using UnityEngine;
using UniRx;

namespace TP.Game.Services
{
    public class LevelController : MonoBehaviour
    {
        [SerializeField] private float timeDivider = 1f;
        
        private LevelModel _levelModel;
        private bool _levelInprogress;
        private bool _levelPaused;
        private float _timeSinceLevelStarted;

        public void Init(LevelModel model)
        {
            _levelModel = model;
            _levelModel.State
                       .Select(state => state == LevelModel.LevelState.InProgress)
                       .Subscribe(inProgress =>
                        {
                            _levelInprogress = inProgress;
                            if (!inProgress)
                            {
                                _timeSinceLevelStarted = 0;
                            }
                        }).AddTo(this);
        }

        // Update is called once per frame
        void Update()
        {
            if (_levelInprogress)
            {
                var lerpTime = _timeSinceLevelStarted / _levelModel.TimeLimit;
                _levelModel.ChangeCurrentTimeLerp01.Execute(lerpTime);
                _timeSinceLevelStarted += Time.deltaTime / timeDivider;
            }
        }
    }
}