using System.Collections.Generic;
using System.Linq;
using TP.Game.Controllers;
using TP.Game.Models;
using TP.Game.Views;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TP.Game.Services
{
    public class LevelSceneInitializer : SceneInitializer
    {
        [SerializeField, HideInInspector] public string levelId;
        [SerializeField] private float _levelTimeLimit = 5f;
        [SerializeField] private LevelController _levelController;
        [SerializeField, HideInInspector] private List<UnitView> _sphereUnits;
        [SerializeField, HideInInspector] private List<ObstacleView> _obstacles;
        [SerializeField, HideInInspector] private List<CoinView> _coins;
        [SerializeField, HideInInspector] private List<BasketView> _baskets;
        [SerializeField] private int maxScore;
        [SerializeField] private bool developmentInProgress = true;

        [SerializeField] private Transform trajectoriesRoot;
        [SerializeField] private TrajectoryView trajectoryViewPrefab;
        
        public List<UnitView> Units => _sphereUnits;
        public List<ObstacleView> Obstacles => _obstacles;
        public float TimeLimit => _levelTimeLimit;
        public int MaxScore => maxScore;
        public bool DevelopmentInProgress => developmentInProgress;

        public static LevelSceneInitializer Instance { get; private set; }

        public LevelModel LevelModel { get; private set; }

        protected override void InitInternal()
        {
            Instance = this;
            LevelModel = new LevelModel(levelId, _levelTimeLimit);
            Init();
            var units = new List<UnitModel>();
            var index = 0;
            _sphereUnits.ForEach(view =>
            {
                var unit = new UnitModel(view, view.Priority);
                index++;
                units.Add(unit);
                var trajectory = Instantiate(trajectoryViewPrefab, trajectoriesRoot);
                trajectory.transform.localPosition = view.transform.position;
                trajectory.Init(unit.TrajectoryModel, view.DefaultColor);
            });
//            var obstacles = new List<ObstacleModel>();
            _obstacles.ForEach(view => 
            {
                var obstacle = new ObstacleModel(view.Type);
                view.Init(obstacle);
//                obstacles.Add(obstacle);
            });
            var coins = new List<CoinModel>();
            _coins.ForEach(view =>
            {
                var coinModel = new CoinModel(view.Value, view.Type);
                view.Init(coinModel);
                coins.Add(coinModel);
            });
            var baskets = new List<BasketModel>();
            _baskets.ForEach(view =>
            {
                var basket = new BasketModel(view);
                view.Init(basket);
                baskets.Add(basket);
            });
//            _painter.Init(levelModel);
            LevelModel.Init(units, coins, baskets);
            _levelController.Init(LevelModel);
        }

        public void Init()
        {
            _sphereUnits = FindObjectsOfType<UnitView>().Where(view => view.gameObject.activeSelf).ToList();
            _obstacles = FindObjectsOfType<ObstacleView>().Where(view => view.gameObject.activeSelf).ToList();
            _coins = FindObjectsOfType<CoinView>().Where(view => view.gameObject.activeSelf).ToList();
            _baskets = FindObjectsOfType<BasketView>().Where(view => view.gameObject.activeSelf).ToList();
            maxScore = _coins.Sum(coin => coin.Value);
        }

        private void OnDestroy()
        {
            LevelModel.Dispose();
            LevelModel = null;
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(LevelSceneInitializer))]
    public class LevelSceneInitializerEditor : Editor
    {
        private LevelSceneInitializer _initializer;
        
        private void Awake()
        {
            _initializer = (target as LevelSceneInitializer);
            _initializer.Init();
        }
    }
#endif
}