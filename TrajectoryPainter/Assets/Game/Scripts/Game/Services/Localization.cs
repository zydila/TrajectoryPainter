using System;
using System.Collections.Generic;
using TP.Game.Data;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public static class Localization
    {
        private const string LOCALIZATION_PATH = "Data/Localizations/";
        
        public static ReactiveCommand LocalizationChanged { get; } = new ReactiveCommand();
        private static Dictionary<string, string> _localization = new Dictionary<string, string>();

        public static void InitLocalization()
        {
            Options.Language.Subscribe(LoadLocalization);
        }

        private static void LoadLocalization(Language language)
        {
            _localization.Clear();
            var data = Resources.Load<LocalizationData>(LOCALIZATION_PATH + language);
            _localization = data.GetLocalization();
//            data = null;
//            Resources.UnloadUnusedAssets();
            Resources.UnloadAsset(data);
            LocalizationChanged.Execute();
        }
        
        public static string Get(string key)
        {
            if (_localization.ContainsKey(key))
            {
                return _localization[key];
            }
            return $"#{key}";
        }

        public static string Get(string key, params string[] parameters)
        {
            if (parameters.Length % 2 != 0)
            {
                throw new ArgumentException("args count should be an even number");
            }
            var parametersDictionary = new Dictionary<string, string>();
            for (var i = 0; i < parameters.Length / 2; i++)
            {
                parametersDictionary.Add(parameters[i * 2], parameters[i * 2 + 1]);
            }

            return Get(key, parametersDictionary);
        }
        
        public static string Get(string key, Dictionary<string, string> parameters)
        {
            if (_localization.ContainsKey(key))
            {
                var loc =_localization[key];
                var text = Localization.Get(key);
                foreach (var parameter in parameters)
                {
                    loc = loc.Replace($"{{{parameter.Key}}}", parameter.Value);
                }

                return loc;
            }
            return $"#{key}";
        }
        
        public enum Language
        {
            English,
            Russian
        }
    }
}