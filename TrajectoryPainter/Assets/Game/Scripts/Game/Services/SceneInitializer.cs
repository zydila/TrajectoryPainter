using TP.Game.Controllers;
using TP.Game.Data;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public class SceneInitializer : MonoBehaviour
    {
        private const string DONT_DESTROY_ON_LOAD_ROOT_CANVAS_NAME = "DontDestroyOnLoadCanvas";
        private const string DONT_DESTROY_ON_LOAD_ROOT_CANVAS_PREFAB = "Prefabs/UI/" + DONT_DESTROY_ON_LOAD_ROOT_CANVAS_NAME;
        
        [SerializeField] private ScreensController screensController;

        public IReadOnlyReactiveProperty<bool> Inited => _inited;
        private readonly ReactiveProperty<bool> _inited = new ReactiveProperty<bool>(false);
        
        public ScreensController ScreensController => screensController;

        private int _initialResolutionX;
        private int _initialResolutionY;

        protected void Awake()
        {
            if (screensController == null)
            {
                screensController = FindObjectOfType<ScreensController>();
            }

            var dontDestroyOnLoad = GameObject.Find(DONT_DESTROY_ON_LOAD_ROOT_CANVAS_NAME);
            if (dontDestroyOnLoad == null)
            {
                dontDestroyOnLoad = (GameObject) Instantiate(Resources.Load(DONT_DESTROY_ON_LOAD_ROOT_CANVAS_PREFAB));
                dontDestroyOnLoad.name = dontDestroyOnLoad.name.Replace("(Clone)", "");
            }

            DontDestroyOnLoad(dontDestroyOnLoad);
            GameContainer.Init(this);
            InitInternal();
            AdjustCameraAndScreen();
            _inited.Value = true;
        }

        void AdjustCameraAndScreen()
        {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
            _initialResolutionX = Options.InitialResolutionX;
            _initialResolutionY = Options.InitialResolutionY;
            if (_initialResolutionX == 0 || _initialResolutionY == 0)
            {
                _initialResolutionX = (int) (Display.main.systemHeight * 0.8f / 16f * 9f);
                _initialResolutionY = (int) (Display.main.systemHeight * 0.8f);
                Options.InitialResolutionX = _initialResolutionX;
                Options.InitialResolutionY = _initialResolutionY;
            }
            
            Screen.SetResolution(_initialResolutionX, _initialResolutionY, false);
#elif UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
            var size = Mathf.Max( 9f / 16f / ((float) Screen.width / Screen.height), 1);
            Camera.main.orthographicSize = Camera.main.orthographicSize * size;
#endif
        }

        protected virtual void InitInternal() { }

        private void OnDestroy()
        {
            
        }
    }
}
