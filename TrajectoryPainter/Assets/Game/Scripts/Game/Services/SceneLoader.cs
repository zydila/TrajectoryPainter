using System.Threading.Tasks;
using TP.Game.Controllers;
using TP.Game.UI.Elements;
using UnityEngine.SceneManagement;

namespace TP.Game.Services
{
    public static class SceneLoader
    {
        public static async void LoadScene(string id)
        {
            var inputLock = ScreensController.LockInput();
            
            await UILoading.Show();
            UILoadingIcon.Show();
            
            SceneManager.LoadScene(id);
            
            await UILoading.Hide();
            UILoadingIcon.Hide();
            
            inputLock.Unlock();
        }
        
        public static async void LoadScene<T>(string id) where T : ScreenController
        {
            var inputLock = ScreensController.LockInput();
            
            await UILoading.Show();
            UILoadingIcon.Show();
            
            LoadScene(id);
            await Task.Delay(150);
            ScreensController.OpenScreen<T>(true);
            
            await UILoading.Hide();
            UILoadingIcon.Hide();
            
            inputLock.Unlock();
        }
        
        
    }
}