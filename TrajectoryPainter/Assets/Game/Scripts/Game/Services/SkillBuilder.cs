using System;
using TP.Core.Extensions;
using TP.Game.Interfaces;
using TP.Game.UI.Elements;
using TP.Game.Views;
using SkillView = TP.Game.UI.Elements.SkillView;

namespace TP.Game.Services
{
    public class SkillBuilder
    {
        public ISkill BuildSkill(SkillComponent skillComponent)
        {
            var attribute = (SkillAttribute) Attribute.GetCustomAttribute(skillComponent.GetType(), typeof(SkillAttribute));
            object[] args = {skillComponent};
            return Activator.CreateInstance(attribute.SkillType, args) as ISkill;
        }
    }
}