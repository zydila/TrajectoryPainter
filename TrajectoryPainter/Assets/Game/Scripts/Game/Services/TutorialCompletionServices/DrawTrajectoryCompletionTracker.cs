using TP.Game.Models;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public class DrawTrajectoryCompletionTracker : TutorialStageCompletionTracker
    {
        [SerializeField] private UnitView unitView;
        [SerializeField] private int countPoints;
        
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        protected override void StartTracking()
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            (unitView.SceneModel as UnitModel)?.ClearTrajectory.Execute();
            (unitView.SceneModel as UnitModel)?.TrajectoryModel
                                               .TrajectoryPoints
                                               .ObserveCountChanged()
                                               .Subscribe(count =>
                                                {
                                                    if (count > countPoints)
                                                    {
                                                        _disposable?.Dispose();
                                                        Complete();
                                                    }
                                                }).AddTo(_disposable);
        }

        protected override void StopTracking()
        {
            _disposable?.Dispose();
        }
    }
}