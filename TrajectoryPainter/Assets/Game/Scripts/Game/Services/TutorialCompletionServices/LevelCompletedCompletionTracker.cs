using TP.Game.Models;
using UniRx;

namespace TP.Game.Services
{
    public class LevelCompletedCompletionTracker : TutorialStageCompletionTracker
    {
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        protected override void StartTracking()
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Subscribe(OnLevelLoaded)
                         .AddTo(_disposable);
        }

        private void OnLevelLoaded(LevelModel model)
        {
            model?.FinishLevel
                  .Subscribe(result =>
                   {
                       if (result)
                       {
                           Complete();
                       }
                       else
                       {
                           _controller.NextStage.Execute(stage);
                       }
                   })
                  .AddTo(_disposable);
        }

        protected override void StopTracking()
        {
            _disposable?.Dispose();
        }
    }
}