using TP.Game.Models;
using UniRx;

namespace TP.Game.Services
{
    public class LevelStartedCompletionTracker : TutorialStageCompletionTracker
    {
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        protected override void StartTracking()
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Subscribe(OnLevelLoaded)
                         .AddTo(_disposable);
        }

        private void OnLevelLoaded(LevelModel model)
        {
            model?.StartLevel
                  .Subscribe(uniy => Complete())
                  .AddTo(_disposable);
        }

        protected override void StopTracking()
        {
            _disposable?.Dispose();
        }
    }
}