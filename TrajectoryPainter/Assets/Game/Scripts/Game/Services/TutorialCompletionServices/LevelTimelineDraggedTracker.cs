using TP.Game.Models;
using UniRx;

namespace TP.Game.Services
{
    public class LevelTimelineDraggedTracker : TutorialStageCompletionTracker
    {
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        protected override void StartTracking()
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Subscribe(OnLevelLoaded)
                         .AddTo(_disposable);
        }

        private void OnLevelLoaded(LevelModel model)
        {
            model?.CurrentTimeLerp
                  .Where(lerp => lerp > 0.5f)
                  .Take(1)
                  .Subscribe(uniy => Complete())
                  .AddTo(_disposable);
        }

        protected override void StopTracking()
        {
            _disposable?.Dispose();
        }
    }
}