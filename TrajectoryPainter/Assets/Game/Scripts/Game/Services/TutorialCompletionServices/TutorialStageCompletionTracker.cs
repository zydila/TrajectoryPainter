using TP.Game.Controllers;
using TP.Game.Models;
using UniRx;
using UnityEngine;

namespace TP.Game.Services
{
    public class TutorialStageCompletionTracker : MonoBehaviour
    {
        [SerializeField] protected int stage;
        [SerializeField] private bool finishTutorial;

        private LevelModel _model;
        protected TutorialController _controller;
        
        private CompositeDisposable _disposable = new CompositeDisposable();

        public void Init(TutorialController controller)
        {
            _controller = controller;
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            InitInternal();
            _model = GameContainer.LevelSelectionModel.CurrentLevelModel.Value;
            _model.Restart
                 .Subscribe(unit => InitInternal())
                 .AddTo(_disposable);
            InitInternal();
            controller.CurrentStage
                      .Where(currentStage => currentStage == stage)
                      .Subscribe(unit => StartTracking())
                      .AddTo(_disposable);
        }

        protected virtual void StartTracking() { }
        protected virtual void StopTracking() { }
        protected virtual void InitInternal() { }
        
        protected void Complete()
        {
           StopTracking();
           _controller.NextStage.Execute(stage);
            if (finishTutorial)
            {
                _controller.Finish.Execute();
            }
        }

        private void OnDestroy()
        {
            _disposable.Dispose();
            StopTracking();
        }
    }
}