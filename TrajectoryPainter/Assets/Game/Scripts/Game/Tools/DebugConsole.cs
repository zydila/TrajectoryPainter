using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.Tools
{
    public class DebugConsole : MonoBehaviour
    {
        [SerializeField] private GameObject debugMenu;
        [SerializeField] private Slider offsetSlider;

        private void Awake()
        {
            gameObject.SetActive(Debug.isDebugBuild);
            offsetSlider.value = TrajectoryView.DragOffset / TrajectoryView.MAX_DRAG_OFFSET;
        }

        public void ShowHideDebugMenu()
        {
            debugMenu.SetActive(!debugMenu.activeSelf);
        }

        public void AddLife()
        {
            GameContainer.PlayerModel.AddLifeDebug.Execute();
        }

        public void AddLevel()
        {
            GameContainer.PlayerModel.AddLevelDebug.Execute();
        }
        
        public void MinusLevel()
        {
            GameContainer.PlayerModel.MinusLevelDebug.Execute();
        }
        
        public void ClearSaveData()
        {
            GameContainer.PlayerModel.ClearSaveData.Execute();
        }

        public void OnOffsetChange(float lerp01)
        {
            TrajectoryView.DragOffset = lerp01 * TrajectoryView.MAX_DRAG_OFFSET;
        }
    }
}