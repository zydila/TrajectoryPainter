using System.Collections.Generic;
using System.Linq;
using TP.Core.Tools;
using TP.Game.Models;
using UnityEngine;

namespace TP.Game.Tools
{
    [ExecuteInEditMode]
    public class EditorTrajectory : MonoBehaviour
    {
        [SerializeField] private EditorTrajectoryPoint pointPrefab;
        [SerializeField] public GameObject Unit;

        public List<Vector3> Points => points;
        public Vector3[] SmoothedPoints => smoothedPoints;

        [SerializeField]
        private List<Vector3> points = new List<Vector3>();
        [SerializeField]
        private List<GameObject> pointsGo = new List<GameObject>();
        
        [SerializeField]
        private Vector3[] smoothedPoints;

        [SerializeField] private int _maxNumOfPoints;

        public void Init(GameObject unit, int maxNumOfPoints)
        {
            _maxNumOfPoints = maxNumOfPoints;
            Unit = unit;
        }
        
        public void AddPoint(Vector3 point)
        {
            Debug.Log($"Add point {point}");
            points.Add(point);
            var pointInstance = Instantiate(pointPrefab, transform);
            pointInstance.Init(points.Count);
            pointInstance.gameObject.transform.position = point;
            pointsGo.Add(pointInstance.gameObject);
            smoothedPoints = LineSmoother.SmoothLine(points, TrajectoryModel.SEGMENT_SIZE);
        }

        public void Clear()
        {
            foreach (var o in pointsGo)
            {
                DestroyImmediate(o);
            }
            points.Clear();
            pointsGo.Clear();
            smoothedPoints = null;
        }

        public Vector3 Lerp01(float lerp)
        {
            if (SmoothedPoints == null || SmoothedPoints.Length == 0)
            {
                if (Points.Count == 0)
                {
                    return Unit.transform.position;
                }
                else
                {
                    return Points[0];
                }
            }
            
            lerp = Mathf.Clamp01(lerp);
            lerp *= (float) _maxNumOfPoints / Points.Count;
            lerp = Mathf.Clamp01(lerp);
            var indexUnclamped = lerp * SmoothedPoints.Length;
            var indexClamped = Mathf.FloorToInt(lerp * SmoothedPoints.Length);
            if (indexClamped + 1 >= SmoothedPoints.Length)
            {
                return SmoothedPoints[SmoothedPoints.Length - 1];
            }
            
            return Vector3.Lerp(SmoothedPoints[indexClamped], SmoothedPoints[indexClamped + 1], indexUnclamped - indexClamped);
        }
    }
}