#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TP.Game.Tools
{
    [ExecuteInEditMode]
    public class EditorTrajectoryBrush : MonoBehaviour
    {
        public void SelectLevelCreator()
        {
#if UNITY_EDITOR
            Selection.activeObject = FindObjectOfType<LevelCreator>().gameObject;
#endif
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(EditorTrajectoryBrush))]
    public class EditorTrajectoryBrushEditor : Editor
    {
        private EditorTrajectoryBrush _brush;
        
        void Awake()
        {
            _brush = (target as EditorTrajectoryBrush);
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Go To Creator"))
            {
                _brush.SelectLevelCreator();
            }
        }
    }
#endif
}