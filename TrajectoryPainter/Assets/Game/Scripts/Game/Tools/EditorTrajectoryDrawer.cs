using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Models;
using TP.Game.Views;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace TP.Game.Tools
{
    [ExecuteInEditMode]
    public class EditorTrajectoryDrawer : MonoBehaviour
    {
        [SerializeField] private Transform brush;
        [SerializeField] private EditorTrajectory trajectoryPrefab;
        [SerializeField] private Transform trajectoriesRoot;

        private readonly Dictionary<GameObject, EditorTrajectory> _unitsTrajectories = new Dictionary<GameObject, EditorTrajectory>();
        private EditorTrajectory _selectedTrajectory;
        private Vector3 _lastDrawnPoint;
        private int _maxNumOfPoints;
        private bool _startDraw;
        private LevelCreator _creator;

        public void Init(int maxNumOfPoints, LevelCreator creator)
        {
            Clear();
            _creator = creator;
            _maxNumOfPoints = maxNumOfPoints;
            _unitsTrajectories.Clear();
            var trajectories = FindObjectsOfType<EditorTrajectory>().ToList();
            foreach (var editorTrajectory in trajectories)
            {
                _unitsTrajectories.Add(editorTrajectory.Unit, editorTrajectory);
            }
        }

        private void OnEnable()
        {
            trajectoriesRoot.gameObject.SetActive(!Application.isPlaying);
        }

        public void CaptureTrajectoryForUnit(GameObject unit)
        {
            Debug.Log($"Capture Trajectory for {unit.name}");
            if (_unitsTrajectories.ContainsKey(unit))
            {
                _selectedTrajectory = _unitsTrajectories[unit];
            }
            else
            {
                _selectedTrajectory = Instantiate(trajectoryPrefab, trajectoriesRoot);
                _selectedTrajectory.Init(unit, _maxNumOfPoints);
                _unitsTrajectories.Add(unit, _selectedTrajectory);
                AddPointToCurrentTrajectory(unit.transform.position);
            }

            if (_unitsTrajectories[unit].Points.Count == 0)
            {
                AddPointToCurrentTrajectory(unit.transform.position);
            }
            
            brush.position = _unitsTrajectories[unit].Points.Last();
#if UNITY_EDITOR
            Selection.activeObject = brush;
#endif
            _startDraw = true;
        }

        public void ClearTrajectory(GameObject unit)
        {
            Debug.Log($"Clear Trajectory for {unit.name}");
            if (_unitsTrajectories.ContainsKey(unit))
            {
                _unitsTrajectories[unit].Clear();
            }
        }

        public void ReleaseTrajectory()
        {
            Debug.Log("Release Trajectory");
            _selectedTrajectory = null;
            _startDraw = false;
#if UNITY_EDITOR
            Selection.activeObject = _creator.gameObject;
#endif
        }

        public EditorTrajectory GetTrajectory(GameObject unit)
        {
            if (_unitsTrajectories.ContainsKey(unit))
            {
                return _unitsTrajectories[unit];
            }

            return null;
        }
        
        private void Update()
        {
            if (!_startDraw)
            {
                return;
            }
            
            if (_selectedTrajectory.Points.Count >= _maxNumOfPoints)
            {
                ReleaseTrajectory();
                return;
            }
            
            var currentPoint = brush.position;
            currentPoint.Set(currentPoint.x, currentPoint.y, TrajectoryModel.DEFAULT_Z_POSITION);

            var lastIndex = _selectedTrajectory.Points.Count - 1;
            var angleCheck = true;
            if (_selectedTrajectory.Points.Count > 1)
            {
                var angle = Vector2.Angle(currentPoint - _selectedTrajectory.Points[lastIndex], _selectedTrajectory.Points[lastIndex] - _selectedTrajectory.Points[lastIndex - 1]);
                angleCheck = angle < TrajectoryView.MAX_ANGLE;
            }

            var distance = (currentPoint - _lastDrawnPoint).magnitude;
            if (angleCheck)
            {
                if (distance > TrajectoryView.MAX_DISTANCE)
                {
                    currentPoint = (currentPoint - _lastDrawnPoint).normalized * TrajectoryView.MAX_DISTANCE + _lastDrawnPoint;
                    AddPointToCurrentTrajectory(currentPoint);
                }
            }
        }

        private void Clear()
        {
            _selectedTrajectory = null;
            _startDraw = false;
            _maxNumOfPoints = 0;
        }

        private void AddPointToCurrentTrajectory(Vector3 point)
        {
            point.Set(point.x, point.y, TrajectoryModel.DEFAULT_Z_POSITION);
            _selectedTrajectory.AddPoint(point);
            _lastDrawnPoint = point;
        }

        public void ClearAll()
        {
            Clear();
            foreach (var trajectory in _unitsTrajectories)
            {
                trajectory.Value.Clear();
                DestroyImmediate(trajectory.Value.gameObject);
            }
            _unitsTrajectories.Clear();
        }
    }
}