using System.Collections;
using System.Collections.Generic;
using TMPro;
using TP.Game.Models;
using UnityEngine;

namespace TP.Game.Tools
{
    public class EditorTrajectoryPoint : MonoBehaviour
    {
        [SerializeField] private TextMeshPro text;

        public void Init(int index)
        {
            text.text = (index * TrajectoryModel.TIME_FOR_POINT).ToString("0:00");
        }
    }
}