using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Data;
using TP.Game.Models;
using TP.Game.Services;
using TP.Game.Views;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TP.Game.Tools
{
    [Serializable]
    public class LevelCreatorCacheData
    {
        [SerializeField] public LevelSceneInitializer initializer;
        [SerializeField] public EditorTrajectoryDrawer editorTrajectoryDrawer;
        [SerializeField] public float TimeLerp;
        [SerializeField] public float TimeLerpCache;
        [SerializeField] public bool ShowObstacles;
        [SerializeField] public bool ShowUnits;
        [SerializeField] public bool PlayLevel;
        [SerializeField] public double TimePlayStarted;
        [SerializeField] public List<bool> ToggleUnitDrawTrajectory = new List<bool>();
        [SerializeField] public List<bool> ToggleUnitDrawTrajectoryCache = new List<bool>();
        [SerializeField] public int _maxNumOfPoints;
        [SerializeField]
        public List<ObstacleView> Obstacles = new List<ObstacleView>(); 
        [SerializeField]
        public List<UnitView> Units = new List<UnitView>();

        public void Clear()
        {
            ShowObstacles = false;
            TimeLerp = 0;
            ShowObstacles = false;
            ShowUnits = false;
            PlayLevel = false;
            TimePlayStarted = 0;
            ToggleUnitDrawTrajectory = new List<bool>();
            ToggleUnitDrawTrajectoryCache = new List<bool>();
            editorTrajectoryDrawer?.ClearAll();
        }
    } 
    
    [ExecuteInEditMode]
    public class LevelCreator : MonoBehaviour
    {
        [SerializeField] public LevelPackType pack;
        [SerializeField] public LevelCreatorCacheData cache;
        
        public bool Inited { get; private set; }
        public float TimeLimit => cache.initializer.TimeLimit;
        private int _maxNumOfPoints;

        private void Awake()
        {
            Init();
        }

        public void Init()
        {
            cache.initializer = FindObjectOfType<LevelSceneInitializer>();
            cache.initializer.Init();
            cache.initializer.levelId = SceneManager.GetActiveScene().name;
            cache.editorTrajectoryDrawer = FindObjectOfType<EditorTrajectoryDrawer>();
            _maxNumOfPoints = Mathf.CeilToInt(TimeLimit / TrajectoryModel.TIME_FOR_POINT) + 1;
            cache.Obstacles.Clear();
            cache.Units.Clear();
            cache.Obstacles = FindObjectsOfType<ObstacleView>().ToList();
            cache.Units.Clear();
            cache.Units = FindObjectsOfType<UnitView>().ToList();
            LerpTime(0);
            
            cache.editorTrajectoryDrawer.Init(_maxNumOfPoints, this);
            cache.Obstacles.ForEach(obstacle => obstacle.Animation?.SetNumOfFrames(Mathf.CeilToInt(TimeLimit * 60)));
            cache.Obstacles.ForEach(obstacle => obstacle.Animation?.SetStartKey());
            UpdateLevelsData();
            Inited = true;
        }

        public void CaptureTrajectory(GameObject unit)
        {
            cache.editorTrajectoryDrawer.CaptureTrajectoryForUnit(unit);
        }

        public void ReleaseTrajectory()
        {
            cache.editorTrajectoryDrawer.ReleaseTrajectory();
        }

        public void ClearTrajectory(GameObject unit)
        {
            cache.editorTrajectoryDrawer.ClearTrajectory(unit);
        }

        public void LerpTime(float lerp)
        {
            cache.TimeLerp = lerp;
            cache.TimeLerpCache = cache.TimeLerp;
            foreach (var obstacle in cache.Obstacles)
            {
                ObstacleLerp(obstacle, lerp);
            }

            foreach (var unit in cache.Units)
            {
                var trajectory = cache.editorTrajectoryDrawer.GetTrajectory(unit.gameObject);
                if (trajectory != null)
                {
                    unit.transform.position = cache.editorTrajectoryDrawer.GetTrajectory(unit.gameObject).Lerp01(lerp);
                }
            }
        }

        private void ObstacleLerp(ObstacleView obstacleView , float lerp)
        {
            if (obstacleView.Animation != null)
            {
                var key = obstacleView.Animation.LerpAnimation01(lerp);
                if (key.Valid)
                {
                    var transform1 = obstacleView.transform;
                    transform1.position = key.Position;
                    transform1.rotation = key.Rotation;
                    transform1.localScale = key.Scale;
                }
            }
        }

        public void Clear()
        {
            LerpTime(0);
            cache.Clear();
        }

        private void UpdateLevelsData()
        {
#if UNITY_EDITOR
            foreach (var packType in Enum.GetValues(typeof(LevelPackType)).Cast<LevelPackType>())
            {
                var levelsData = AssetDatabase.LoadAssetAtPath<LevelsPackData>($"Assets/Game/Resources/Data/LevelPacks/{packType}.asset");
                if (levelsData == null)
                {
                    continue;
                }
                var levelIndex = levelsData.Levels.FindIndex(level => level.Id == cache.initializer.levelId);
                if (cache.initializer.DevelopmentInProgress || packType != pack)
                {
                    if (levelIndex != -1)
                    {
                        levelsData.Levels.RemoveAt(levelIndex);
                    }
                }
                else if (levelIndex == -1)
                {
                    var newLevel = new LevelDataElement();
                    newLevel.Id = SceneManager.GetActiveScene().name;
                    newLevel.MaxScore = cache.initializer.MaxScore;
                    newLevel.NameKey = newLevel.Id;
                    levelsData.Levels.Add(newLevel);
                }
                else
                {
                    levelsData.Levels[levelIndex].MaxScore = cache.initializer.MaxScore;
                }

                EditorUtility.SetDirty(levelsData);
            }
#endif
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(LevelCreator))]
    public class LevelCreatorEditor : Editor
    {
        private LevelCreator _creator;
        private Vector2 _scrollPosition;
        
        void Awake()
        {
            _creator = (target as LevelCreator);
            Init();
        }
    
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUI.BeginChangeCheck();
            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
            if (_creator.Inited)
            {
                _creator.cache.ShowObstacles = EditorGUILayout.BeginFoldoutHeaderGroup(_creator.cache.ShowObstacles, "Obstacles");
                if (_creator.cache.ShowObstacles)
                {
                    DrawObstacles();
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                _creator.cache.ShowUnits = EditorGUILayout.BeginFoldoutHeaderGroup(_creator.cache.ShowUnits, "Units");
                if (_creator.cache.ShowUnits)
                {
                    DrawUnits();
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                
                DrawSlider();
                DrawPlayLevel();
            }

            DrawClear();
            //DrawInit();
            GUILayout.EndScrollView();
            
            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(target);
        }

        private void DrawClear()
        {
            if (GUILayout.Button("Clear (this will clear trajectories)"))
            {
                _creator.Clear();
                Init();
            }
        }

        private void DrawInit()
        {
            if (GUILayout.Button(_creator.Inited ? "Update" : "Init"))
            {
                Init();
            }
        }

        private void Init()
        {
            _creator.Init();
            _creator.cache.ToggleUnitDrawTrajectory = new List<bool>(_creator.cache.Units.Count);
            _creator.cache.ToggleUnitDrawTrajectoryCache = new List<bool>(_creator.cache.Units.Count);
            foreach (var gameObject in _creator.cache.Units)
            {
                _creator.cache.ToggleUnitDrawTrajectory.Add(false);
                _creator.cache.ToggleUnitDrawTrajectoryCache.Add(false);
            }
        }

        private void DrawSlider()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Lerp01", GUILayout.Width(50));
            _creator.cache.TimeLerp = GUILayout.HorizontalSlider(_creator.cache.TimeLerp, 0, 1, GUILayout.Height(25));
            EditorGUILayout.EndHorizontal();
            if (Mathf.Abs(_creator.cache.TimeLerpCache - _creator.cache.TimeLerp) > 0.01)
            {
                _creator.LerpTime(_creator.cache.TimeLerp);
            }
        }

        private void DrawObstacles()
        {
            foreach (var obstacle in _creator.cache.Obstacles)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button(obstacle.name, GUILayout.Width(150)))
                {
                    Selection.activeObject = obstacle.gameObject;
                }

//                if (GUILayout.Button("AddKey"))
//                {
//                    obstacle.Animation.AddAnimationKey(obstacle.transform, _creator.cache.TimeLerp);
//                }

                EditorGUILayout.EndHorizontal();
            }
        }
        
        private void DrawUnits()
        {
            var hasActiveDraw = _creator.cache.ToggleUnitDrawTrajectory.Any(draw => draw);
            for (var i = 0; i < _creator.cache.Units.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button(_creator.cache.Units[i].name, GUILayout.Width(150)))
                {
                    Selection.activeObject = _creator.cache.Units[i].gameObject;
                }

                _creator.cache.ToggleUnitDrawTrajectory[i] = GUILayout.Toggle(_creator.cache.ToggleUnitDrawTrajectory[i], "Start Draw", GUILayout.Width(150));
                if (_creator.cache.ToggleUnitDrawTrajectoryCache[i] != _creator.cache.ToggleUnitDrawTrajectory[i])
                {
                    if (_creator.cache.ToggleUnitDrawTrajectory[i])
                    {
                        if (hasActiveDraw)
                        {
                            _creator.cache.ToggleUnitDrawTrajectory[i] = false;
                        }
                        else
                        {
                            hasActiveDraw = true;
                            _creator.CaptureTrajectory(_creator.cache.Units[i].gameObject);
                        }
                    }
                    else
                    {
                        hasActiveDraw = false;
                        _creator.ReleaseTrajectory();
                    }
                    _creator.cache.ToggleUnitDrawTrajectoryCache[i] = _creator.cache.ToggleUnitDrawTrajectory[i];
                }
                
                if (GUILayout.Button("Clear trajectory", GUILayout.Width(150)))
                {
                    _creator.LerpTime(0);
                    _creator.ClearTrajectory(_creator.cache.Units[i].gameObject);
                    _creator.cache.ToggleUnitDrawTrajectory[i] = false;
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void DrawPlayLevel()
        {
            if (GUILayout.Button(_creator.cache.PlayLevel ? "Stop" : "Play"))
            {
                _creator.cache.PlayLevel = !_creator.cache.PlayLevel;
                _creator.cache.TimeLerp = 0;
                _creator.cache.TimePlayStarted = 0;
            }

            ProcessPlay();
        }

        private void ProcessPlay()
        {
            if (_creator.cache.PlayLevel)
            {
                _creator.transform.position += Vector3.zero;
                if (_creator.cache.TimePlayStarted < 1)
                {
                    _creator.cache.TimePlayStarted = EditorApplication.timeSinceStartup;
                }
                else
                {
                    var timePassed = (float) (EditorApplication.timeSinceStartup - _creator.cache.TimePlayStarted);
                    if(timePassed > _creator.TimeLimit)
                    {
                        timePassed = _creator.TimeLimit;
                        _creator.cache.PlayLevel = false;
                    }
                    _creator.cache.TimeLerp = timePassed / _creator.TimeLimit;
                    EditorUtility.SetDirty(target);
                }
                
            }
        }
    }
#endif
}
