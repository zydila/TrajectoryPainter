using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Tools
{
    public class MonoBehaviourEvents : MonoBehaviour
    {
        [SerializeField] private UnityEvent OnAwakeEvent;
        [SerializeField] private UnityEvent OnStartEvent;
        [SerializeField] private UnityEvent OnEnableEvent;
        [SerializeField] private UnityEvent OnDisableEvent;
        [SerializeField] private UnityEvent OnDestroyEvent;
        
        private void Awake()
        {
            Debug.Log($"{name} Awake");
            OnAwakeEvent?.Invoke();
        }
        
        private void Start()
        {
            Debug.Log($"{name} Start");
            OnStartEvent?.Invoke();
        }
        
        private void OnEnable()
        {
            Debug.Log($"{name} Enable");
            OnEnableEvent?.Invoke();
        }
        
        private void OnDisable()
        {
            Debug.Log($"{name} Disable");
            OnDisableEvent?.Invoke();
        }
        
        private void OnDestroy()
        {
            Debug.Log($"{name} Destroy");
            OnDestroyEvent?.Invoke();
        }
    }
}