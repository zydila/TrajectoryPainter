using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceRandomizedPlay : MonoBehaviour
{
    private AudioSource _source;
    private bool isPlayingCache = false;
    
    private void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (isPlayingCache != _source.isPlaying)
        {
            isPlayingCache = _source.isPlaying;
            if (isPlayingCache)
            {
                _source.time = Random.Range(0, _source.clip.length);
            }
        }
    }
}
