using TP.Game.Data;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceVolumeAdjuster : MonoBehaviour
{
    [SerializeField] private VolumeFilter volumeFilter = VolumeFilter.Basic;
    
    private AudioSource _source;
    private float baseVolume = 1f;
    
    private void Awake()
    {
        _source = GetComponent<AudioSource>();
        baseVolume = _source.volume;
        switch (volumeFilter)
        {
            case VolumeFilter.Music:
                Options.MasterVolume
                       .CombineLatest(Options.MusicVolume, (masterVolume, musicVolume) => baseVolume * masterVolume * musicVolume)
                       .Subscribe(volume => _source.volume = volume)
                       .AddTo(this);
                break;
            default:
                Options.MasterVolume
                       .Subscribe(volume => _source.volume = baseVolume * volume)
                       .AddTo(this);
                break;
        }
        Options.SoundMuted
               .Subscribe(mute => _source.mute = mute)
               .AddTo(this);
    }

    private enum VolumeFilter
    {
        Basic,
        Music,
        Effect,
        UI
    }
}
