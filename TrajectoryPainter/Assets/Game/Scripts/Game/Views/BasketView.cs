using System.Collections.Generic;
using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    [RequireComponent(typeof(Collider2D))]
    public class BasketView : MonoBehaviour, ISceneCollider
    {
        [SerializeField] private List<UnitModel.UnitType> unitTypes;

        public IReadOnlyCollection<UnitModel.UnitType> UnitTypes => unitTypes;
        
        public ISceneModel SceneModel => _basketModel;
        private BasketModel _basketModel;
        
        public void Init(BasketModel model)
        {
            _basketModel = model;
        }
    }
}