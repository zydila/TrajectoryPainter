using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    [RequireComponent(typeof(Collider2D))]
    public class CoinView : MonoBehaviour, ISceneCollider
    {
        [SerializeField] private CoinModel.Type type = CoinModel.Type.Basic;
        [SerializeField] private int value;

        [SerializeField] private UnityEvent onCollected; 

        private Collider2D _collider;
        
        public CoinModel.Type Type => type; 
        public int Value => value; 
        
        public ISceneModel SceneModel => _model;
        private CoinModel _model;

        public void Init(CoinModel model)
        {
            _collider = GetComponent<Collider2D>();
            _model = model;
            gameObject.SetActive(true);
            _model.IsCollected
                .Subscribe(collected =>
                   {
                       if (collected)
                       {
                           transform.localScale = Vector3.zero;
                           _collider.enabled = false;
                           onCollected?.Invoke();
                       }
                       else
                       {
                           gameObject.SetActive(true);
                           transform.localScale = Vector3.one;
                           _collider.enabled = true;
                       }
                   })
                .AddTo(this);
        }
    }
}