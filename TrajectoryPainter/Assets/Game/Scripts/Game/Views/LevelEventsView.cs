using System;
using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    public class LevelEventsView : MonoBehaviour
    {
        [SerializeField] private LevelEvents levelEvents;

        private CompositeDisposable _disposable = new CompositeDisposable();

        private void Awake()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel
                         .Subscribe(OnLevelUpdated)
                         .AddTo(this);
        }

        private void OnLevelUpdated(LevelModel newLevelModel)
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            newLevelModel?.State.Subscribe(state =>
            {
                switch (state)
                {
                    case LevelModel.LevelState.Prepare:
                        levelEvents.OnPrepare?.Invoke();
                        break;
                    case LevelModel.LevelState.InProgress:
                        levelEvents.OnInProgress?.Invoke();
                        break;
                    case LevelModel.LevelState.Finished:
                        levelEvents.OnFinished?.Invoke();
                        break;
                    case LevelModel.LevelState.Lost:
                        levelEvents.OnLost?.Invoke();
                        break;
                }
            }).AddTo(_disposable);
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
        }

        [Serializable]
        public class LevelEvents
        {
            public UnityEvent OnPrepare;
            public UnityEvent OnInProgress;
            public UnityEvent OnFinished;
            public UnityEvent OnLost;
        }
    }
}