using System;
using System.Collections.Generic;
using TP.Game.Services;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.Views
{
    [RequireComponent(typeof(Text))]
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField] private string key;

        public string Key => key;
        
        private Text _text;
        private Dictionary<string, string> _parameters = new Dictionary<string, string>();
        
        private void Awake()
        {
            _text = GetComponent<Text>();
            UpdateText();
            Localization.LocalizationChanged
                        .Subscribe(unit => UpdateText())
                        .AddTo(this);
        }

        public LocalizedText SetTextKey(string newKey)
        {
            key = newKey;
            return this;
        }

        public LocalizedText SetParameter(string key, string value)
        {
            if (_parameters.ContainsKey(key))
            {
                _parameters[key] = value;
            }
            else
            {
                _parameters.Add(key, value);
            }
            return this;
        }

        public LocalizedText SetParameters(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
            return this;
        }

        public LocalizedText SetParameters(params string[] parameters)
        {
            if (parameters.Length % 2 != 0)
            {
                throw new ArgumentException("args count should be an even number");
            }
            _parameters.Clear();
            for (var i = 0; i < parameters.Length / 2; i++)
            {
                _parameters.Add(parameters[i * 2], parameters[i * 2 + 1]);
            }
            return this;
        }

        public void UpdateText()
        {
            if (_text == null)
            {
                _text = GetComponent<Text>();
            }
            _text.text = Localization.Get(key, _parameters);
        }
    }
}