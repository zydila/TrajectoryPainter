using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    public class ObstacleView : MonoBehaviour, ISceneCollider
    {
//        [SerializeField] private Animator _animator;
        [SerializeField] private ObstacleAnimation _animation;
        [SerializeField] private ObstacleModel.Type _type = ObstacleModel.Type.Basic;

        [SerializeField] private ObstacleEvents events;

        public ObstacleAnimation Animation => _animation;
        
        public ObstacleModel.Type Type => _type;
        
        public ISceneModel SceneModel => _model;
        private ObstacleModel _model;

        private AnimationClip _clip;
        private float _clipLength;

        private List<Collider2D> _colliders;
        private Vector3 defaultScale;

        public void Init(ObstacleModel model)
        {
            _model = model;
            defaultScale = transform.localScale;
            _colliders = GetComponents<Collider2D>().ToList();
            
            if (_animation != null)
            {
                GameContainer.LevelSelectionModel.CurrentLevelModel.Value.CurrentTimeLerp.Subscribe(lerp =>
                {
                    var key = _animation.LerpAnimation01(lerp);
                    if (key.Valid)
                    {
                        var transform1 = transform;
                        transform1.position = key.Position;
                        transform1.rotation = key.Rotation;
                        transform1.localScale = key.Scale;
                    }
                }).AddTo(this);
                _model.Kill.Subscribe(unit =>
                {
                    transform.localScale = Vector3.zero;
                    _colliders.ForEach(col => col.enabled = false);
                    events.OnDied?.Invoke();
                });
                _model.Active
                      .Where(active => active)
                      .Subscribe(active =>
                       {
                           gameObject.SetActive(true);
                           _colliders.ForEach(col => col.enabled = true);
                           transform.localScale = defaultScale;
                       })
                      .AddTo(this);
            }
        }
        
        [Serializable]
        public class ObstacleEvents
        {
            [SerializeField] public UnityEvent OnDied;
            [SerializeField] public UnityEvent OnStartMoving;
            [SerializeField] public UnityEvent OnStopMoving;
        }
    }
}