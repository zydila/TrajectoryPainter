using TP.Game.Models;
using UnityEngine;

namespace TP.Game.Views
{
    public class ActiveSkillComponent : SkillComponent
    {
        [SerializeField] protected float timeActive = 1f;
        
        public float ActiveTime => timeActive;
        
        protected ActiveBaseSkill activeSkill => skill as ActiveBaseSkill;
    }
}
