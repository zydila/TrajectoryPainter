using TP.Game.Interfaces;
using TP.Game.Models;

namespace TP.Game.Views
{
    [SkillAttribute(typeof(InvincibleSkill))]
    public class InvincibilitySkillComponent : ActiveSkillComponent
    {
    }
}
