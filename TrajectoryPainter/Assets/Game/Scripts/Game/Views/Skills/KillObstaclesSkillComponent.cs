using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;

namespace TP.Game.Views
{
    [SkillAttribute(typeof(KillObstaclesOnTouchSkill))]
    public class KillObstaclesSkillComponent : ActiveSkillComponent
    {
    }
}
