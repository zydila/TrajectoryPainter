using System;
using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEditor.UIElements;
using UnityEngine;

namespace TP.Game.Views
{
    [SkillAttribute(typeof(ShrinkSkill))]
    public class ShrinkSkillComponent : ActiveSkillComponent
    {
        [SerializeField] private AnimationCurve shrinkCurve;

        private bool skillActive;
        private float timePassed;
        private Vector3 defaultScale;
        
        protected override void OnInitedInternal()
        {
            base.OnInitedInternal();
            skill.Abort
                 .Subscribe(unit => RestoreInitialView())
                 .AddTo(this);
            defaultScale = unitView.transform.localScale;
            activeSkill.Active
                       .Subscribe(active =>
                        {
                            timePassed = 0;
                            skillActive = active;
                        })
                       .AddTo(this);
        }

        public override void RestoreInitialView()
        {
            if (inited)
            {
                unitView.transform.localScale = defaultScale;
            }
        }

        private void Update()
        {
            if (skillActive)
            {
                unitView.transform.localScale = defaultScale * shrinkCurve.Evaluate(timePassed / ActiveTime);
                timePassed += Time.deltaTime;
            }
        }
    }
}
