using System;
using TP.Game.Interfaces;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    public abstract class SkillComponent : MonoBehaviour
    {
        [SerializeField] private string nameLocalizationKey;
        [SerializeField] private string descriptionLocalizationKey;
        [SerializeField] protected SkillEvents events;

        public string NameKey => nameLocalizationKey;
        public string DescriptionKey => descriptionLocalizationKey;
        public Color Color => unitView.DefaultColor;

        protected UnitView unitView;
        protected ISkill skill;
        protected bool inited;
        
        public void Init(UnitView unitView)
        {
            this.unitView = unitView;
            if (skill != null)
            {
                OnInited();
            }
        }

        public void SetSkill(ISkill skill)
        {
            this.skill = skill;
            skill.Use.Subscribe(unit => events.OnUsed?.Invoke()).AddTo(this);
            skill.Abort.Subscribe(unit => events.OnAborted?.Invoke()).AddTo(this);
            skill.Finished.Subscribe(unit =>
            {
                if (skill.Aborted.Value)
                    events.OnAborted?.Invoke();
                else
                    events.OnFinished?.Invoke();
            }).AddTo(this);
            if (unitView != null)
            {
                OnInited();
            }
        }

        public virtual void RestoreInitialView() { }
        
        private void OnInited()
        {
            OnInitedInternal();
            inited = true;
            RestoreInitialView();
        }
        
        protected virtual void OnInitedInternal() { }

        [Serializable]
        public class SkillEvents
        {
            public UnityEvent OnUsed;
            public UnityEvent OnFinished;
            public UnityEvent OnAborted;
        }
    }
}