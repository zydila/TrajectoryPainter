using TP.Game.Interfaces;
using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TP.Game.Views
{
    public class SphereView : UnitView, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData) 
        {
//            Debug.Log($"OnPointerClick {gameObject.name}");
            if (Model.IsSelected.Value)
            {
                Model.Deselect.Execute();
            }
            else
            {
                Model.Select.Execute();
            }
        }
    }
}
