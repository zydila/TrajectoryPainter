using UnityEngine;
using UnityEngine.EventSystems;

namespace TP.Game.Views
{
    [RequireComponent(typeof(Collider2D))]
    public class TrajectoryPointView : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        private const float TIME_TO_DELETE = 0.66f;

        private TrajectoryView _trajectoryView;
        private int _index;
        private Vector2 _pointerDownPosition;
        private float _pointerDownTime;
        private bool _selected;
        private bool _startedPaint;
        private readonly Vector3 _selectedScale = new Vector3(1.5f, 1.5f, 1);
        private readonly Vector3 _normalScale = new Vector3(1, 1, 1);
        
        public void Init(TrajectoryView trajectoryView, int index)
        {
            _trajectoryView = trajectoryView;
            _index = index;
        }

        public void SetLast(bool isLast)
        {
            _spriteRenderer.color = isLast ? Color.green : Color.white;
            transform.localScale = isLast ? _selectedScale : _normalScale;
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if (_trajectoryView.CheckIfLast(_index))
            {
                _trajectoryView.CaptureTrajectory();
            }
            _selected = true;
            _startedPaint = false;
            _pointerDownPosition = eventData.position;
            if (Time.timeSinceLevelLoad - _pointerDownTime < TIME_TO_DELETE)
            {
                _trajectoryView.RemovePoint(_index + 1);
                _trajectoryView.ReleaseTrajectory();
                _selected = false;
            }
            _pointerDownTime = Time.timeSinceLevelLoad;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_selected)
            {
                if ((eventData.position - _pointerDownPosition).magnitude > TrajectoryView.MAX_DISTANCE)
                {
                    if (_trajectoryView.CheckIfLast(_index))
                    {
                        _trajectoryView.CaptureTrajectory();
                        _startedPaint = true;
                    }
                    else
                    {
                        _selected = false;
                    }
                }
            }
        }
        
        public void OnPointerUp(PointerEventData eventData)
        {
            _trajectoryView.ReleaseTrajectory();
            _selected = false;
        }

        private void Update()
        {
            if (_selected)
            {
                if (!_startedPaint && Time.timeSinceLevelLoad - _pointerDownTime >= TIME_TO_DELETE)
                {
                    _trajectoryView.RemovePoint(_index + 1);
                    _trajectoryView.ReleaseTrajectory();
                    _selected = false;
                }
            }
        }
        
        public void OnPointerClick(PointerEventData eventData) { }
    }
}