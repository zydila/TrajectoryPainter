using System;
using System.Collections.Generic;
using System.Linq;
using TP.Core.Tools;
using TP.Game.Interfaces;
using TP.Game.Models;
using TP.Game.Services;
using TP.Game.UI.Elements;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace TP.Game.Views
{
    [RequireComponent(typeof(LineRenderer))]
    public class TrajectoryView : MonoBehaviour, IDisposable
    {
        [SerializeField] private TrajectoryPointView pointViewPrefab;
        [SerializeField] private Transform root;
        [SerializeField] private UISoundEmitter emitter;

        public static float DragOffset = 0f;
        public const float MAX_DRAG_OFFSET = 1.5f;
        
        public const float MAX_DISTANCE = 1f;
        public const float MAX_ANGLE = 90f;
        
        private readonly List<TrajectoryPointView> _points = new List<TrajectoryPointView>();
        
        private LineRenderer _lineRenderer;
        private TrajectoryModel _trajectoryModel;
        private Camera _mainCamera;
        private bool _captured;
        private Vector3 _desiredPoint;
        private Vector3 _lastDesiredPoint;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        
        public void Init(TrajectoryModel trajectoryModel, Color color)
        {
            _mainCamera = Camera.main;
            _trajectoryModel = trajectoryModel;
            if (_lineRenderer == null)
            {
                _lineRenderer = GetComponent<LineRenderer>();
            }
            
            _lineRenderer.startColor = color;
            _lineRenderer.endColor = color;

            _trajectoryModel.SmoothedPoints.Subscribe(UpdateDrawer).AddTo(_disposable);
            _trajectoryModel.TrajectoryPoints
                            .ObserveAdd()
                            .Subscribe(added => TrajectoryPointAdd(added.Value))
                            .AddTo(_disposable);
            _trajectoryModel.TrajectoryPoints
                            .ObserveRemove()
                            .Subscribe(removedPoint => TrajectoryPointRemove())
                            .AddTo(_disposable);
            TrajectoryPointAdd(trajectoryModel.TrajectoryPoints[0]);
            _trajectoryModel.UnitModel
                            .State
                            .Select(state => state == UnitModel.UnitState.Alive)
                            .Subscribe(gameObject.SetActive)
                            .AddTo(_disposable);
            LevelSceneInitializer.Instance
                                 .LevelModel
                                 .State
                                 .Subscribe(state => gameObject.SetActive(state == LevelModel.LevelState.Prepare))
                                 .AddTo(this);
        }

        private void UpdateDrawer(List<Vector3> points) 
        {
            if (points != null)
            {
                _lineRenderer.positionCount = points.Count;
                _lineRenderer.SetPositions(points.ToArray());
            }
        }

        private void TrajectoryPointAdd(Vector3 point)
        {
            var pointView = Instantiate(pointViewPrefab, root);
            pointView.transform.position = point;
            pointView.Init(this, _points.Count);
            _points.Add(pointView);
            _points[_points.Count - 1].SetLast(true);
            if (_points.Count > 1)
            {
                _points[_points.Count - 2].SetLast(false);
            }
        }

        public bool CheckIfLast(int index)
        {
            return index == _points.Count - 1;
        }

        public void CaptureTrajectory()
        {
            _trajectoryModel.Capture.Execute();
            _captured = true;
        }

        public void ReleaseTrajectory()
        {
            _captured = false;
            _trajectoryModel.CurrentDesirablePoint.Execute(_trajectoryModel.LastDrawnPoint.Value);
        }

        public void RemovePoint(int index)
        {
            if (index >= _points.Count)
            {
                return;
            }
            
            _trajectoryModel.RemoveAllPointsAfterIndex.Execute(index);
        }

        private void TrajectoryPointRemove()
        {
            var last = _points.Last();
            Destroy(last.gameObject);
            _points.RemoveAt(_points.Count - 1);
            _points.Last().SetLast(true);
        }

        private void Update()
        {
            if (!_captured)
            {
                return;
            }
            
            var currentInputPoint = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            currentInputPoint.Set(currentInputPoint.x, currentInputPoint.y, TrajectoryModel.DEFAULT_Z_POSITION);

            _desiredPoint = currentInputPoint + DragOffset * MAX_DISTANCE * (_trajectoryModel.LastDrawnPoint.Value - currentInputPoint).normalized;
            if ((currentInputPoint - _desiredPoint).magnitude > (currentInputPoint - _trajectoryModel.LastDrawnPoint.Value).magnitude)
            {
                _desiredPoint = _trajectoryModel.LastDrawnPoint.Value;
            }
            
            var lastIndex = _trajectoryModel.TrajectoryPoints.Count - 1;
            var angleCheck = true;
            if (_trajectoryModel.TrajectoryPoints.Count > 1)
            {
                var angle = Vector2.Angle(_desiredPoint - _trajectoryModel.TrajectoryPoints[lastIndex], _trajectoryModel.TrajectoryPoints[lastIndex] - _trajectoryModel.TrajectoryPoints[lastIndex - 1]);
                angleCheck = angle < MAX_ANGLE;
            }
            
            var distance = (_desiredPoint - _trajectoryModel.LastDrawnPoint.Value).magnitude;
            if (angleCheck)
            {
                if (distance > MAX_DISTANCE)
                {
                    _desiredPoint = (_desiredPoint - _trajectoryModel.LastDrawnPoint.Value).normalized * MAX_DISTANCE + _trajectoryModel.LastDrawnPoint.Value;
                    _trajectoryModel.CurrentDesirablePoint.Execute(_desiredPoint);
                    _trajectoryModel.AddPoint.Execute(_desiredPoint);
                }
                else
                {
                    _trajectoryModel.CurrentDesirablePoint.Execute(_desiredPoint);
                }
            }

            var desiredDistance = (_desiredPoint - _lastDesiredPoint).magnitude;
            if (desiredDistance > 0.1f)
            {
                emitter?.Emmit(false);
                _lastDesiredPoint = _desiredPoint;
            }
        }

        public void Dispose()
        {
            _trajectoryModel?.Dispose();
            _disposable?.Dispose();
            Destroy(gameObject);
        }
    }
}
