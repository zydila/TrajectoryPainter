using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Data;
using TP.Game.Interfaces;
using TP.Game.Models;
using TP.Game.Services;
using TP.Game.UI.Elements;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace TP.Game.Views
{
    [RequireComponent(typeof(Collider2D))]
    public class UnitView : MonoBehaviour, ISceneCollider
    {
        [SerializeField] private float timeLimit;
        [SerializeField] private int priority;
        [SerializeField] private UnitModel.UnitType type;
        [SerializeField] private List<ObstacleModel.Type> _obstacles = new List<ObstacleModel.Type>{ ObstacleModel.Type.Basic };
        [SerializeField] private List<CoinModel.Type> _coins = new List<CoinModel.Type>{ CoinModel.Type.Basic };
        [SerializeField] private Color defaultColor = Color.yellow;
        [SerializeField] private Color collisionColor = Color.gray;
        [SerializeField] UnitEvents events;
        
        public List<SkillComponent> Skills { get; private set; }
        public List<CoinModel.Type> CoinTypes => _coins;
        public List<ObstacleModel.Type> ObstacleTypes => _obstacles;
        public int Priority => priority;
        public float TimeLimit => timeLimit;
        public UnitModel.UnitType Type => type;
        public Color DefaultColor => defaultColor;

        private Collider2D _collider;
        private SpriteRenderer _sprite;

        public ISceneModel SceneModel => Model;
        protected UnitModel Model;
        
        private CompositeDisposable _refreshDisposable = new CompositeDisposable();

        public void Init(UnitModel model)
        {
            if (_sprite == null)
            {
                _sprite = GetComponentInChildren<SpriteRenderer>();
            }

            Skills = GetComponentsInChildren<SkillComponent>().Where(skill => skill.gameObject.activeSelf).ToList();
            Model = model;
            _collider = GetComponent<Collider2D>();
            Model.TrajectoryModel
                 .TrajectoryTimeLeft01
                 .Subscribe(lerp => events.OnLerpLifeTime?.Invoke(lerp))
                 .AddTo(this);
            Model.Position.Subscribe(position => transform.position = position).AddTo(this);
            Model.State
                 .Subscribe(state =>
                  {
                      switch (state)
                      {
                          case UnitModel.UnitState.Alive:
                              _sprite.color = defaultColor;
                              gameObject.SetActive(true);
                              _collider.enabled = true;
                              events.OnRespawn?.Invoke();
                              Skills.ForEach(skill => skill.RestoreInitialView());
                              break;
                          case UnitModel.UnitState.Dead:
                              events.OnDied?.Invoke();
                              events.OnStopMoving?.Invoke();
                              _collider.enabled = false;
                              break;
                          case UnitModel.UnitState.Consumed:
                              events.OnConsumed?.Invoke();
                              events.OnStopMoving?.Invoke();
                              _collider.enabled = false;
                              break;
                      }
                  })
                 .AddTo(this);
            Model.IsCollidingWithObstacle
                 .Subscribe(colliding => _sprite.color = colliding ? collisionColor : defaultColor)
                 .AddTo(this);
            
            Skills.ForEach(skill => skill.Init(this));
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Value
                         .State
                         .Subscribe(state =>
                          {
                              switch (state)
                              {
                                  case LevelModel.LevelState.InProgress:
                                      events.OnStartMoving?.Invoke();
                                      break;
                                  case LevelModel.LevelState.Finished:
                                  case LevelModel.LevelState.Lost:
                                      events.OnStopMoving?.Invoke();
                                      break;
                              }
                          }).AddTo(this);
        }
        
        public void RefreshCollider()
        {
            _collider.enabled = false;
            _refreshDisposable?.Dispose();
            _refreshDisposable = new CompositeDisposable();
            Observable.NextFrame()
                      .Subscribe(unit =>
                       {
                           _collider.enabled = true;
                           _refreshDisposable?.Dispose();
                       }).AddTo(_refreshDisposable);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log($"Collide with {other.gameObject.name}");
            var sceneCollider = other.GetComponent<ISceneCollider>() ?? other.transform.parent.GetComponent<ISceneCollider>();
            if (sceneCollider != null)
            {
                Model.Collision.Execute(sceneCollider.SceneModel);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            Debug.Log($"Collider exit {other.gameObject.name}");
            var sceneCollider = other.GetComponent<ISceneCollider>() ?? other.transform.parent.GetComponent<ISceneCollider>();
            if (sceneCollider != null)
            {
                Model.CollisionExit.Execute(sceneCollider.SceneModel);
            }
        }

        private void OnDestroy()
        {
            _refreshDisposable?.Dispose();
        }

        public void OnEditorAwake()
        {
            if (timeLimit < 0.1f)
            {
                timeLimit = FindObjectOfType<LevelSceneInitializer>().TimeLimit;
            }
        }
    }

    [Serializable]
    public class UnitEvents
    {
        [SerializeField] public UnityEvent OnRespawn;
        [SerializeField] public UnityEvent OnDied;
        [SerializeField] public UnityEvent OnConsumed;
        [SerializeField] public UnityEvent OnStartMoving;
        [SerializeField] public UnityEvent OnStopMoving;
        [SerializeField] public FloatUnityEvent OnLerpLifeTime;
    }
    
    [Serializable]
    public class FloatUnityEvent : UnityEvent<float> { }
}
