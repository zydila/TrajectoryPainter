using System;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.Views
{
    public class ErrorInterceptor : MonoBehaviour
    {
        [SerializeField] private Text title;
        [SerializeField] private Text body;
        
        private void Awake()
        {
            if (Debug.isDebugBuild)
            {
                Application.logMessageReceived += OnLogError;
            }

            gameObject.SetActive(false);
        }

        private void OnLogError(string condition, string stackTrace, LogType type)
        {
            if (type != LogType.Error && type != LogType.Exception )
            {
                return;
            }

            if (gameObject.activeSelf)
            {
                return;
            }

            title.text = "Error Received";
            body.text = condition + "\n\n\n" + stackTrace;
            gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            if (Debug.isDebugBuild)
            {
                Application.logMessageReceived -= OnLogError;
            }
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}