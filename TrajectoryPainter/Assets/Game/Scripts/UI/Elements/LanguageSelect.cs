using System;
using System.Collections.Generic;
using System.Linq;
using TP.Game.Data;
using TP.Game.Services;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class LanguageSelect : MonoBehaviour
{
    [SerializeField] private Dropdown dropdown;

    private List<Localization.Language> _languages = new List<Localization.Language>();

    private void Awake()
    {
        var options = new List<Dropdown.OptionData>();
        foreach (var language in Enum.GetValues(typeof(Localization.Language)).Cast<Localization.Language>())
        {
            _languages.Add(language);
            options.Add(new Dropdown.OptionData(Localization.Get(language.ToString())));
        }

        dropdown.options = options;
        Options.Language
               .Select(selectedLanguage => _languages.FindIndex(lang => lang == selectedLanguage))
               .Subscribe(languageIndex => dropdown.value = languageIndex)
               .AddTo(this);
    }

    public void OnValueChanged(int value)
    {
        Options.SetLanguage(_languages[value]);
    }
}
