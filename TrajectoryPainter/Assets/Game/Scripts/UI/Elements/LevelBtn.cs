using System;
using TP.Game.Data;
using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Elements
{
    public class LevelBtn : MonoBehaviour
    {
        [SerializeField] private LocalizedText levelName;
        [SerializeField] private LocalizedText levelScore;
        [SerializeField] private LocalizedText levelTimeLeft;
        [SerializeField] private Button btn;
        [SerializeField] private Button buyLevelBtn;
        [SerializeField] private LocalizedText buyLevelBtnTxt;

        private string _levelId;

        private CompositeDisposable _disposable = new CompositeDisposable();

        public void Init(LevelDataElement levelData, int index)
        {
            _levelId = levelData.Id;
            levelName.SetTextKey(levelData.NameKey).UpdateText();
            var score = 0;
            var timeLeft = 0f;
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            GameContainer.LevelSelectionModel
                         .LevelsSaveData
                         .Subscribe(levelsSaveData =>
                          {
                              if (levelsSaveData != null && levelsSaveData.Scores.Count > 0 &&
                                  levelsSaveData.Scores.ContainsKey(_levelId))
                              {
                                  score = levelsSaveData.Scores[_levelId].Score;
                                  timeLeft = levelsSaveData.Scores[_levelId].TotalTimeLeft01;
                              }

                              levelScore.SetParameter("AMOUNT", score.ToString())
                                        .SetParameter("MAX", levelData.MaxScore.ToString())
                                        .UpdateText();
                              levelTimeLeft.SetParameter("AMOUNT", timeLeft.ToString("0.00"))
                                           .UpdateText();
                          }).AddTo(_disposable);
            GameContainer.LevelSelectionModel
                         .CurrentLevel
                         .Subscribe(level => buyLevelBtn.gameObject.SetActive(level == index - 1))
                         .AddTo(_disposable);
            GameContainer.PlayerModel
                         .CoinStorage
                         .CurrentAmount
                         .Subscribe(coins => buyLevelBtn.interactable =  levelData.MaxScore < coins)
                         .AddTo(_disposable);
            buyLevelBtnTxt.SetParameter("AMOUNT", levelData.MaxScore.ToString())
                          .UpdateText();
            gameObject.SetActive(true);
        }

        public void SelectLevel()
        {
            GameContainer.LevelSelectionModel.StartLevel.Execute(_levelId);
        }

        public void SetInteractable(bool newInteractable)
        {
            btn.interactable = newInteractable;
        }

        public void OnBuyLevel()
        {
            GameContainer.LevelSelectionModel.BuyLevel.Execute(_levelId);
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
        }

        public void Hide()
        {
            _disposable?.Dispose();
            gameObject.SetActive(false);
        }
    }
}
