using TP.Game.Models;
using TP.Game.Views;
using UniRx;
using UnityEngine;

namespace TP.Game.UI.Elements
{
    [RequireComponent(typeof(LocalizedText))]
    public class LevelStatusView : MonoBehaviour
    {
        private LocalizedText _text;
        
        private CompositeDisposable _disposable = new CompositeDisposable();

        private void Awake()
        {
            _text = GetComponent<LocalizedText>();
            GameContainer.LevelSelectionModel.CurrentLevelModel.Subscribe(OnLevelChanged).AddTo(this);
        }

        private void OnLevelChanged(LevelModel model)
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            model?.State
                  .Subscribe(state => _text.SetTextKey(state.ToString()).UpdateText())
                  .AddTo(_disposable);
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
        }
    }
}
