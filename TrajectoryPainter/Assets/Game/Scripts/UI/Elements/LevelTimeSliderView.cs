using TP.Game.Models;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    [RequireComponent(typeof(Slider))]
    public class LevelTimeSliderView : MonoBehaviour
    {
        [SerializeField] private Text time;
        
        private Slider _slider;
        
        private CompositeDisposable _disposable = new CompositeDisposable();

        private void Awake()
        {
            _slider = GetComponent<Slider>();
            GameContainer.LevelSelectionModel.CurrentLevelModel.Subscribe(OnLevelChanged).AddTo(this);
        }

        public void OnValueChanged(float value)
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value?.ChangeCurrentTimeLerp01.Execute(value);
        }

        private void OnLevelChanged(LevelModel model)
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            if (model == null)
            {
                return;
            }

            model.State
                 .Select(state => state == LevelModel.LevelState.Prepare)
                 .Subscribe(isPrepare => _slider.interactable = isPrepare)
                 .AddTo(_disposable);
            model.CurrentTime
                 .Subscribe(levelTime => time.text = levelTime.ToString("0.00"))
                 .AddTo(_disposable);
            model.CurrentTimeLerp
                 .Subscribe(value => _slider.SetValueWithoutNotify(value))
                 .AddTo(_disposable);
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
        }
    }
}
