using System.Collections.Generic;
using TP.Game.Data;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    public class LevelsList : MonoBehaviour
    {
        [SerializeField] private LevelBtn _levelBtnPrefab;
        [SerializeField] private VerticalLayoutGroup _verticalLayoutGroup;

        private List<LevelBtn> levelsButtons = new List<LevelBtn>();
        
        private void Awake()
        {
            GameContainer.LevelSelectionModel
                         .LevelsData
                         .Subscribe(OnLevelsLoaded)
                         .AddTo(this);

            GameContainer.LevelSelectionModel
                         .CurrentLevel
                         .Subscribe(level =>
                          {
                              for (var i = 0; i < levelsButtons.Count; i++)
                              {
                                  levelsButtons[i].SetInteractable(i <= level);
                              }
                          }).AddTo(this);
        }

        private void OnLevelsLoaded(LevelsPackData levelsPackData)
        {
            for (var i = 0; i < levelsPackData.Levels.Count; i++)
            {
                if (i >= levelsButtons.Count)
                {
                    var levelBtn = Instantiate(_levelBtnPrefab, _verticalLayoutGroup.transform);
                    levelBtn.gameObject.SetActive(true);
                    levelsButtons.Add(levelBtn);
                }
                levelsButtons[i].Init(levelsPackData.Levels[i], i);
                levelsButtons[i].SetInteractable(i <= GameContainer.LevelSelectionModel.CurrentLevel.Value);
            }

            if (levelsButtons.Count > levelsPackData.Levels.Count)
            {
                for (var i = levelsPackData.Levels.Count; i < levelsButtons.Count; i++)
                {
                    levelsButtons[i].Hide();
                }
            }
        }
    }
}