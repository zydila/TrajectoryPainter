using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Elements
{
    public class LivesCounter : MonoBehaviour
    {
        [SerializeField] private LocalizedText lives; 
        [Header("Can be null")]
        [SerializeField] private Timer timer;

        private void Awake()
        {
            GameContainer.PlayerModel.PlayerLives
                         .Subscribe(livesCount => lives.SetParameter("AMOUNT", livesCount.ToString()).UpdateText())
                         .AddTo(this);
            GameContainer.PlayerModel.LivesTimer
                         .ObserveOnMainThread()
                         .Subscribe(timer.StartTimer)
                         .AddTo(this);
        }
    }
}