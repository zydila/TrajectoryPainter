using TP.Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    public class MasterVolumeController : MonoBehaviour
    {
        [SerializeField] private Slider slider;

        private void OnEnable()
        {
            slider.SetValueWithoutNotify(Options.MasterVolume.Value);
        }

        public void OnValueChanged(float value)
        {
            if (value <= 0.01)
            {
                value = 0;
            }
            Options.SetMasterVolume(value);
        }
    }
}