using TP.Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    public class MusicVolumeController : MonoBehaviour
    {
        [SerializeField] private Slider slider;

        private void OnEnable()
        {
            slider.SetValueWithoutNotify(Options.MusicVolume.Value);
        }

        public void OnValueChanged(float value)
        {
            if (value <= 0.01)
            {
                value = 0;
            }
            Options.SetMusicVolume(value);
        }
    }
}