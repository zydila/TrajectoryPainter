using TP.Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    public class MuteSoundController : MonoBehaviour
    {
        [SerializeField] private Toggle toggle;

        private void OnEnable()
        {
            toggle.SetIsOnWithoutNotify(Options.SoundMuted.Value);
        }

        public void OnValueChanged(bool value)
        {
            Options.SetSoundMuted(value);
        }
    }
}