using TP.Game.Controllers;
using TP.Game.Data;
using TP.Game.Models;
using TP.Game.UI.Screens;
using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Elements
{
    public class PackView : MonoBehaviour
    {
        [SerializeField] private LocalizedText packName;
        [SerializeField] private LocalizedText cost;
        [SerializeField] private Button mainBtn;
        [SerializeField] private GameObject buyBtn;

        private LevelPackType _type;
        private bool isFree;
        
        public void Init(LevelPackType type, LevelsPackData packData)
        {
            _type = type;
            packName.SetTextKey(type.ToString())
                .UpdateText();
            isFree = packData.IsFree;
            if (!isFree)
            {
                cost.SetParameter("AMOUNT", packData.Product.LocalizePrice())
                    .UpdateText();
            }

            GameContainer.LevelSelectionModel
                         .PacksSaveData
                         .Subscribe(OnLevelPackUpdate)
                         .AddTo(this);
        }

        public void OnClick()
        {
            GameContainer.LevelSelectionModel.SelectPack.Execute(_type);
            ScreensController.OpenScreen<LevelSelectScreen>();
        }

        public void BuyPack()
        {
            GameContainer.LevelSelectionModel.BuyPack.Execute(_type);
        }

        private void OnLevelPackUpdate(LevelPacksSave save)
        {
            var isUnlocked = isFree || save.IsPackUnlocked(_type);
            mainBtn.interactable = isUnlocked;
            buyBtn.SetActive(!isUnlocked);
        }
    }
}