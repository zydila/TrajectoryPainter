using TP.Game.Interfaces;
using TP.Game.Services;
using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Elements
{
    public class SkillView : MonoBehaviour
    {
        [SerializeField] private Button btn;
        [SerializeField] private LocalizedText text;

        private ISkill _skill;
        private string _localizedDescription; 

        public void Init(ISkill skill)
        {
            _skill = skill;
//            this.skill
//                .CanBeUsed
//                .Subscribe(canBeUsed => btn.interactable = canBeUsed)
//                .AddTo(this);
            gameObject.SetActive(true);
            var colors = new ColorBlock
            {
                normalColor = skill.ViewConfig.Color,
                highlightedColor = new Color(skill.ViewConfig.Color.r + 0.1f, skill.ViewConfig.Color.g + 0.1f, skill.ViewConfig.Color.b + 0.1f, 1),
                pressedColor = new Color(skill.ViewConfig.Color.r - 0.1f, skill.ViewConfig.Color.g - 0.1f, skill.ViewConfig.Color.b - 0.1f, 1),
                selectedColor = new Color(skill.ViewConfig.Color.r - 0.1f, skill.ViewConfig.Color.g - 0.1f, skill.ViewConfig.Color.b - 0.1f, 1),
                disabledColor = new Color(skill.ViewConfig.Color.r - 0.3f, skill.ViewConfig.Color.g - 0.3f, skill.ViewConfig.Color.b - 0.3f, 1),
                colorMultiplier = 1,
                fadeDuration = 0.1f
            };
            btn.colors = colors;
            _localizedDescription = !string.IsNullOrEmpty(skill.ViewConfig.LocKeyParameter) ? 
                                        Localization.Get(skill.ViewConfig.DescriptionKey, "AMOUNT", skill.ViewConfig.LocKeyParameter) : 
                                        Localization.Get(skill.ViewConfig.DescriptionKey);
            text.SetTextKey(skill.ViewConfig.NameKey).UpdateText();
        }
        
        public void UseSkill()
        {
            if (_skill.CanBeUsed.Value)
            {
                _skill?.Use.Execute();
            }
            else
            {
                Ticker.ShowLocalized(_localizedDescription);
            }
        }
    }
}