using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Utilities;
using TP.Game.Views;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    public class Ticker : MonoBehaviour
    {
        private const int TICKET_SCREEN_TIME_IN_MILLISECONDS = 3000;

        [SerializeField] private LocalizedText text;
        [SerializeField] private SimpleAnimation _animation;
        
        private static Ticker _instance;

        private Queue<TickerInfo> _tickets = new Queue<TickerInfo>();
        private bool isShowing;
        private CancellationTokenSource _cancellation;
        
        private void Awake()
        {
            _instance = this;
        }

        public static void Show(string messageKey)
        {
            _instance?.ShowTickerInternal(messageKey, false);
        }

        public static void ShowLocalized(string localizedMessage)
        {
            _instance?.ShowTickerInternal(localizedMessage, true);
        }

        private void ShowTickerInternal(string messageKey, bool localized)
        {
            if (!_tickets.ToList().Exists(ticker => ticker.MessageKey == messageKey))
            {
                _tickets.Enqueue(new TickerInfo(messageKey, localized));
                ShowNextInQueue();
            }
        }

        private async void ShowNextInQueue()
        {
            if (isShowing || _tickets.Count == 0)
            {
                return;
            }

            SetTicker(_tickets.Peek());
            isShowing = true;
            _cancellation?.Cancel();
            _cancellation = new CancellationTokenSource();
            await _animation.Play("Show", _cancellation);
            await Task.Delay(TICKET_SCREEN_TIME_IN_MILLISECONDS);
            _cancellation = new CancellationTokenSource();
            await _animation.Play("Hide", _cancellation);
            _tickets.Dequeue();
            isShowing = false;
            ShowNextInQueue();
        }

        private void SetTicker(TickerInfo ticker)
        {
            if (ticker.Localized)
            {
                text.GetComponent<Text>().text = ticker.MessageKey;
            }
            else
            {
                text.SetTextKey(ticker.MessageKey).UpdateText();
            }
        }

        private void OnDestroy()
        {
            _cancellation?.Cancel();
        }

        private struct TickerInfo
        {
            public readonly string MessageKey;
            public readonly bool Localized;

            public TickerInfo(string messageKey, bool localized)
            {
                MessageKey = messageKey;
                Localized = localized;
            }
        }
    }
}