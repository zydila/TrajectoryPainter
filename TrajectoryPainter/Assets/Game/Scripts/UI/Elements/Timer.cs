using TP.Game.Models;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Elements
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] private Text timer;
        
        private CompositeDisposable _timerDisposable = new CompositeDisposable();

        public void StartTimer(TimerModel model)
        {
            _timerDisposable?.Dispose();
            _timerDisposable = new CompositeDisposable();
            timer.gameObject.SetActive(model != null && !model.Completed);
            model?.CompletedObservable.Subscribe(completed => timer.gameObject.SetActive(!completed)).AddTo(_timerDisposable);
            model?.TimeLeft.Subscribe(timeLeft => timer.text = timeLeft.ToString("0:00")).AddTo(_timerDisposable);
        }

        private void OnDestroy()
        {
            _timerDisposable?.Dispose();
        }
    }
}