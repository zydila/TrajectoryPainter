using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.Views
{
    [RequireComponent(typeof(LocalizedText))]
    public class TotalCoinsView : MonoBehaviour
    {
        private LocalizedText _text;
        
        private void Awake()
        {
            _text = GetComponent<LocalizedText>();
            GameContainer.PlayerModel
                         .CoinStorage
                         .CurrentAmount
                         .Subscribe(coins => _text.SetParameter("AMOUNT", coins.ToString()).UpdateText())
                         .AddTo(this);
        }
    }
}