using System.Threading;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

namespace TP.Game.UI.Elements
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class UILoading : MonoBehaviour
    {
        private static UILoading _instance;

        private SimpleAnimation _animation;
        private int _showCounter;
        private bool IsShowing => _showCounter > 0;
        private CancellationTokenSource _cancellation;
        private CompositeDisposable _disposable;
        
        private void Awake()
        {
            _instance = this;
            _animation = GetComponent<SimpleAnimation>();
            _animation.Init();
            Hide();
        }

        public static Task Show()
        {
            return _instance?.ShowInternal();
        }

        public static Task Hide()
        {
            return _instance?.HideInternal();
        }

        private Task ShowInternal()
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            
            gameObject.SetActive(true);
            if (!IsShowing)
            {
                _cancellation?.Cancel();
                _cancellation = new CancellationTokenSource();
                _showCounter++;
                return _animation.Play("Show", _cancellation);
            }
            _showCounter++;
            return Task.CompletedTask;
        }

        private Task HideInternal()
        {
            _cancellation?.Cancel();
            _disposable?.Dispose();
            _showCounter--;
            if (_showCounter < 0)
            {
                _showCounter = 0;
            }

            if (_showCounter == 0)
            {
                _disposable = new CompositeDisposable();
                _cancellation = new CancellationTokenSource();
                var hideTask = _animation.Play("Hide", _cancellation);
                hideTask.ToObservable()
                    .ObserveOnMainThread()
                    .Subscribe(unit =>
                     {
                         gameObject.SetActive(false);
                         _disposable?.Dispose();
                     }).AddTo(_disposable);
                return hideTask;
            }
            
            return Task.CompletedTask;
        }

        private void OnDestroy()
        {
            _cancellation?.Cancel(); 
            _disposable?.Dispose();
        }
    }
}