using System.Threading;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;

namespace TP.Game.UI.Elements
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class UILoadingIcon : MonoBehaviour
    {
        private static UILoadingIcon _instance;

        private readonly ReactiveProperty<int> _counter = new ReactiveProperty<int>(0);
        
        private void Awake()
        {
            _instance = this;
            _counter.Subscribe(count =>
            {
                if (count < 0)
                {
                    _counter.Value = 0;
                    return;
                }

                gameObject.SetActive(count > 0);
            }).AddTo(this);
        }

        public static void Show()
        {
            _instance?.ShowInternal();
        }

        public static void Hide()
        {
            _instance?.HideInternal();
        }

        private void ShowInternal()
        {
            _counter.Value++;
        }

        private void HideInternal()
        {
            _counter.Value--;
        }

        void UpdateVisibility()
        {
            
        }
    }
}