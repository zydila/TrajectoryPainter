using System;
using System.Collections;
using System.Collections.Generic;
using TP.Game.UI.Elements;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TP.Game.UI.Elements
{
    [DisallowMultipleComponent]
    public class UISoundEmitter : MonoBehaviour, ISelectHandler
    {
        public event Action<bool> OnEmitSound;

        public void Emmit(bool force)
        {
            OnEmitSound?.Invoke(force);
        }

        public void OnSelect(BaseEventData eventData)
        {
            OnEmitSound?.Invoke(true);
        }
    }
}