using TP.Game.Controllers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    [DisallowMultipleComponent]
    public class UISoundPlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip clip;

        [SerializeField, Range(0f, 2f)] private float pitch = 1f;
        [SerializeField, Range(0f, 1f)] private float volume = 1f;

        [SerializeField] private bool playOnAwake;
        [SerializeField] private bool playOnEnable;
        
        private void Awake()
        {
            Init();
            if (playOnAwake)
            {
                Play(true);
            }
        }
        protected virtual void Init(){ }

        private void OnEnable()
        {
            if (playOnEnable)
            {
                Play(true);
            }
        }

        public void Play(bool force)
        {
            UIAudioSource.PlaySound(clip, pitch, volume, force);
        }
    }
}