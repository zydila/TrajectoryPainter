using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TP.Game.UI.Elements
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ISelectHandler))]
    public class UISoundSelectablePlayer : UISoundPlayer
    {
        private Button _button;
        private Toggle _toggle;
        private Slider _slider;
        private Dropdown _dropMenu;
        private UISoundEmitter _soundEmitter;

        protected override void Init()
        {
            _button = GetComponent<Button>();
            _toggle = GetComponent<Toggle>();
            _slider = GetComponent<Slider>();
            _dropMenu = GetComponent<Dropdown>();
            _soundEmitter = GetComponent<UISoundEmitter>();
            
            if (_button != null)
            {
                _button.onClick.AddListener(() => Play(true));
            }
            else if (_toggle != null)
            {
                _toggle.onValueChanged.AddListener(value => Play(true));
            }
            else if (_slider != null)
            {
                _slider.onValueChanged.AddListener(value => Play(false));
            }
            else if (_dropMenu != null)
            {
                _dropMenu.onValueChanged.AddListener(value => Play(true));
            }
            else if (_soundEmitter != null)
            {
                _soundEmitter.OnEmitSound += Play;
            }
        }
    }
}