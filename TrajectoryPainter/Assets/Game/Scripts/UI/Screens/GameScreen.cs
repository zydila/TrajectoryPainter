using System;
using TP.Game.Controllers;
using TP.Game.Models;
using TP.Game.UI.Elements;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Screens
{
    public class GameScreen : AnimatedScreenController
    {
        [SerializeField] private GameObject clearTrajectoriesBtnObject;
        [SerializeField] private GameObject exitBtnObject;
        [SerializeField] private GameObject startBtnObject;
        [SerializeField] private Button optionBtn;
        
        [SerializeField] private SkillView skillViewPrefab;
        [SerializeField] private Transform skillsRoot;
        
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        protected override void OnAwake()
        {
            GameContainer.LevelSelectionModel
                         .CurrentLevelModel
                         .Subscribe(OnLevelChanged)
                         .AddTo(this);
        }

        private void Start()
        {
            foreach (var unitModel in GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Units)
            {
                foreach (var skill in unitModel.Skills)
                {
                    var view = Instantiate(skillViewPrefab, skillsRoot);
                    view.gameObject.SetActive(true);
                    view.Init(skill);
                }
            }
        }

        private void OnLevelChanged(LevelModel model)
        {
            _disposable?.Dispose();
            _disposable = new CompositeDisposable();
            model?.FinishLevel
                  .Subscribe(result => ScreensController.OpenScreen<LevelFinishedScreen>().Init(result))
                  .AddTo(_disposable);
            model?.State
                  .Select(state => state == LevelModel.LevelState.Prepare)
                  .Subscribe(isPrepare =>
                   {
                       clearTrajectoriesBtnObject.SetActive(isPrepare);
                       exitBtnObject.SetActive(isPrepare);
                       startBtnObject.SetActive(isPrepare);
                       optionBtn.interactable = isPrepare;
                   }).AddTo(_disposable);
        }

        public void LevelStart()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.StartLevel.Execute();
        }

        public void ClearTrajectories()
        {
            foreach (var unit in GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Units)
            {
                unit.ClearTrajectory.Execute();
            }
        }

        public void ExitToMainMenu()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ExitLevel.Execute();
        }

        private void OnDestroy()
        {
            _disposable?.Dispose();
        }
    }
}