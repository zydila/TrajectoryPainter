using TP.Game.Controllers;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TP.Game.UI.Screens
{
    public class LevelFinishedScreen : AnimatedScreenController
    {
        [SerializeField] private Button nextLevelBtn;

        protected override void OnAwake()
        {
            GameContainer.LevelSelectionModel
                         .StartNextLevel
                         .CanExecute
                         .Subscribe(canExecuteNextLevel => nextLevelBtn.interactable = canExecuteNextLevel)
                         .AddTo(this);
        }

        public void Init(bool result)
        {
            nextLevelBtn.gameObject.SetActive(result);
        }

        public void RestartLevel()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Restart.Execute();
            ScreensController.OpenScreen<GameScreen>();
        }

        public void NextLevel()
        {
            GameContainer.LevelSelectionModel.StartNextLevel.Execute();
        }

        public void ExitToMainMenu()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ExitLevel.Execute();
        }
    }
}