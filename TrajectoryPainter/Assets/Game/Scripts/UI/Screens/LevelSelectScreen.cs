using TP.Game.Controllers;
using UnityEngine;

namespace TP.Game.UI.Screens
{
    public class LevelSelectScreen : AnimatedScreenController
    {
        public void OpenMainMenu()
        {
            ScreensController.OpenScreen<MainMenuScreen>();
        }
    }
}