using System.Threading.Tasks;
using TP.Game.Controllers;
using TP.Game.UI.Elements;
using UnityEngine;

namespace TP.Game.UI.Screens
{
    public class MainMenuScreen : AnimatedScreenController
    {
        public void OpenOptions()
        {
            ScreensController.OpenScreen<OptionsScreen>();
        }
        
        public void OpenLevelSelect()
        {
            ScreensController.OpenScreen<PackSelectScreen>();
        }

        public async void OpenShop()
        {
            Debug.Log("Open Shop");
            Ticker.Show("test");
            Ticker.Show("test2");
            await UILoading.Show();
            Ticker.Show("test3");
            await Task.Delay(2000);
            await UILoading.Hide();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}