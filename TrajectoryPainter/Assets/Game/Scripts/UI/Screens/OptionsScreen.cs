using TP.Game.Controllers;

namespace TP.Game.UI.Screens
{
    public class OptionsScreen : AnimatedScreenController
    {
        public void OpenMainMenu()
        {
            ScreensController.OpenScreen<MainMenuScreen>();
        }
    }
}