using TP.Game.Controllers;
using TP.Game.Models;
using TP.Game.UI.Elements;
using UniRx;
using UnityEngine;

namespace TP.Game.UI.Screens
{
    public class PackSelectScreen : AnimatedScreenController
    {
        [SerializeField] private PackView packPrefab;
        [SerializeField] private Transform packRoot;
        
        protected override void OnAwake()
        {
            foreach (var pack in GameContainer.LevelSelectionModel.PacksData)
            {
                var packView = Instantiate(packPrefab, packRoot);
                packView.Init(pack.Key, pack.Value);
                packView.gameObject.SetActive(true);
            }

//            GameContainer.PlayerModel
//                         .LevelPacksSaveData
//                         .Subscribe()
//                         .AddTo(this);
        }
    }
}