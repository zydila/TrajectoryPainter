using TP.Game.Models;
using TP.Game.UI.Elements;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace TP.Game.UI.Screens
{
    public class TestUI : MonoBehaviour
    {
        [SerializeField] private Text text;
        [SerializeField] private Slider slider;
        [SerializeField] private Text levelState;
        [SerializeField] private Text scoreTxt;
        [SerializeField] private GameObject startLevelBtn;
        [SerializeField] private GameObject restartLevelBtn;
        [SerializeField] private GameObject exitLevelBtn;
        [SerializeField] private GameObject clearTrajectories;
        [SerializeField] private Transform skillsRoot;
        [SerializeField] private SkillView skillViewPrefab;

        private void Start()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.CurrentTime
                .Subscribe(value => text.text = value.ToString("#0.00"))
                .AddTo(this);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.CurrentTimeLerp
                .Subscribe(value => slider.SetValueWithoutNotify(value))
                .AddTo(this);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State
                .Subscribe(state =>
                {
                    startLevelBtn.SetActive(state == LevelModel.LevelState.Prepare);
                    clearTrajectories.SetActive(state == LevelModel.LevelState.Prepare);
                    exitLevelBtn.SetActive(state == LevelModel.LevelState.Prepare || state == LevelModel.LevelState.Finished  || state == LevelModel.LevelState.Lost);
                    slider.interactable = state == LevelModel.LevelState.Prepare;
                })
                .AddTo(this);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.State
                .Subscribe(state => levelState.text = state.ToString())
                .AddTo(this);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.TotalScore
                .Subscribe(score => scoreTxt.text = score.ToString())
                .AddTo(this);
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.LevelFinished
                .Subscribe(value =>
                {
                    restartLevelBtn.SetActive(value);
                })
                .AddTo(this);
            foreach (var unitModel in GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Units)
            {
                foreach (var skill in unitModel.Skills)
                {
                    var view = Instantiate(skillViewPrefab, skillsRoot);
                    view.gameObject.SetActive(true);
                    view.Init(skill);
                }
            }
        }

        public void OnSliderValueChanged(float value)
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ChangeCurrentTimeLerp01.Execute(value);
        }

        public void StartLevel()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.StartLevel.Execute();
        }

        public void RestartLevel()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Restart.Execute();
        }

        public void ExitLevel()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ExitLevel.Execute();
        }

        public void AddToTime()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ChangeCurrentTimeLerp01.Execute(slider.value + 0.01f);
        }

        public void MinusFromTime()
        {
            GameContainer.LevelSelectionModel.CurrentLevelModel.Value.ChangeCurrentTimeLerp01.Execute(slider.value - 0.01f);
        }

        public void ClearTrajectories()
        {
            foreach (var unit in GameContainer.LevelSelectionModel.CurrentLevelModel.Value.Units)
            {
                unit.ClearTrajectory.Execute();
            }
        }
    }
}
