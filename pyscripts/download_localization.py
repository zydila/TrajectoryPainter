#!/usr/bin/env python
#https://developers.google.com/sheets/api/quickstart/python?authuser=4
from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1NsBGrw6zfLKr63ksodU_bQ3AdoTbP5OGEFIIOdDf15M'
SAMPLE_RANGE_NAME = 'Localization'

def main():
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('../pyscripts/token.json'):
        creds = Credentials.from_authorized_user_file('../pyscripts/token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('../pyscripts/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('../pyscripts/token.json', 'w') as token:
            token.write(creds.to_json())

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('Localization import: FAILED')
    else:
        print('Localization import: SUCCESS')
        f = open("../pyscripts/TP Localization.csv", "w+", encoding='utf-8')
        for row in values:
            str1 = ","
            f.write("%s\n" % str1.join(row))
        f.close()

if __name__ == '__main__':
    main()